# Français

## Comment utiliser l'application

### Utiliser directement

1. Télécharger le code source
2. Charger le fichier `index.html`

### Inclure l'application dans une page HTML

1. Importer le fichier `build/app.js`
2. Charger le constructeur `MCDG.default` en passant un `HTMLCanvasElement` en argument
3. Lancer la méthode `run()` sur l'objet créé pour faire fonctionner l'application dans le `HTMLCanvasElement` donné.

# English

## How to use

### Use the application directly

1. Download the source
2. Load the `index.html` file

### Include the application in a HTML page or in a larger javaScript application

1. Import the `build/app.js` file
2. Use `MCDG.default` as constructor for the application, passing an `HTMLCanvasElement` as an argument
3. Run the `start()` function on the application object thus created in order to run the application in the given `HTMLCanvasElement`

## How to build

1. Run `npm install webpack`
2. Run `webpack`
3. Get back the `build/app.js` file

## Project architecture

- app: source code of the application
	- activities
		- tools
		- operations
		- actions
	- controller 
	- geometry
	- scene
	- serialization
	- vue
		- printers
- test: test files
- vendor: third-party libraries
 

