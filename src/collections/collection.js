import GeometryInterface from "../geometry/geometryInterface";

/**
 * @class Collection
 * @abstract
 *
 * @brief Regroups many GeometryInterface elements
 *
 * @version 3.0
 */
export default class Collection {
	/**
	 * @param {Array<GeometryInterface>} elements
	 */
	constructor(...elements) {
		this._elements = elements;
	}

	/**
	 * @brief List of the elements of this collection
	 *
	 * @type {Array<GeometryInterface>}
	 */
	get elements() {
		return this._elements;
	}

	/**
	 * @brief Removes the specified element from this collection
	 *
	 * @param {GeometryInterface} element: the element to remove
	 */
	remove(element) {
		this._elements = this._elements.filter(
			e => e !== element && e.id !== element.id
		);
	}

	/**
	 * @brief Adds a new element to this collection
	 *
	 * @param {GeometryInterface} element: the element to add to the collection
	 */
	add(element) {
		if (!(element instanceof GeometryInterface)) {
			throw new Error(`Expected element to be a GeometryInterface, ${typeof element} given`);
		}
		this.elements.push(element);
	}

	/**
	 * @brief Checks if this collection contains some geometrical element
	 *
	 * @param {GeometryInterface} element: the element to check
	 *
	 * @return {boolean}
	 */
	contains(element) {
		return this.elements.map(e => e.id).indexOf(element.id) > -1;
	}
	/**
	 * Supprime tous les élements, utile pour le calque brie
	 */
	empty(){
		this._elements = []
	}
}
