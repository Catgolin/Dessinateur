import GeometryInterface from "../geometry/geometryInterface";
import Collection from "./collection";

export default class Layer extends Collection {
	/**
	 * @param {string} layerSettings
	 */
	constructor(layerSettings) {
		super();
		this.layerSettings = layerSettings;
	}
	/**
	 * @param {ActionInterface} action
	 * @return {boolean}
	 */
	allows(action) {
		return this.layerSettings[action.constructor];
	}
	/**
	 * @override
	 * @param {GeometryInterface} element
	 */
	add(element) {	
		super.add(element);
		// if(element.layer !== this)
		// 	element.layer = this;
	}
	update(element) {
        super.empty()
		super.add(element)
		// if(element.layer !== this)
		// 	element.layer = this;
	}
}
