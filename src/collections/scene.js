import GeometryInterface from "../geometry/geometryInterface";
import Layer from "./layer";

export default class Scene {
	/**
	 * @brief (readonly) The layers constituting the scene
	 * @type {Array<Layer>}
	 */
	#layers;
	get layers() {
		return this.#layers;
	}

	/**
	 * @brief (readonly) The layer used to preview elements that are not on the scene
	 * @type {Layer}
	 */
	#preview;
	get preview() {
		return this.#preview;
	}

	constructor() {
		this.#layers = [new Layer()];
		this.#preview = new Layer();
	}

	/**
	 * @param {Layer} layerOptions
	 */
	addLayer(layerSettings) {
		this.layers.push(new Layer(layerSettings));
	}
	/**
	 * @param {GeometryInterface} element
	 */
	removeElement(element) {
		this.layers.forEach(layer => layer.remove(element));
	}
	/**
	 * @param {GeometryInterface} element
	 * @return {boolean}
	 */
	includes(element) {
		return element.layer !== undefined &&
			this.layers.indexOf(element.layer) > -1;
	}
}
