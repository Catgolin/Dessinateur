import Collection from "../../collections/collection";
import GeometryInterface from "../../geometry/geometryInterface";

/**
 * @brief Describes an action to perform on a specific element in a specific layer
 * @version 2.0
 * @date 2022-01-30
 * @author Catgolin
 */
export default class ActionInterface {
	/**
	 * @brief The type of element allowed for this action
	 * @type {function}
	 */
	static get type() {
		return GeometryInterface;
	}

	/**
	 * @brief (readonly) The element concerned by this action
	 * @type {GeometryInterface}
	 */
	#element;
	get element() {
		return this.#element;
	}
	/**
	 * @param {GeometryInterface} element The element on which the action should be performed
	 */
	constructor(element) {
		if (!(element instanceof this.constructor.type)) {
			throw new Error(`Expected the element to be an instanceof ${this.constructor.type.name}, got ${element ? element.constructor.name : element}`);
		}
		this.#element = element;
	}
	/**
	 * @abstract
	 * @brief Performs the action
	 * @param {Layer} layer The layer on which the action should be performed
	 */
	execute(layer) {
		throw new Error(`${this.constructor.name} should implement the execute method`);
	}
}
