import ActionInterface from "./actionInterface";

/**
 * @brief Adds the element to the layer
 *
 * @note The element will not be duplicated to the new layer:
 * The responsability to create a new element lies with the operation if it is required,
 * or we would override the execute method in a child class.
 *
 * @version 2.0
 * @author Catgolin
 * @date 2022-01-31
 */
export default class CreateElementAction extends ActionInterface {
	/**
	 * @override
	 * @inheritdoc
	 */
	execute(layer) {
		layer.add(this.element);
	}
}
