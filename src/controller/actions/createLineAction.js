import CreateElementAction from "./createElementAction";
import Line from "../../geometry/curves/line";

/**
 * @brief Adds a line to the layer
 * @version 2.0
 * @date 2022-01-30
 * @author Catgolin
 */
export default class CreateLineAction extends CreateElementAction {
	/**
	 * @override
	 * @inheritdoc
	 */
	static get type() {
		return Line;
	}
}
