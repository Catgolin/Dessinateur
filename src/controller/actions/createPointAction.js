import CreateElementAction from "./createElementAction";
import Point from "../../geometry/points/point";

/**
 * @brief Adds a point to the layer
 * @version 2.0
 * @date 2022-01-30
 * @author Catgolin
 */
export default class CreatePointAction extends CreateElementAction {
	/**
	 * @override
	 * @inheritdoc
	 */
	static get type() {
		return Point;
	}
}
