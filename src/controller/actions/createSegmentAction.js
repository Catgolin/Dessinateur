import CreateElementAction from "./createElementAction";
import Segment from "../../geometry/curves/segment";

/**
 * @brief Adds a segment to the layer
 * @version 2.0
 * @author Catgolin
 * @date 2022-01-31
 */
export default class CreateSegmentAction extends CreateElementAction {
	/**
	 * @override
	 * @inheritdoc
	 */
	static get type() {
		return Segment;
	}
}
