import ModeInterface from "./modeInterface";
import Point from "../../geometry/points/point";
import CreatePointOperation from "../operations/createPointOperation";
import CreateLineOperation from "../operations/createLineOperation";
import CreateSegmentOperation from "../operations/createSegmentOperation";

/**
 * @brief Mode where we can create new elements
 * @versino 2.0
 * @date 2022-01-30
 * @author Catgolin
 */
export default class DrawMode extends ModeInterface {
	/**
	 * @override
	 * @inheritdoc
	 */
	get operations() {
		return {
			point: new CreatePointOperation(this.app.scene),
			line: new CreateLineOperation(this.app.scene),
			segment: new CreateSegmentOperation(this.app.scene),
		};
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get clickOperation() {
		return this.operations.point;
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get dragOperation() {
		return this.operations.line;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	click(point) {
		super.click(point);
		switch (this.operation.constructor) {
		case CreateLineOperation:
		case CreateSegmentOperation:
			this.help = `operation.line.step.click`;
		case CreatePointOperation:
			this.operation.prepare(point);
			break;
		}
		if (this.operation.ready) {
			this.operation.execute();
			this.resetOperation();
		}
		return this;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	dragStart(point) {
		super.dragStart(point);
		switch (this.operation.constructor) {
		case CreateLineOperation:
		case CreateSegmentOperation:
			this.operation.prepare(point);
			return this;
		}
		return this;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	dragEnd(point) {
		super.dragEnd(point);
		switch (this.operation.constructor) {
		case CreateLineOperation:
		case CreateSegmentOperation:
			this.operation.prepare(point);
			this.operation.execute();
			this.resetOperation();
			return this;
		}
		return this;
	}
}
