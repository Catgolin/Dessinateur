import MCDG from "../../index.js";
import OperationInterface from "../operations/operationInterface";
import Point from "../../geometry/points/point";

/**
 * @brief Describes a mode in which different operations are presented to the user
 * @abstract
 * @version 2.0
 * @date 2022-01-30
 * @author Catgolin
 */
export default class ModeInterface {
	/**
	 * @abstract
	 * @brief (readonly) Associative array of the available operations
	 * @type {Object<OperationInterface>}
	 */
	get operations() {
		throw new Error(`The operation get method should be specified for ${this.constructor.name}`);
	}

	/**
	 * @brief (readonly) Lists the default operations of this mode for each action
	 * @type {Object<OperationInterface>}
	 */
	get default() {
		return {
			"click": this.clickOperation,
			"drag": this.dragOperation,
		};
	}
	/**
	 * @brief The default operation for left clicks
	 * @type {?OperationInterface}
	 */
	get clickOperation() {
		return undefined;
	}

	/**
	 * @brief The default operation for drag and drop
	 * @type {?OperationInterface}
	 */
	get dragOperation() {
		return undefined;
	}

	/**
	 * @brief (readonly) The current operation
	 * @type {OperationInterface}
	 */
	#operation;
	get operation() {
		return this.#operation;
	}

	/**
	 * @abstract
	 * @brief Identificator of a help message to tell the user how to interact with the current operation
	 * @type {string}
	 *
	 * @description The identificator helps identify what phrase to print the user.
	 * It should be something of the form "operation.<name>.step.<stepName>"
	 * If no operation is selected, the default is "operation.none"
	 */
	#help;
	get help() {
		return this.#help ? this.#help : "operation.none";
	}
	set help(id) {
		if (typeof id != typeof "") {
			throw new Error(`Expected help to be a string, got ${id? id.constructor.name : id}`);
		}
		this.#help = id;
		this.app.displayOperations();
	}

	/**
	 * @brief The main controller of the application
	 * @type {MCDG}
	 */
	#app;
	get app() {
		return this.#app;
	}
	set app(object) {
		if (!(object instanceof MCDG)) {
			throw new Error(`Expected app to be an instanceof MCDG, got ${object ? object.constructor.name : object}`);
		}
		this.#app = object;
	}
	/**
	 * @param {MCDG} app The main controller of the aplication
	 */
	constructor(app) {
		if (this.constructor.name === "ModeInterface") {
			throw new Error(`The ModeInterface is abstract and should not be implemented`);
		}
		this.app = app;
		this.#operation = null;
	}

	/**
	 * @brief Sets the mode to prepare a new operation
	 * @param {string} name: name of the operation to prepare
	 * @return {self}
	 *
	 * @note The help message is set to the 'init' step of the operation
	 */
	selectOperation(name) {
		if (typeof name !== typeof "") {
			try {
				name = this.getName(name);
			} catch {
				throw new Error(`The name of the operation should be a string, got ${name ? name.constructor.name : name}`);
			}
		}
		if (!(name in this.operations)) {
			throw new Error(`${name} is not a valid operation of ${this.constructor.name}`);
		}
		this.#operation = this.operations[name];
		this.#help = `operation.${name}.step.init`;
		return this;
	}

	/**
	 * @brief Returns the name of a specified operation in the context of this mode
	 * @param {OperationInterface} operation
	 * @return {string}
	 */
	getName(operation) {
		if (!(operation instanceof OperationInterface)) {
			throw new Error(`Expected first parameter to be an instance of OperationInterface, got ${operation ? operation.constructor.name : operation}`);
		}
		for (const name in this.operations) {
			if (this.operations[name] instanceof operation.constructor) {
				return name;
			}
		}
		throw new Error(`The ${operation.constructor.name} is not available in ${this.constructor.name}`);
	}

	/**
	 * @brief Cancels the current operation
	 *
	 * @description Cancels the current operation and resets the help message to default.
	 * @return {self}
	 */
	cancelOperation() {
		this.#operation = null;
		this.#help = undefined;
		return this;
	}

	/**
	 * @brief Resets the current operation to a fresh one
	 * @return {self}
	 */
	resetOperation() {
		this.selectOperation(this.getName(this.operation));
		return this;
	}

	/**
	 * @brief Performs actions that should be performed when a click occured
	 * @param {Point} point The position of the mouse in the scene when the click occured
	 * @return {self}
	 *
	 * @description By default, the default.click operation will be selected if no other operation was active.
	 * You should override this method to add custom logic for the specific operations of this mode.
	 */
	click(point) {
		if (!(point instanceof Point)) {
			throw new Error(`The point should be a Point, got ${point ? point.constructor.name : point}`);
		}
		if (!this.operation && this.default.click) {
			this.selectOperation(this.getName(this.default.click));
		}
		return this;
	}

	/**
	 * @brief Performs actions that should be performed when a drag action started
	 * @param {Point} point The position of the mouse in the scene at the start of the dragging
	 * @return {self}
	 *
	 * @description By default, the default.drag operation is initialized if no other operation was active,
	 * and the help message is set to the drag step either way
	 * You should override this method to add custom logic for the specific operations of this mode
	 */
	dragStart(point) {
		if (!(point instanceof Point)) {
			throw new Error(`The point should be a Point, got ${point ? point.constructor.name : point}`);
		}
		if (!this.operation && this.default.drag) {
			this.selectOperation(this.getName(this.default.drag));
		}
		if (this.operation) {
			this.help = `operation.${this.getName(this.operation)}.step.drag`;
		}
		this.app.displayOperations();
		return this;
	}

	/**
	 * @brief Registers the mouvement of the mouse during a dragging action
	 * @param {Point} point the new position of the mouse in the scene
	 * @return {self}
	 */
	move(point) {
		if (!(point instanceof Point)) {
			throw new Error(`The point should be a Point, got ${point ? point.constructor.name : point}`);
		}
		return this;
	}

	/**
	 * @brief Ends a dragging action
	 * @param {Point} point The position of the mouse in the scence at the end of dragging
	 * @return {self}
	 */
	dragEnd(point) {
		if (!(point instanceof Point)) {
			throw new Error(`The point should be a point, got ${point ? point.constructor.name : point}`);
		}
		return this;
	}
}
