import CreateLineAction from "../actions/createLineAction";
import OperationInterface from "./operationInterface";
import Point from "../../geometry/points/point";
import Line from "../../geometry/curves/line";

/**
 * @brief Creates a new line
 * @version 2.0
 * @date 2022-01-31
 * @author Catgolin
 */
export default class CreateLineOperation extends OperationInterface {
	/**
	 * @brief The starting point of the line
	 * @type {Point}
	 */
	#start;

	/**
	 * @override
	 * @inheritdoc
	 */
	get ready() {
		// TODO Check if there is a need to add intersection points
		return this.actions.length === 1;
	}

	/**
	 * @override
	 * @inheritdoc
	 * @description Expects two points.
	 * When the two points are given, creates a new line between those points and plans it's addition to the layer
	 */
	prepare(...elements) {
		if (elements.length < 1 || !(elements[0] instanceof Point)) {
			throw new Error(`Expected first element to be a Point, got ${elements.length < 1 ? undefined : elements[0]}`);
		}
		if (!this.#start) {
			this.#start = elements[0];
			if (elements.length > 1) {
				return this.prepare(elements.slice(1));
			}
		} else if (!this.ready) {
			const line = Line.fromPoints(this.#start, elements[0]);
			this.actions.push(new CreateLineAction(line));
		}
		return this;
	}
}
