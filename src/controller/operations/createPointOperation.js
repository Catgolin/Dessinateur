import CreatePointAction from "../actions/createPointAction";
import OperationInterface from "./operationInterface";
import Point from "../../geometry/points/point";

/**
 * @brief Creates a new point and adds it to the layer
 *
 * @description The prepare operation expects one point.
 * The point will be duplicated and put to the new layer.
 *
 * @version 2.0
 * @author Catgolin
 * @date 2022-01-30
 */
export default class CreatePointOperation extends OperationInterface {
	/**
	 * @override
	 * @inheritdoc
	 */
	get ready() {
		return this.actions.length === 1;
	}

	/**
	 * @override
	 * @inheritdoc
	 * @description Expects one point: a new point with the same coordinates will be created to be added to the layer.
	 */
	prepare(...elements) {
		if (elements.length < 1 || !(elements[0] instanceof Point)) {
			throw new Error(`Expected first element to be a Point, got ${elements.length < 1 ? undefined : elements[0]}`);
		}
		if (!this.ready) {
			const point = new Point(elements[0].x, elements[0].y);
			this.actions.push(new CreatePointAction(point));
		}
		return this;
	}
}
