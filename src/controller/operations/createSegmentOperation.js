import CreateSegmentAction from "../actions/createSegmentAction";
import CreatePointAction from "../actions/createPointAction";
import Point from "../../geometry/points/point";
import Segment from "../../geometry/curves/segment";
import OperationInterface from "./operationInterface";

/**
 * @brief Creates a new segment and eventually creates it's vertices
 * @version 2.0
 * @date 2022-01-31
 * @author Catgolin
 */
export default class CreateSegmentOperation extends OperationInterface {
	/**
	 * @brief The starting point of the segment
	 * @type {Point}
	 */
	#start;
	get start() {
		return this.#start;
	}
	set start(point) {
		this.#setPoint(point);
		this.#start = point;
	}

	/**
	 * @brief The end point of the segment
	 * @type {Point}
	 */
	#end;
	get end() {
		return this.#end;
	}
	set end(point) {
		this.#setPoint(point);
		this.#end = point;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get ready() {
		// TODO Check if there is a need to add intersection points
		return this.actions[this.actions.length - 1] instanceof CreateSegmentAction;
	}

	/**
	 * @override
	 * @inheritdoc
	 * @description Expects two points delimitating the two extremities of the segment.
	 * If the points are not in the scene, plans their addition to the layer.
	 * When the two points are set, creates a new segment between them and plans their addition to the layer.
	 */
	prepare(...elements) {
		if (elements.length < 1 || !(elements[0] instanceof Point)) {
			throw new Error(`Expected element to be a Point, got ${elements.length < 1 ? undefined : elements[0].constructor.name}`);
		}
		if (!this.start) {
			this.start = elements[0];
		} else if (!this.end) {
			this.end = elements[0];
			this.actions.push(new CreateSegmentAction(new Segment(this.start, this.end)));
		}
		if (elements.length > 1 && !this.end) {
			return this.prepare(elements.slice(1));
		}
		return this;
	}

	/**
	 * @brief Checks if the point is on the scene and adds a CreatePointAction if it is not
	 * @param {Point} point
	 */
	#setPoint(point) {
		if (!(point instanceof Point)) {
			throw new Error(`Expected a Point, got ${point ? point.constructor.name : point}`);
		}
		if (this.scene.layers[0].elements.find((element) => element instanceof Point && element.distance.contains(point)) === undefined) {
			this.actions.push(new CreatePointAction(point));
		}
	}
}
