import Collection from "../../collections/collection";
import Scene from "../../collections/scene";

/**
 * @abstract
 * @brief Describes a series of actions to perform on the scene when the user commanded a specific action
 * @description Encapsulates an operation at a level that would make sense for the user
 * @version 2.0
 * @author Catgolin
 * @date 2022-01-30
 */
export default class OperationInterface {
	/**
	 * @brief (readonly) The ordered list of actions to perform in this operation
	 * @type {Array<ActionInterface>}
	 */
	#actions;
	get actions() {
		return this.#actions;
	}

	/**
	 * @brief The scene on which this operation is to be performed
	 * @type {Scene}
	 */
	#scene;
	get scene() {
		return this.#scene;
	}
	set scene(object) {
		if (!(object instanceof Scene)) {
			throw new Error(`Expected scene to be an instanceof Scene, got ${object ? object.constructor.name : object}`);
		}
		this.#scene = object;
	}

	/**
	 * @brief Tells if the operation is ready to be executed without error
	 * @type {boolean}
	 */
	get ready() {
		throw new Error(`${this.constructor.name} should implement get ready`);
	}

	/**
	 * @param {?Scene} scene The scene on which this operation should be performed
	 */
	constructor(scene) {
		if (this.constructor === OperationInterface) {
			throw new Error("OperationInterface is abstract and should not be instanciated");
		}
		this.scene = scene;
		this.#actions = [];
	}

	/**
	 * @abstract
	 * @brief Builds the list of actions to perform in regards to the given elements
	 * @param {Array<GeometryInterface>} elements Elements that need to be taken into account for this operation.
	 * Each operation should expect elements to be given in a specific order.
	 * @return {self}
	 */
	prepare(elements) {
		throw new Error(`${this.constructor.name} should implement prepare`);
	}

	/**
	 * @brief Executes each action in the order with the specified layers
	 * TODO See how we should specify the right layers
	 */
	execute() {
		if (!this.ready) {
			throw new Error(`The operation is not ready yet`);
		}
		this.actions.forEach((action) => action.execute(this.scene.layers[0]));
	}
}
