import GeometryInterface from "../geometry/geometryInterface";
import ScreenPoint from "../vue/screenPoint";
import MCDG from "../index";
import ComponentInterface, {
	STATE
} from "../vue/gui/components/componentInterface";

/**
 * @brief Links user events (click, etc.) to the appropriate controllers
 *
 * Summary:
 * 1. Click events
 * 2. Drag events
 * 3. Other mouse events (hover, scroll)
 *
 * @version 2.0
 */
export default class UserInuptController {
	/**
	 * @brief (readonly) The main class of the application
	 * @type {MCDG}
	 */
	#controller;
	get controller() {
		return this.#controller;
	}

	/**
	 * @brief The position of the cursor in the canvas when the drag started
	 * @public
	 * @type {?ScreenPoint}
	 *
	 * @description
	 * Returns undefined if the user is not curently dragging the cursor, or returns the position on the canvas where it started dragging if it is.
	 * The dragStartPosition is updated when the dragStartPoint is.
	 *
	 * @see dragStartPosition
	 */
	#dragStartPoint;
	get dragStartPoint() {
		return this.#dragStartPoint;
	}
	set dragStartPoint(point) {
		if (!(point === undefined || point instanceof ScreenPoint)) {
			throw new Error(`Expected dragStartPoint to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		this.#dragStartPoint = point;
		if (point) {
			this.#dragStartPosition = this.controller.camera.canvasToScene(point, this.controller.canvas);
		} else {
			this.#dragStartPosition = undefined;
		}
	}
	/**
	 * @brief (readonly) Position of the cursor in the scene where the dragging action started
	 * @type {?Point}
	 *
	 * @description
	 * Returns undefined if the user is not curently dragging the cursor, or returns the position in the scene where it started dragging if it is.
	 * This value is on readonly as it is updated with dragStartPoint.
	 *
	 * @note Updated in set dragStartPoint and not in get dragStartPosition to avoid errors if the camera moved after the dragging started
	 * @see dragStartPoint
	 */
	#dragStartPosition;
	get dragStartPosition() {
		return this.#dragStartPosition;
	}

	/**
	 * @brief (readonly) Is there a dragging action taking place
	 * @type {boolean}
	 *
	 * @note Setting the dragging value to false will reset the dragStartPoint to undefined
	 *
	 * @see dragStartPoint
	 */
	get dragging() {
		return this.dragStartPoint instanceof ScreenPoint;
	}
	set dragging(value) {
		if (value !== false) {
			throw new Error(`The dragging propertie can only be set manually to false, got ${value}`);
		}
		this.dragStartPoint = undefined;
	}

	/**
	 * @brief Position of the last click on the canvas
	 * @public
	 * @type {?ScreenPoint}
	 *
	 * @description
	 * Returns the position of the cursor in the canvas on the last mouseDown event of the main button (left click)
	 * When this value is set, it updates the lastClickPosition
	 * @see lastClickPosition
	 */
	#lastClick;
	get lastClick() {
		return this.#lastClick;
	}
	set lastClick(point) {
		if (!(point === undefined || point instanceof ScreenPoint)) {
			throw new Error(`Expected click point to be a Screen Point, got ${point ? point.constructor.name : point}`);
		}
		this.#lastClick = point;
		if (point) {
			this.#lastClickPosition = this.controller.camera.canvasToScene(point, this.controller.canvas);
		} else {
			this.#lastClickPosition = undefined;
		}
	}

	/**
	 * @brief (readonly) Position of the last click on the scene
	 * @type {?Point}
	 *
	 * @description
	 * Returns the position of the cursor in the scene on the last mouseDown event of the main button (left click)
	 *
	 * @note This value is updated in the setter of lastClick. It is not calculated on the fly with lastClick because the camera might have moved
	 */
	#lastClickPosition;
	get lastClickPosition() {
		return this.#lastClickPosition;
	}

	/**
	 * @brief The component of the menu that is currently being interacted with
	 * @public
	 * @type {?ComponentInterface}
	 */
	#targetComponent;
	get targetComponent() {
		return this.#targetComponent;
	}
	set targetComponent(object) {
		if (object && !(object instanceof ComponentInterface)) {
			throw new Error(`Expected targetComponent to implement ComponentInterface, got ${object ? object.constructor.name : object}`);
		}
		this.#targetComponent = object;
	}

	/**
	 * @brief The element of the menu currently beeing hoovered
	 * @public
	 * @type {?ComponentInterface}
	 */
	#hovered;
	get hovered() {
		return this.#hovered;
	}
	set hovered(object) {
		if (object && !(object instanceof ComponentInterface)) {
			throw new Error(`Expected hovered object to be a ComponentInterface, got ${object ? object.constructor.name : object}`);
		}
		this.#hovered = object;
	}

	/**
	 * @param {MCDG} controller: the main controller object
	 */
	constructor(controller) {
		this.#controller = controller;
	}

	/* ===============
	 * 1. Click events
	 * ==============*/

	/**
	 * @brief Gets the position of the cursor at the begining of a click
	 *
	 * @description Called on mouseDown or equivalent, it stores the information of the position of the cursror for later use.
	 * It also removes the tooltip of the component being clicked on if it had been drawn by the hovering of the mouse.
	 *
	 * @note The actions are not triggered because we don't know yet  if the user is dragging, clicking, or else.
	 *
	 * @param {ScreenPoint} point: position of the cursor when the click event started
	 */
	prepareClick(point) {
		if (!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		this.lastClick = point;
		this.targetComponent = this.controller.menu.collision(point);
		if (this.targetComponent instanceof ComponentInterface) {
			this.targetComponent.tooltip.remove();
			this.targetComponent.mouseDown();
			this.targetComponent.draw();
		}
	}

	/**
	 * @brief Triggers the action associated with a click
	 *
	 * @description
	 * After the click was prepared, executes the appropriate actions and resets this controller to manage the next click events.
	 * The given position of the cursor will be compared to the position of the cursor in the last prepareClick:
	 * - if the two positions are not close enough, then the click action should not be performed and should be replaced by a drag event
	 *
	 * @param {ScreenPoint} point: position of the cursor
	 *
	 * @see prepareClick
	 */
	leftClick(point) {
		if (!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		if (this.targetComponent instanceof ComponentInterface) {
			this.targetComponent.click();
			this.targetComponent.mouseUp();
			this.controller.draw();
		} else {
			this.controller.mode.click(this.lastClickPosition);
		}
	}

	/* ===============
	 * 2. Drag events
	 * ==============*/

	/**
	 * @brief Starts a dragging action
	 *
	 * @param {ScreenPoint} point: position of the mouse at the start of the drag
	 *
	 * @return {boolean} Whether or not the dragging comportenement should start
	 */
	dragStart(point) {
		if (!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		this.dragStartPoint = point;
		if (!(this.targetComponent instanceof ComponentInterface)) {
			this.controller.mode.dragStart(this.dragStartPosition);
			return false;
		}
		if (this.targetComponent.isActive) {
			this.targetComponent.state -= STATE.ACTIVE;
			this.controller.draw();
		}
		return true;
	}

	/**
	 * @brief Registers an update in an ongoing dragging action
	 *
	 * @param {ScreenPoint} start: position of the mouse at the start of dragging
	 * @param {ScreenPoint} point: current position of the mouse
	 */
	dragMove(start, point) {
		if (!(start instanceof ScreenPoint)) {
			throw new Error(`Expected starting point to be a ScreenPoint, got ${start ? start.constructor.name : start}`);
		}
		if (!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		if (this.dragStartPoint !== start) {
			this.dragStart(start);
		}
		// @todo Delegate actions to the tool currently selected
	}

	/**
	 * @brief Ends a dragging action and validate it's effects
	 *
	 * @param {ScreenPoint} point: position of the mouse at the end of drag
	 */
	dragEnd(point) {
		this.dragging = false;
		// @todo Delegate actions to the tool currently selected
	}

	/**
	 * @brief Ends a dragging action without validating it's effects.
	 *
	 * @description This is called when the dragging action is cancelled
	 * either by exiting the canvas, pressing escape, or else.
	 *
	 * @param {ScreenPoint} point: position of the mouse at the moment of cancel
	 */
	dragCancel(point) {
		this.dragging = false;
		// @todo Delegate actions to the tool currently selected
	}

	/* =====================
	 * 3. Other mouse events
	 * ====================*/

	/**
	 * @brief Called when the mouse moved inside of the canvas
	 *
	 * @description Checks if an element of the menu is beeing hovered
	 *
	 * @param {ScreenPöint} point: current position of the mouse
	 */
	hover(point) {
		const hovered = this.controller.menu.collision(point);
		let needsRedraw = false;
		// The previously hovered element is not hovered anymore
		if (this.hovered instanceof ComponentInterface && this.hovered !== hovered) {
			needsRedraw |= this.hovered.cancelHovering();
			if (this.hovered === this.targetComponent) {
				needsRedraw |= this.targetComponent.mouseUp();
			}
		}
		// Updating the new hovered element
		if (hovered instanceof ComponentInterface) {
			needsRedraw |= hovered.hover(point);
			if (this.hovered !== hovered && hovered === this.targetComponent) {
				needsRedraw |= this.targetComponent.mouseDown();
			}
		}
		this.hovered = hovered;
		// Drawing
		/*
		if (needsRedraw) {
			this.controller.draw();
		}
		*/
		this.controller.draw();
	}

	/**
	 * @brief Called when the user is zooming or rotating,
	 * for instance with the mouse wheel or with touch events.
	 *
	 * @todo Update the camera
	 *
	 * @param {number} scale
	 * @param {number} rotation
	 */
	scroll(scale, rotation) {
		console.log("Scrolling for scale " + scale + " and rotation " + rotation);
	}

}
