import GeometryInterface from "../geometryInterface";
import LineDistance from "../../physics/distances/lineDistance";
import Point from "../points/point.js";

/**
 * @class Line
 *
 * @brief Describes an infinite line
 *
 * The line is describe by the Cartesian equation of the form:
 * ax+by+c = 0
 *
 * @version 2.0
 */
export default class Line extends GeometryInterface {

	/**
	 * @brief Factor for x in the cartesian equation
	 *
	 * @type {number}
	 */
	#a;

	/**
	 * @brief Factor for y in the cartesian equation
	 *
	 * @type {number}
	 */
	#b;

	/**
	 * @brief Constant factor of the cartesian equation
	 *
	 * @type {number}
	 */
	#c;

	/**
	 * @brief Creates a line using the parameters of the cartesian equation
	 *
	 * @param {number} a: factor for x in the cartesian equation
	 * @param {number} b: factor for y in the cartesian equation
	 * @param {number} c: constant factor of the cartesian equation
	 */
	constructor(a, b, c) {
		super();
		if (typeof a !== "number" || typeof b !== "number" || typeof c !== "number" || isNaN(a + b + c)) {
			throw new Error(`Expected three numbers as cartesian coefficients but got ${typeof a}, ${typeof b}, ${typeof c} (${a+b+c})`);
		}
		if (a === 0 && b === 0) {
			throw new Error("a and b cannot both equal 0");
		}
		this.#a = a;
		this.#b = b;
		this.#c = c;

	}

	/**
	 * @brief Creates a new line passing by two defined points
	 *
	 * @param {Point} start: The first point used to define the line
	 * @param {Point} end: The second point used to define the line
	 *
	 * @returns {Line} A new line passing by the two specified points
	 *
	 * @throws {Error} if start and end are located at the same position
	 *
	 * Complexity: constant
	 * Sources of rounding errors:
	 * - substraction of coordinates and multiplication by the difference
	 */
	static fromPoints(start, end) {
		if (!start || !end || !(start instanceof Point) || !(end instanceof Point)) {
			throw Error(`Expected two Points, got ${start ? start.constructor.name : start} and ${end ? end.constructor.name : end}`);
		}
		if (start.distance.contains(end)) {
			throw Error(`Can't create a line with two points located at the same position at (${start.x}, ${start.y})`);
		}
		const a = start.y - end.y;
		const b = end.x - start.x;
		const c = -(start.x * a + start.y * b);
		return new Line(a, b, c);
	}

	/**
	 * @brief Cartesian coefficients of the equation of the form ax+by+c=0
	 * @type {{a: number, b: number, c: number}}
	 */
	get cartesianCoefficients() {
		return {
			a: this.#a,
			b: this.#b,
			c: this.#c,
		};
	}

	/**
	 * @inheritdoc
	 * @type {LineDistance}
	 */
	get distance() {
		return new LineDistance(this);
	}
}
