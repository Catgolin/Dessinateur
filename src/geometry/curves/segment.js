import PointCollision from "../../physics/collisions/pointCollision";
import GeometryInterface from "../geometryInterface";
import SegmentDistance from "../../physics/distances/segmentDistance";
import Line from "./line";

/**
 * @class Segment
 *
 * @brief Describes a finite line delimitated by two points
 *
 * @version 2.0
 */
export default class Segment extends GeometryInterface {

	/**
	 * @brief The two extermities of the segment
	 *
	 * @type {Array<Point>}
	 */
	#vertices;

	/**
	 * @breif The line carrying this segment
	 *
	 * @type {Line}
	 */
	#line;

	/**
	 * @param {Point} start
	 * @param {Point} end
	 *
	 * @throws {Error} if start and end are located at the same position
	 */
	constructor(start, end) {
		super();
		this.#vertices = [start, end];
		this.#line = Line.fromPoints(start, end);
	}

	/**
	 * Start and end point of this line segment
	 * @type {array}
	 */
	get vertices() {
		return this.#vertices;
	}

	/**
	 * The infinite line extending this segment
	 * @type {Line}
	 */
	get line() {
		return this.#line;
	}

	/**
	 * @inheritdoc
	 * @type {SegmentDistance}
	 */
	get distance() {
		return new SegmentDistance(this);
	}

	/**
	 * @brief Cartesian coefficients of the equation, of the form ax+by+c=0
	 * @type {{a: number, b: number, c: number}}
	 */
	get cartesianCoefficients() {
		return this.line.cartesianCoefficients;
	}
}
