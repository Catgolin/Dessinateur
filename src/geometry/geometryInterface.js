import DistanceInterface from "../physics/distances/distanceInterface";

/**
 * @class GeometryInterface
 * @abstract
 *
 * @brief Represents any geometric element (line, point, curve, etc.)
 *
 * @version 2.0
 */
export default class GeometryInterface {

	/**
	 * @brief Number of objects created
	 * Used to generate id
	 * @type {number}
	 */
	static number = 0;

	/**
	 * @brief (Read only) Unique identifier of this object
	 * @type {number}
	 */
	#id;

	constructor() {
		if (this.constructor === GeometryInterface) {
			throw Error("GeometryInterface is abstract and should not be instanciated");
		}
		this.#id = GeometryInterface.number;
		GeometryInterface.number++;
	}

	/**
	 * @brief Unique identifier of this element
	 * @type {string}
	 */
	get id() {
		return this.#id;
	}

	/**
	 * @brief Human-readable identifier of this element
	 *
	 * By default, the name is the identifier of this element
	 * @type {string}
	 */
	get name() {
		return typeof this._name === "string" ? this._name : this.id;
	}
	/**
	 * @param {string} name: a human-readable name for this element
	 */
	set name(name) {
		if (typeof name !== "string") {
			throw Error("Expected name to be a string, " + typeof name + " given");
		}
		this._name = name;
	}

	/**
	 * @brief An object set to compare points to this element
	 * @type {DistanceInterface}
	 */
	get distance() {
		throw Error(this.constructor.name + " should instanciate distance");
	}
}
