import PieceDistance from "../../physics/distances/pieceDistance";
import Segment from "../curves/segment";
import GeometryInterface from "../geometryInterface";
import Point from "../points/point";

/**
 * @class Piece
 *
 * @brief Describes a polygone.
 *
 * The polygon is identified by it's edges who determine it's vertices.
 *
 * @version 2.0
 */
export default class Piece extends GeometryInterface {
	/**
	 * @brief (Read-only) The edges of the polygon
	 * @type {Array<Segment>}
	 */
	#edges;

	/**
	 * @param {Array<Segment>} edges
	 */
	constructor(...edges) {
		super();
		if(!Array.isArray(edges) || edges.length < 3) {
			throw Error("You shall give edges to create a piece");
		}
		edges.forEach((edge) => {
			if(!(edge instanceof Segment)) {
				throw Error(`Expected edge to be a Segment, got ${typeof edge}`);
			}
		});
		this.#edges = edges;
		if (this.notClosed(edges)) {
			throw Error("The piece was not closed");
		}

	}
	/**
	 * @type {Array<Segment>}
	 * @see #edges
	 */
	get edges() {
		return this.#edges;
	}

	/**
	 * @type {Array<Point>}
	 * Complexity: O(e) with e the number of edges
	 */
	get vertices() {
		const vertices = [];
		this.edges.forEach(
			(edge) => edge.vertices.forEach(
				(point) => {
					const contained = vertices.findIndex(
						(vertice) => vertice.distance.contains(point)
					);
					if(contained === -1) {
						vertices.push(new Point(point.x, point.y));
					}
				}
			)
		);
		return vertices;
	}

	get distance() {
		return new PieceDistance(this);
	}

	notClosed(edges) {
		return this.vertices.length !== this.edges.length;
	}
}
