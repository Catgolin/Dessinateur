import PointDistance from "../../physics/distances/pointDistance";
import GeometryInterface from "../geometryInterface";

/**
 * @class Point
 *
 * @brief Describes a simple point on the plane.
 *
 * A point is described by it's coordinates (x, y).
 *
 * @version 2.0
 */
export default class Point extends GeometryInterface {

	/**
	 * @brief Abscissa of this point
	 * @public
	 * @type {number}
	 */
	#x;

	/**
	 * @brief Ordinate of this point
	 * @public
	 * @type {number}
	 */
	#y;

	/**
	 * @param {number} x: Abscissa of this point
	 * @param {number} y: Ordinate of this point
	 */
	constructor(x, y) {
		super("Point");
		this.x = x;
		this.y = y;
	}

	/**
	 * @param {number} x
	 */
	set x(x) {
		if (typeof x !== "number") {
			throw Error(`Expected x to be a number, ${typeof x} given`);
		}
		if (isNaN(x)) {
			throw Error(`Expected x to be a number, ${x} given`);
		}
		this.#x = x;
	}

	/**
	 * @param {number} y
	 */
	set y(y) {
		if (typeof y !== "number") {
			throw Error(`Expected y to be a number, ${typeof y} given`);
		}
		if (isNaN(y)) {
			throw Error(`Expected y to be a number, ${y} given`);
		}
		this.#y = y;
	}

	/**
	 * @type {number}
	 */
	get x() {
		return this.#x;
	}

	/**
	 * @type {number}
	 */
	get y() {
		return this.#y;
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get distance() {
		return new PointDistance(this);
	}
}
