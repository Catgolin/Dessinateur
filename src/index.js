import Camera from "./vue/camera.js";
import GUI from "./vue/gui/containers/gui.js";
import Translator from "./vue/translator.js";
import UserInputController from "./controller/userInputController.js";
import MouseInputManager from "./vue/userInput/mouseInputManager.js";
import Scene from "./collections/scene.js";
import Point from "./geometry/points/point";
import Segment from "./geometry/curves/segment";
import Line from "./geometry/curves/line";
import ModeInterface from "./controller/modes/modeInterface";
import DrawMode from "./controller/modes/drawMode";

/**
 * @class MCDG
 * @brief The main controller of the application
 * @date 2022-01-30
 * @author Catgolin
 * @version 2.0
 */
export default class MCDG {
	/**
	 * @brief (readonly) The object responsible to print human-readable texts
	 * @type {Translator}
	 */
	get translator() {
		return new Translator();
	}

	/**
	 * @brief The scene containing all the geometric objects in layers
	 * @public
	 * @type {Scence}
	 */
	#scene;
	get scene() {
		return this.#scene;
	}
	set scene(object) {
		if (!(object instanceof Scene)) {
			throw new Error(`Expected scene to be a Scence, got ${object ? object.constructor.name : object}`);
		}
		this.#scene = object;
	}

	/**
	 * @brief The camera responsible for printing objects on the canvas
	 * @public
	 * @type {Camera}
	 */
	#camera;
	get camera() {
		return this.#camera;
	}
	set camera(object) {
		if (!(object instanceof Camera)) {
			throw new Error(`Expected camera to be an instance of Camera, got ${object? object.constructor.name : object}`);
		}
		this.#camera = object;
	}

	/**
	 * @brief The menu printed to the user
	 * @public
	 * @type {GUI}
	 */
	#menu;
	get menu() {
		return this.#menu;
	}
	set menu(object) {
		if (!(object instanceof GUI)) {
			throw new Error(`Expected the menu to be an instance of GUI, got ${object ? object.constructor.name : object}`);
		}
		this.#menu = object;
	}

	/**
	 * @brief (readonly) Object responsible to manage inputs
	 * @type {UserInputController}
	 */
	#inputManager;
	get inputManager() {
		return this.#inputManager;
	}

	/**
	 * @brief The object linked to the mouseEvents
	 * @type {MouseInputManager}
	 *
	 * @note Only set if the canvas has been defined
	 */
	#mouseManager;

	/**
	 * @brief The canvas on which the application is drawn
	 * @public
	 * @type {HTMLCanvasElement}
	 *
	 * @note The menu and mouseManager are updated when when the canvas is set
	 */
	#canvas;
	get canvas() {
		return this.#canvas;
	}
	set canvas(object) {
		if (!(object instanceof HTMLCanvasElement)) {
			throw new Error(`Expected canvas to be a HTMLCanvasElement, got ${object ? object.constructor.name : object}`);
		}
		this.#canvas = object;
		if (this.menu) {
			this.menu.parent = object;
		}
		this.#mouseManager = new MouseInputManager(this.inputManager, object);
	}

	/**
	 * @brief Tells what mode we are in
	 * @public
	 * @type {ModeInterface}
	 */
	#mode;
	get mode() {
		return this.#mode;
	}
	set mode(object) {
		if (!(object instanceof ModeInterface)) {
			throw new Error(`Expected mode to be an instance of ModeInterface, got ${object ? object.constructor.name : object}`);
		}
		this.#mode = object;
	}

	/**
	 * @param {HTMLCanvasElement} canvas The html element in which the application will be displayed
	 * @param {Scene} scene The scene to print. If null or undefined, a new empty scene is created
	 * @param {Camera} camera The camera used to print the scene. If null or undefined, a new camera is created
	 */
	constructor(canvas, scene = null, camera = null) {
		this.#inputManager = new UserInputController(this);
		this.scene = scene instanceof Scene ? scene : new Scene();
		this.camera = camera instanceof Camera ? camera : new Camera();
		this.mode = new DrawMode(this);
		this.resetMenu();
		if (canvas) {
			this.canvas = canvas;
			this.draw();
		}
	}

	/**
	 * @brief Creates a new menu with the default elements
	 * @return {GUI}
	 */
	resetMenu() {
		this.menu = new GUI(this.canvas, this.translator);
		this.menu.right.top.add.button("mode.dessin", (function () {
			this.mode = new DrawMode(this);
			this.displayOperations();
		}).bind(this));
		this.displayOperations();
		return this.menu;
	}

	/**
	 * @brief Draws the scene and the menu on the current canvas
	 */
	draw() {
		const ctx = this.canvas.getContext("2d");
		ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.camera.draw(this.scene, this.canvas);
		this.displayOperations();
		this.menu.draw();
	}

	/**
	 * @brief Adds elements in the menu to help the user navigate the operations
	 *
	 * @description
	 * - Adds buttons in the top left corner to display the available operations
	 * - Adds a help message on the top side of the canvas to tell what is the next step
	 * - Adds a cancel button on the bottom side of the canvas
	 * - removes the previous elements if they are not needed anymore
	 */
	displayOperations() {
		for (const name in this.mode.operations) {
			if (!this.menu.left.top.components.find((element) => element.descriptionId === `operation.${name}`)) {
				this.menu.left.top.add.button(`operation.${name}`, (() => this.mode.selectOperation(name) && this.displayOperations()).bind(this));
			}
		}
		if (!this.mode.operation) {
			this.menu.top.center.empty();
			this.menu.bottom.center.empty();
		} else {
			if (this.menu.top.center.components.length < 1) {
				this.menu.top.center.add.message(this.mode.help);
			}
			this.menu.top.center.components[0].descriptionId = this.mode.help;
			if (this.menu.bottom.center.components.length < 1) {
				this.menu.bottom.center.add.button("cancel", (() => this.mode.cancelOperation() && this.displayOperations()).bind(this));
			}
		}
	}

}

export {
	Camera,
	Point,
	Segment,
	Line
};
