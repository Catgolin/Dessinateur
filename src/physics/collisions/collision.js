import Collection from "../../collections/collection";
import GeometryInterface from "../../geometry/geometryInterface";

/**
 * @class Collision
 * @abstract
 *
 * @brief Detects interactions between a collection of objects of the same type.
 *
 * @version 2.0
 */
export default class Collision {
	/**
	 * @brief The type of Geometry managed by this collision class
	 * @public
	 * @type {prototype}
	 */
	#type;

	#elements;

	/**
	 * @param {function} type: constructor of the type of elements managed by this collision class
	 * @throws {Error} if type does not extend GeometryInterface
	 */
	constructor(type) {
		if (this.constructor === Collision) {
			throw Error("Collision is abstract and should not be instanciated");
		}
		this.#elements = [];
		this.type = type;
	}

	/**
	 * @param {function} type: should extend GeometryInterface
	 * Complexity: O(n) with n the level of inheritance from GeometryInterface
	 */
	set type(type) {
		let proto = type.prototype;
		while (proto !== null) {
			if (proto.constructor === GeometryInterface)
				return this.#type = type;
			proto = proto.__proto__;
		}
		throw Error(
			type.prototype.constructor.name + " doesn't extend GeometryInterface"
		);
	}
	get type() {
		return this.#type;
	}

	/**
	 * @type {Array<GeometryInterface>}
	 */
	get elements() {
		return this.#elements;
	}

	/**
	 * Adds new elements to the list of elements being considered
	 *
	 * @param {...GeometryInterface} elements
	 *
	 * @returns {Collision} this
	 *
	 * Complexity: O(n) with n the number of elements to add
	 */
	add(...elements) {
		for (let element of elements) {
			if (!(element instanceof this.type)) {
				throw Error(
					this.constructor.name + " only manages " +
					this.type.prototype.constructor.name + " elements, got " +
					element.constructor.name
				);
			}
		}
		this.elements.push(...elements);
		return this;
	}

	/**
	 * The list of collision points
	 * between the observed elements,
	 * taking their borders into account.
	 * @type {Array<Point>}
	 * @abstract
	 */
	get collisions() {
		throw Error(this.constructor.name + " should implement collisions");
	}

	/**
	 * The intersection ensemble between the observed elements,
	 * taking their borders into account.
	 * @type {Collection}
	 * @abstract
	 */
	get intersection() {
		throw Error(this.constructor.name + " should implement intersection");
	}

	/**
	 * The list of collision points between the observed elements,
	 * not counting borders of the elements
	 * @type {Array<Point>}
	 * @abstract
	 */
	get strictCollisions() {
		return this.collisions;
	}

	/**
	 * The intersection ensemble between the observed element,
	 * not including their boreders
	 * @type {Collection}
	 * @abstract
	 */
	get strictIntersection() {
		return this.strictIntersection;
	}

	/**
	 * Classes of objects sharing an equivalence class
	 * @description
	 * Each element of the arrays contains only the observed objects
	 * that are parallel to each other.
	 * @type {Array<Collection>}
	 * @abstract
	 */
	get equivalence() {
		throw Error(this.constructor.name = " should implement parallels");
	}
}
