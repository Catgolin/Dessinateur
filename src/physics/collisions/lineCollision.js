import Collision from "./collision";
import Line from "../../geometry/curves/line";
import Point from "../../geometry/points/point";
import Collection from "../../collections/collection";

/**
 * @class LineCollision
 *
 * @brief Describes the collisions and intersection with the line
 *
 * @version 2.0
 *
 * @see Collision
 */
export default class LineCollision extends Collision {

	/**
	 * @brief The intersection between the selected elements
	 * @var {Collection} #intersection
	 */
	#intersection;

	/**
	 * @param {Array<Line>} lines: the lines to consider in this collision
	 *
	 * Complexity: O(line.length**2)
	 */
	constructor(...lines) {
		super(Line);
		this.#intersection = new Collection();
		this.add(...lines);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(n) with n the number of elements previously added
	 */
	add(...elements) {
		elements.forEach(element => this.#push(element));
		return this;
	}

	/**
	 * @private
	 *
	 * @brief Adds one element at a time and updates the intersection
	 *
	 * @param {Line} line: the new line to add to the list of elements to consider
	 *
	 * @see #intersection
	 *
	 * Complexity: O(n) with n the number of elements already added
	 */
	#push(line) {
		if (this.elements.length === 0) {
			/* As this is the only time we add a line,
			   and the only other time something is added to the intersection,
			   a line have to be removed,
			   this ensures that there is never more than one element in the intersection
			*/
			this.#intersection.add(line);
		}
		super.add(line);
		// Remove elements of the intersection that are not included in the new line.
		this.#intersection.elements.forEach(
			element => {
				if (element instanceof Point && !line.distance.contains(element)) {
					// The point is not on the line
					this.#intersection.remove(element);
				} else if (element instanceof Line && this.#collisionBetweenTwo(element, line)) {
					// Two lines are not parallel, replace with intersection point
					this.#intersection.remove(element);
					this.#intersection.add(this.#collisionBetweenTwo(element, line));
				} else if (element instanceof Line && this.equivalence.length === 1) {
					// Two lines are parallel, check if they are merged
					if (!line.distance.contains(this.#buildPoint(element))) {
						this.#intersection.remove(element);
					}
				}
			}
		);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(this.equivalence.length**2)
	 */
	get collisions() {
		if (this.equivalence.length < 2) {
			return [];
		}
		const collisions = [];
		for (let i = 0; i < this.equivalence.length; i++) {
			const firstClass = this.equivalence[i];
			for (let j = i + 1; j < this.equivalence.length; j++) {
				const secondClass = this.equivalence[j];
				collisions.push(...this.#collisionBetweenClasses(firstClass, secondClass));
			}
		}
		// Remove duplicates
		return collisions.filter(
			(point, i) =>
			collisions.findIndex(p => p.distance.contains(point)) === i
		);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get intersection() {
		return this.#intersection;
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(elements.length)
	 */
	get equivalence() {
		/// @var {Array<Collection>>} eqClasses: equivalence classes regrouping lines that are parallel to each other
		const eqClasses = [];
		this.elements.forEach(
			(line) => {
				// Find where the element belongs in the classes of parallel lines already computed
				const i = eqClasses.findIndex(
					(eqClass) => {
						return this.#collisionBetweenTwo(line, eqClass.elements[0]) === null;
					}, this
				);
				// Include the element in the class it belongs, with lines parallel to it
				if (i < 0) {
					eqClasses.push(new Collection(line));
				} else {
					eqClasses[i].add(line);
				}
			}, this
		);
		return eqClasses;
	}

	/**
	 * @private
	 *
	 * @brief Returns the list of collision points between the lines of two equivalence classes
	 *
	 * @param {Collection} first: first class of parallel lines
	 * @param {Collection} second: second class of parallel lines
	 *
	 * @return {Array<Point>}
	 *
	 * @see collisions
	 *
	 * Complexity: O(first.length + second.length)
	 */
	#collisionBetweenClasses(first, second) {
		const collisions = [];
		first.elements.forEach(
			(line1) => {
				second.elements.forEach(
					(line2) => {
						// As line1 and line2 belong to different classes, they have collision point
						collisions.push(this.#collisionBetweenTwo(line1, line2));
					}, this
				);
			}, this
		);
		return collisions;
	}

	/**
	 * @private
	 *
	 * @param {Line} l1
	 * @param {Line} l2
	 *
	 * @returns {?Point}: null if l1 and l2 are parallel, or their intersection point
	 *
	 * Complexity: constant
	 * Sources of rounding errors:
	 * - Substraction of products of coefficients
	 * - Division by the determinant of the coefficients matrix
	 */
	#collisionBetweenTwo(l1, l2) {
		// Check if the two lines are parallel
		const {
			a: a1,
			b: b1,
			c: c1
		} = l1.cartesianCoefficients;
		const {
			a: a2,
			b: b2,
			c: c2
		} = l2.cartesianCoefficients;
		const det = (a1 * b2) - (b1 * a2);
		if (det === 0)
			return null;
		// Build the intersection point
		const x = (c2 * b1 - c1 * b2) / det;
		const y = (a2 * c1 - a1 * c2) / det;
		return new Point(x, y);
	}

	/**
	 * @private
	 *
	 * @brief Builds a random point on the line
	 *
	 * @param {Line} line: the line on which the point should be built
	 *
	 * @return {Point}
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding error:
	 * - division by a cartesian coefficient of the line
	 */
	#buildPoint(line) {
		const {
			a,
			b,
			c
		} = line.cartesianCoefficients;
		if (b == 0) {
			return new Point(-c / a, Math.random());
		}
		const x = Math.random();
		const y = -(a * x + c) / b;
		return new Point(x, y);
	}
}
