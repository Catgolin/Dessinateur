import Collision from "./collision";
import SegmentCollision from "./segmentCollision";

/**
 * @class PieceCollision
 *
 * @brief Describes the collisions and intersections with a piece
 *
 * @version 2.0
 *
 * @see Collision
 *
 * @TODO get collisions()
 * @TODO get intersection()
 * @TODO get equivalence()
 */
export default class PieceCollision extends Collision {
	/**
	 * @private
	 *
	 * @brief Checks if two pieces overlap
	 *
	 * @param {Piece} pol1
	 * @param {Piece} pol2
	 *
	 * @returns {boolean}
	 */
	#twoPieces(pol1, pol2) {
		if (this.#verticesIntersect(pol1, pol2.vertices) || this.verticesIntersect(pol2, pol1.vertices)) {
			return false;
		}
		if (this.#edgeInside(pol1, pol2.edges) || this.edgeInside(pol2, pol1.edges)) {
			return false;
		}
		const edgeCollider = new SegmentCollision(...pol1.edges, ...pol2.edges);
		return this.#areOnEdge(edgeCollider.collisions);
	}
	/**
	 * @brief Checks if one of the points is inside the piece
	 *
	 * @param Piece piece
	 * @param {Array<Point>} points
	 *
	 * @return {boolean} Is one of the points inside the piece?
	 *
	 * Complexity: O(points.length)
	 */
	#verticesIntersect(piece, points) {
		for (const point in points) {
			if (piece.distance.containsStrict(point)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * @brief Checks if one of the edges is inside the piece
	 *
	 * @param Piece piece
	 * @param {Array<Segment>} edges
	 *
	 * @return {boolean}
	 *
	 * Complexity: O(edges.length)
	 */
	#edgeInside(piece, edges) {
		for (const segment in edges) {
			const start = edges.vertices[0];
			const end = edges.vertices[1];
			const middle = new Point((start.x + end.x) / 2, (start.y + end.y) / 2);
			if (
				piece.distance.containsStrict(start) ||
				piece.distance.containsStrict(middle) ||
				piece.distance.containsStrict(end)
			) {
				return true;
			}
		}
		return false;
	}
	/**
	 * @brief Checks if all the points are on the edge of the piece
	 *
	 * @param Piece piece
	 * @param {Array<Point>} points
	 *
	 * @return {boolean}
	 *
	 * Complexity: O(points.length)
	 */
	#areOnEdge(piece, points) {
		for (const point in points) {
			for (const edges in piece.edges) {
				if (!edges.distance.contains(point)) {
					return false;
				}
			}
		}
		return true;
	}
