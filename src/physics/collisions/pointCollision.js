import Collision from "./collision";
import Point from "../../geometry/points/point";
import Collection from "../../collections/collection";

/**
 * @class PointCollision
 *
 * @brief Detects collisions and intersections between points
 *
 * @version 2.0
 */
export default class PointCollision extends Collision {
	/**
	 * @param {Array<Point>} ...points
	 */
	constructor(...points) {
		super(Point);
		this.add(...points);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(this.elements.length**2)
	 */
	get collisions() {
		let inter = [];
		for (let i = 0; i < this.elements.length; i++) {
			for (let j = i + 1; j < this.elements.length; j++) {
				const croisement = this.#collisionsBetweenTwo(this.elements[i], this.elements[j]);
				if (
					croisement &&
					(
						inter.length === 0 ||
						!inter.find((p) => p.distance.contains(p))
					)
				) {
					inter.push(croisement);
				}
			}
		}
		return inter;
	}
	/**
	 * @private
	 * @param {Point} p1
	 * @param {Point} p2
	 * @returns {Point}
	 */
	#collisionsBetweenTwo(p1, p2) {
		if (p1.distance.contains(p2)) {
			return p1;
		}
		return null;
	}
	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(this.collisions.length)
	 */
	get intersections() {
		let collection = new Collection();
		for (let inter of this.collisions) {
			collection.add(inter);
		}
		return collection;
	}
	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(this.elements.length)
	 */
	get equivalence() {
		let parallels = [this.elements[0]];
		for (let p of this.elements) {
			if (parallels.find((test) => p.distance.contains(test)) === undefined) {
				parallels.push(p);
			}
		}
		return parallels;
	}
}
