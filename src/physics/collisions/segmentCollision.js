import Collision from "./collision";
import LineCollision from "./lineCollision";
import Segment from "../../geometry/curves/segment";
import Collection from "../../collections/collection";
import Point from "../../geometry/points/point";

export default class SegmentCollision extends Collision {
    /**
     * @param {Array<Segment>} segments
     */
    constructor(...segments) {
        super(Segment);
        /** @type {Collection} */
        this._intersection = new Collection();
        this.add(...segments);
    }
    /**
     * @override
     * @inheritdoc
     */
    add(...elements) {
        elements.forEach(element => this.push(element));
        return this;
    }
    /**
     * Adds one element at a time and updates the intersection
     * @private
     * @param {Segment} segment
     */
    push(segment) {
        if(this.elements.length === 0)
            this._intersection.add(segment);
        super.add(segment);
        this._intersection.elements.forEach(
            element => {
                if(element instanceof Point
                    && !segment.distance.contains(element)) {
                    this._intersection.remove(element);
                } else if(element instanceof Segment
                    && this.equivalence.length > 1) {
                    this._intersection.remove(element);
                } else if(element instanceof Segment) {
                    this._intersection.remove(element);
                    const collider = new LineCollision(
                        element.line,
                        segment.line
                    );
                    if(collider.equivalence.length === 1) {
                        const A = element.vertices[0];
                        const B = element.vertices[1];
                        const C = segment.vertices[0];
                        const D = segment.vertices[1];
                        const Abis =
                            segment.distance.contains(A) ?
                            A :
                            (element.distance.contains(C) ?
                                C :
                                undefined
                            )
                        ;
                        const Bbis = 
                            segment.distance.contains(B) ?
                            B :
                            (element.distance.contains(D) ?
                                D :
                                undefined
                            )
                        ;
                        if(Abis !== undefined && Bbis !== undefined) {
                            this._intersection.add(
                                new Segment(Abis, Bbis)
                            )
                        }
                    }
                }
            }
        );
    }
    /**
     * @override
     * @inheritdoc
     * @type {Array<Segment>}
     */
    get elements() {
        return super.elements;
    }
    /**
     * @override
     * @inheritdoc
     */
    get equivalence() {
        const lines = this.elements.map(segment => {
            const line = segment.line;
            line.segment = segment;
            return line;
        });
        const lineCollider = new LineCollision(...lines);
        return lineCollider.equivalence.map(
            equivalence =>
            new Collection(
                ...equivalence.elements.map(
                    line => line.segment
                )
            )
        );
    }
    /**
	 * @override
	 * @inheritdoc
	 */
	get collisions(){
        const res = [];
        const lines = this.elements.map(
            segment => {
                const line = segment.line;
                line.segment = segment;
                return line;
            }
        );
        const parallels = (new LineCollision(...lines)).equivalence;
        for(let i = 0; i < parallels.length; i++) {
            for(let j = 0; j < i; j++) {
                for(let line of parallels[i].elements) {
                    for(let other of parallels[j].elements) {
                        const collision = (
                            new LineCollision(line, other)
                        ).collisions[0];
                        if(
                            line.segment.distance.contains(collision)
                            && other.segment.distance.contains(collision)
                        )
                            res.push(collision);
                    }
                }
            }
        }
        return res;
    }
    /**
     * @override
     * @inheritdoc
     */
    get strictCollisions() {
        const res = [];
        const lines = this.elements.map(
            segment => {
                const line = segment.line;
                line.segment = segment;
                return line;
            }
        );
        const parallels = (new LineCollision(...lines)).equivalence;
        for(let i = 0; i < parallels.length; i++) {
            for(let j = 0; j < i; j++) {
                for(let line of parallels[i].elements) {
                    for(let other of parallels[j].elements) {
                        const collision = (
                            new LineCollision(line, other)
                        ).collisions[0];
                        if(
                            line.segment.distance.contains(collision)
                            && other.segment.distance.contains(collision)
                        )
                            res.push(collision);
                    }
                }
            }
        }
        return res;
    }
    /**
     * @override
     * @inheritdoc
     */
    get strictCollisions() {
        const res = [];
        const lines = this.elements.map(
            segment => {
                const line = segment.line;
                line.segment = segment;
                return line;
            }
        );
        const parallels = (new LineCollision(...lines)).equivalence;
        for(let i = 0; i < parallels.length; i++) {
            for(let j = 0; j < i; j++) {
                for(let line of parallels[i].elements) {
                    for(let other of parallels[j].elements) {
                        const collision = (
                            new LineCollision(line, other)
                        ).collisions[0];
                        if(
                            line.segment.distance.containsStrict(collision)
                            && other.segment.distance.containsStrict(collision)
                        )
                            res.push(collision);
                    }
                }
            }
        }
        return res;
    }
    /**
     * @override
     * @inheritdoc
     */
    get intersection() {
        const res = new Collection(...this._intersection.elements);
        this.collisions.forEach(
            point => {
                if(!res.elements.some(
                    segment => segment.distance.contains(point)
                ))
                    res.add(point);
            }
        );
        return res;
    }
    /**
     * @]override
     * @inheritdoc
     */
    get strictIntersection() {
        const res = new Collection(...this._intersection.elements);
        this.strictCollisions.forEach(
            point => {
                if(!res.elements.some(
                    segment => segment.distance.containsStrict(point)
                ))
                    res.add(point);
            }
        );
        return res;
    }
}
