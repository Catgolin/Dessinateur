import GeometryInterface from "../../geometry/geometryInterface";

/**
 * @class DistanceInterface
 * @abstract
 *
 * @brief Measures the distance between an object and a point
 *
 * @version 2.0
 */
export default class DistanceInterface {
	/**
	 * @brief (read-only) The object in reference to which distances are measured
	 * @type {GeometryInterface}
	 */
	#object;

	/**
	 * @param {GeometryInterface} object: The object in reference to which distances are measured
	 */
	constructor(object) {
		if (this.constructor === DistanceInterface) {
			throw Error("DistanceInterface is abstract and should not be instanciated");
		}
		this.#object = object;
	}

	/**
	 * @brief The object in reference to which distances are measured
	 * @type {GeometryInterface}
	 */
	get object() {
		return this.#object;
	}

	/**
	 * @brief Distance between the destination and the object of reference
	 * @see object
	 *
	 * @param {Point} destination
	 *
	 * @returns {number} The distance to the destination from the object of reference
	 *
	 * @throws {Error} If point is not a Point
	 */
	to(destination) {
		throw Error(`${this.constructor.name} should implement to()`);
	}

	/**
	 * @brief Square of the distance between the destination and the object of reference
	 * @see to()
	 *
	 * This is meant to avoid calculating the square root when possible,
	 * as calculating the square is cheaper as calculating the square root.
	 *
	 * @param {Point} destination
	 *
	 * @returns {number} Square of the distance to the destination from the object of reference
	 *
	 * @throws {Error} If point is not a Point
	 *
	 * @note This method should be override when the to() method is using a square root
	 */
	squareTo(destination) {
		return this.to(destination) ** 2;
	}

	/**
	 * @brief Checks if the object of reference contains the point
	 *
	 * The inclusion is understood in the large sense: borders are part of the object.
	 *
	 * @param {Point} destination
	 *
	 * @returns {boolean} Is the destination inside the object of reference?
	 *
	 * @throws {Error} If point is not a Point
	 */
	contains(destination) {
		throw Error(`${this.constructor.name} should implement contains`);
	}

	/**
	 * @brief Checks if the object of rerefence strictly contains the point
	 *
	 * The inclusion is understood in the stric sense: borders are NOT part of the object.
	 *
	 * @param {Point} destination
	 *
	 * @returns {boolean} Is the destination strictly inside the object of reference?
	 *
	 * @throws {Error} If point is not a Point
	 */
	containsStrict(destination) {
		return this.contains(destination);
	}
}
