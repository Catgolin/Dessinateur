import DistanceInterface from "./distanceInterface";
import Point from "../../geometry/points/point";

/**
 * @class LineDistance
 *
 * @brief Measures distances from a point to a line
 *
 * @version 2.0
 *
 * @see DistanceInterface
 */
export default class LineDistance extends DistanceInterface {
	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(log(distance))
	 *
	 * Sources of rounding errors:
	 * - PointDistance.to()
	 * - this.projectionOf()
	 */
	to(destination) {
		return this.projectionOf(destination).distance.to(destination);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding error:
	 * - PointDistance.squareTo()
	 * - this.projectionOf()
	 */
	squareTo(destination) {
		return this.projectionOf(destination).distance.squareTo(destination);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding errors:
	 * - this.projectionOf()
	 * Precision depends of the epsilon defined in PointDistance
	 */
	contains(destination) {
		return this.projectionOf(destination).distance.contains(destination);
	}

	/**
	 * @brief Returns the orthogonal projection of the point on the line
	 *
	 * @param {Point} point
	 *
	 * @return {Point}
	 * @throws {Error} If point is not a Point
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding errors:
	 * - Substraction if point is close to 0
	 * - Multiplication if cartesian coefficients are big
	 * - Division if cartesian coefficients are small
	 */
	projectionOf(point) {
		if (!point || typeof point !== 'object') {
			throw new Error(`Expected destination to be a Point, got ${point}`);
		} else if (!(point instanceof Point)) {
			throw new Error(`Expected destination to be a Point, got ${point.constructor.name}`);
		}
		const {
			a,
			b,
			c
		} = this.object.cartesianCoefficients;
		const denum = a ** 2 + b ** 2;
		const x = (b * (b * point.x - a * point.y) - a * c) / denum;
		const y = (a * (a * point.y - b * point.x) - b * c) / denum;
		return new Point(x, y);
	}
}
