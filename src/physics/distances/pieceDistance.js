import SegmentCollision from "../collisions/segmentCollision";
import DistanceInterface from "./distanceInterface";
import Point from "../../geometry/points/point";
import Segment from "../../geometry/curves/segment";

/**
 * @class PieceDistance
 *
 * @brief Measures distance from a point to a piece
 *
 * @version 2.0
 *
 * @see DistanceInterface
 *
 * @TODO to()
 */
export default class PieceDistance extends DistanceInterface {
	/**
	 * @override
	 * @inheritdoc
	 */
	to(destination) {
		return 0; // WIP
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	containsStrict(destination) {
		if (this.#edgesContain(destination)) {
			return false;
		}
		let test = new Point(1000 + Math.random() * 10, 1000 + Math.random() * 10);
		let testLine = new Segment(destination, test);
		let collider = new SegmentCollision(testLine, ...this.object.edges);
		if (collider.strictCollisions.length % 2 === 1) {
			return true;
		}
		return false;
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	contains(destination) {
		return this.containsStrict(destination) || this.edgesContain(destination);
	}
	/**
	 * @brief Checks if one of the edge contains the destination
	 *
	 * @param {Point} destination
	 *
	 * @return {boolean} true if and only if the destination is contained in one of the edges
	 */
	#edgesContain(destination) {
		for (let edge of this.object.edges) {
			if (edge.distance.contains(destination)) {
				return true;
			}
		}
		return false;
	}
}
