import DistanceInterface from "./distanceInterface";

/**
 * @class PointDistance
 *
 * @brief Measures distances between two points
 *
 * @version 2.0
 *
 * @see DistanceInterface
 */
export default class PointDistance extends DistanceInterface {
	/**
	 * @public
	 * @type {number}
	 * @brief Precision of the algorithm for contains checks
	 */
	static #epsilon = 3 - Math.sqrt(3) ** 2;

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: O(log(distance))
	 *
	 * Sources of rounding errors:
	 * - this.squareTo()
	 * - Square root
	 */
	to(destination) {
		return Math.sqrt(this.squareTo(destination));
	}
	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding errors:
	 * - Substraction cancellation if this.object is close to destination
	 * - Square multiplication if this.object is far from destination
	 */
	squareTo(destination) {
		return (this.object.x - destination.x) ** 2 +
			(this.object.y - destination.y) ** 2;
	}
	/**
	 * @overide
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Manages rounding errors by using some epsilon
	 */
	contains(destination) {
		return this.squareTo(destination) < PointDistance.epsilon;
	}
	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 */
	containsStrict(destination) {
		return false;
	}

	/**
	 * @param {number} epsilon
	 */
	static set epsilon(epsilon) {
		if (typeof epsilon !== "number") {
			throw new Error(`Expected epsilon to be a number, ${typeof epsilon} given`);
		}
		if (isNaN(epsilon)) {
			throw new Error(`Expected epsilon to be a number, ${epsilon} given`);
		}
		PointDistance.#epsilon = epsilon;
	}
	/**
	 * @type{number}
	 */
	static get epsilon() {
		return PointDistance.#epsilon;
	}
}
