import DistanceInterface from "./distanceInterface";
import Point from "../../geometry/points/point";
import Line from "../../geometry/curves/line";

/**
 * @class SegmentDistance
 *
 * @brief Measures distances between a point and a segment
 *
 * @version 2.0
 *
 * @see DistanceInterface
 */
export default class SegmentDistance extends DistanceInterface {
	/**
	 * @override
	 * @inheritdoc
	 * @description Checks if the projection of the point on the line is contained by the linesegment, 
	 * if it is, returns the distance from point to line,
	 * if not, returns the minimum distance from the point and the vertices of the linesegment
	 *
	 * Complexity: O(log(distance))
	 *
	 * Sources of rounding errors:
	 * - LineDistance.projectionOf()
	 * - PointDistance.to()
	 */
	to(destination) {
		const projection = this.object.line.distance.projectionOf(destination);
		if (this.contains(projection)) {
			return projection.distance.to(destination);
		}
		return Math.min(
			this.object.vertices[0].distance.to(destination),
			this.object.vertices[1].distance.to(destination)
		);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding errors:
	 * - LineDistance.projectionOf()
	 * - PointDistance.squareTo()
	 */
	squareTo(destination) {
		const projection = this.object.line.distance.projectionOf(destination);
		if (this.contains(projection)) {
			return projection.distance.squareTo(destination);
		}
		return Math.min(
			this.object.vertices[0].distance.squareTo(destination),
			this.object.vertices[1].distance.squareTo(destination)
		);
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding errors:
	 * - LineDistance.contains()
	 * - PointDistance.contains()
	 * - substraction if destination is close to one extremity
	 * - multiplication and addition if destination is far to both extremities
	 * Precision depends of the epsilon defined in PointDistance
	 */
	contains(destination) {
		if (!this.object.line.distance.contains(destination)) {
			return false;
		}
		// Constant in time complexity because there is a constant number of vertices
		if (this.object.vertices.find((point) => point.distance.contains(destination))) {
			return true;
		}
		const A = this.object.vertices[0];
		const B = this.object.vertices[1];
		const P = destination;
		const scalarProduct = (P.x - A.x) * (P.x - B.x) + (P.y - A.y) * (P.y - B.y);
		return scalarProduct < 0;
	}

	/**
	 * @override
	 * @inheritdoc
	 *
	 * Complexity: constant
	 *
	 * Sources of rounding errors:
	 * - this.contians()
	 * - PointDistance.contains()
	 * Precision depends of the epsilon defined in PointDistance
	 */
	containsStrict(destination) {
		if (!this.contains(destination)) {
			return false;
		}
		// Constant in time complexity because there is a constant number of vertices
		const contained = this.object.vertices.find(
			(point) => point.distance.contains(destination)
		);
		return contained === undefined;
	}
}
