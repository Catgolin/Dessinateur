import Scence from "../collections/scene";
import GeometryInterface from "../geometry/geometryInterface";
import LinePrinter from "./printers/linePrinter";
import RoundPointPrinter from "./printers/roundPointPrinter";
import SegmentPrinter from "./printers/segmentPrinter";
import ScreenPoint from "./screenPoint";
import Point from "./../geometry/points/point";

/** @class Camera
 *
 * @brief Controlls the projection of the geometric elements on the canvas space
 *
 * @version 2.0
 */
export default class Camera {
	/**
	 * @brief The default value of every camera when initialized
	 * @public
	 * @type {string}
	 */
	static #defaultBackground = "black";
	static get defaultBackground() {
		return this.#defaultBackground;
	}
	static set defaultBackground(color) {
		if (typeof color !== "string") {
			throw Error(`The default background color must be defined by a string, got ${typeof color}`);
		}
		this.#defaultBackground = color;
	}

	/**
	 * @brief The actual background of this canvas
	 * @public
	 * @type {string}
	 */
	#background;
	get background() {
		if (typeof this.#background === "string") {
			return this.#background;
		}
		return this.constructor.defaultBackground;
	}
	set background(color) {
		if (color !== undefined && typeof color !== typeof "") {
			throw Error(`The background color must be defined by a string, go ${typeof color}`);
		}
		this.#background = color;
	}

	/**
	 * @brief The point in the center of the camera
	 * @public
	 * @type {Point}
	 */
	#center;
	get center() {
		return this.#center;
	}
	set center(object) {
		if (!(object instanceof Point)) {
			throw new Error(`Expected the center to be a Point, got ${object ? object.constructor.name : object}`);
		}
		this.#center = object;
	}


	/**
	 * @brief The objects dedicated to printing the geometric elements on the canvas
	 * @type {Array<Printer>}
	 */
	#printers;

	constructor() {
		this.resetOrientation();
		this.#printers = [
			new RoundPointPrinter(),
			new LinePrinter(),
			new SegmentPrinter()
		];
	}

	/**
	 * @param {Scence} scene: The scene to draw
	 * @param {HTMLCanvasElement} canvas: The canvas on which to draw
	 */
	draw(scene, canvas) {
		const ctx = canvas.getContext("2d");
		ctx.fillStyle = this.background;
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		scene.layers.forEach(
			(layer) =>
			layer.elements.forEach(
				(element) => this.printObject(element, canvas)
			)
		);
	}

	/**
	 * Reset the coordinates, scale and rotation of the camera
	 */
	resetOrientation() {
		this.center = new Point(0, 0);
		this.scale = 1;
		this.rotation = 0;
	}

	/**
	 * @brief Gets the coordinates in the scene of a corresopnding point on the screen
	 *
	 * @param {ScreenPoint} point: The point to convert
	 * @param {HTMLCanvasElement} canvas: The canvas containing at least a width and height properties
	 *
	 * @return {Point}
	 */
	canvasToScene(point, canvas) {
		if (!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		if(!(canvas && "width" in canvas && "height" in canvas)) {
			throw new Error(`Expected canvas to be a HTMLCanvasElement, got ${canvas ? canvas.constructor.name : canvas}`);
		}
		return new Point(
			this.center.x + point.x - this.getCenter(canvas).x,
			this.center.y - point.y + this.getCenter(canvas).y
		);
	}
	/**
	 * @brief Gets the coordinates on the canvas at which the given point is rendered
	 *
	 * @param {Point} point: The point to convert
	 * @param {HTMLCanvasElement} canvas: The canvas containing at least a width and height properties
	 *
	 * @return {ScreenPoint}
	 */
	sceneToCanvas(point, canvas) {
		if (!(point instanceof Point)) {
			throw new Error(`Expected point to be a Point, got ${point ? point.constructor.name : point}`);
		}
		if(!(canvas && "width" in canvas && "height" in canvas)) {
			throw new Error(`Expected canvas to be a HTMLCanvasElement, got ${canvas ? canvas.constructor.name : canvas}`);
		}
		return new ScreenPoint(
			this.getCenter(canvas).x + point.x - this.center.x,
			this.getCenter(canvas).y - point.y + this.center.y
		);
	}

	/**
	 * @brief Prints a single object on the canvas
	 *
	 * @param {GeometryInterface} object: The object to print
	 * @param {HTMLCanvasElement} canvas: The canvas on which to print
	 */
	printObject(object, canvas) {
		if (!(object instanceof GeometryInterface)) {
			throw new Error(`Expected the object to be a GeomtryInterface element, got ${object ? object.constructor.name : object}`);
		}
		const printer = this.getPrinterFor(object);
		printer.print(object, this, canvas.getContext("2d"));
	}

	/**
	 * @brief Returns the appropriate printer for the given element
	 * @param {GeometryInterface} element
	 * @return {Printer}
	 */
	getPrinterFor(object) {
		if (!(object instanceof GeometryInterface)) {
			if (typeof object === "object") {
				throw Error(`Expected the object to be a GeometryInterface element, got a ${object.constructor.name}`);
			}
			throw Error(`Expected the object to be a GeometryInterface element, got ${typeof object}`);
		}
		for (const printer of this.#printers) {
			if (printer.manages(object)) {
				return printer;
			}
		}
		throw Error(`No printer could manage ${object.constructor.name}`);
	}

	/**
	 * @brief Returns the ScreenPoint in the center of the canvas
	 *
	 * @param {HTMLCanvasElement} canvas The canvas of reference
	 *
	 * @return {ScreenPoint}
	 */
	getCenter(canvas) {
		if(!(canvas && "width" in canvas && "height" in canvas)) {
			throw new Error(`Expected canvas to be a HTMLCanvasElement, got ${canvas ? canvas.constructor.name : canvas}`);
		}
		return new ScreenPoint(
			canvas.width / 2,
			canvas.height / 2
		);
	}
}
