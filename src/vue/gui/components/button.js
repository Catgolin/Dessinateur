import ComponentInterface from "./componentInterface";

export default class Button extends ComponentInterface {
	/**
	   * @brief Number of pixels inside the button he left and right of the text
	  * @type {number}
	  */
	static padding = 10;

	/**
	 * @param {ContainerInterface} parent: see ElementInterface
	 * @param {function} action: the function called on click
	 * @param {string} descriptionId: see ComponentInterface
	 */
	constructor(parent, action, descriptionId) {
		super(parent, descriptionId);
		if(!typeof action === "function")
			throw Error("Action should be a function, "+typeof action+" given");
		this.action = action;
	}

	/**
	 * @brief Text displayed on the button
	 * @type {string}
	 */
	get title() {
		return this.gui.translator.translate(this.descriptionId+".btn");
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get description() {
		return this.gui.translator.translate(this.descriptionId+".description");
	}

	/**
	 * @override
	 * @type {number}
	 */
	get width() {
		if(super.width === undefined) {
			return this.measuredWidth;
		} else {
			return super.width;
		}
	}
	/**
	 * @override
	 * @type {number}
	 */
	get height() {
		if(super.height === undefined) {
			return this.measuredHeight;
		} else {
			return super.height;
		}
	}
	/**
	 * @override
	 * @param {number}
	 */
	set width(width) {
		super.width = width;
	}
	/**
	 * @override
	 * @param {number}
	 */
	set height(height) {
		super.height = height;
	}

	/**
	 */
	measureSize() {
		const ctx = this.gui.canvas.getContext("2d");
		let measure = ctx.measureText(this.title);
		this.measuredWidth = measure.width + (2 * this.constructor.padding);
		this.measuredHeight = measure.actualBoundingBoxAscent +
			measure.actualBoundingBoxDescent + (2 * this.constructor.padding);
	}

	/**
	 * @override
	 */
	drawDefault() {
		const ctx = this.gui.canvas.getContext("2d");
		if(!this.width > 0 || !this.height > 0)
			this.measureSize(ctx);
		ctx.fillStyle = this.constructor.background;
		ctx.fillRect(this.absoluteX, this.absoluteY, this.width, this.height);
		let measure = ctx.measureText(this.title);
		const x = this.absoluteX + (this.width - measure.width) / 2;
		const y = this.absoluteY
			- measure.actualBoundingBoxDescent
			+ this.height / 2
		;
		ctx.textBaseline = "middle";
		ctx.fillStyle = this.constructor.foreground;
		ctx.fillText(this.title, x, y);
	}

	click() {
		this.action();
	}
}
