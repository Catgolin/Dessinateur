import ElementInterface from '../elementInterface';
import Tooltip from '../tooltip';
import ScreenPoint from "../../screenPoint";

/**
 * @abstract
 */
export default class ComponentInterface extends ElementInterface {
	/**
	 * @brief The default background color
	 * @type {string} background
	 */
	static background = "blue";
	/**
	 * @brief The default foreground color
	 * @type {string} foreground
	 */
	static foreground = "white";

	/**
	 * @constructor
	 * @abstract
	 * @param {ContainerInterface} parent
	 * @param {string} descriptionId:
	 * id to describe this element in translation.json
	*/
	constructor(parent, descriptionId) {
		super(parent);
		if(this.constructor === ComponentInterface) {
			throw new Error("ComponentInterface is abstract and should not be implemented");
		}
		this.descriptionId = descriptionId;
		this.state = STATE.DEFAULT;
		this.tooltip = null;
	}

	/**
	 * Returns the description translated in the current locale
	 * @type {string}
	 */
	get description() {
		return this.translator.translate(this.descriptionId);
	}

	/**
	 * Checks if this component is hovered by the mouse
	 * @type {boolean}
	*/
	get isHovered() {
		return this.state % 2 == 1;
	}

	/**
	 * @return {boolean}
	*/
	get isFocused() {
		return (this.state >> 1) % 2 == 1;
	}
	/**
	 * @return {boolean}
	*/
	get isActive() {
		return (this.state >> 2) % 2 == 1;
	}
	/**
	 * @return {boolean}
	*/
	get isDisabled() {
		return (this.state >> 3) % 2 == 1;
	}
	get currentState() {
		if(this.isDisabled)
			return STATE.DISABLED;
		if(this.isActive)
			return STATE.ACTIVE;
		if(this.isFocused)
			return STATE.FOCUS;
		if(this.isHovered)
			return STATE.HOVER;
		return STATE.DEFAULT;
	}
	/**
	*/
	click() {}

	/**
	 * @brief Removes any effect added by the hover method
	 *
	 * @return {boolean} True if a change happened, false if not
	 *
	 * @see hover
	*/
	cancelHovering() {
		if(!this.isHovered) {
			return false;
		}
		this.state -= this.isHovered;
		this.tooltip.remove();
		return true;
	}

	/**
	 * @brief Sets the component on the hovered status and spawns a tooltip
	 *
	 * @param {ScreenPoint} point: The current position of the mouse
	 *
	 * @return {boolean} True if a change happened, false if not
	*/
	hover(point) {
		if(!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, got ${point ? point.constructor.name : point}`);
		}
		if(!this.collision(point)) {
			return this.cancelHovering();
		}
		if(!this.isHovered) {
			this.state += STATE.HOVER;
		}
		this.spawnTooltip(point.x, point.y);
		return true;
	}

	mouseDown() {
		if(this.isActive) {
			return false;
		}
		this.state += STATE.ACTIVE;
		return true;
	}

	mouseUp() {
		if(!this.isActive) {
			return false;
		}
		this.state -= STATE.ACTIVE;
		return true;
	}

	focus() {
		if(!this.isFocused) {
			this.state += STATE.FOCUS;
		}
	}

	unfocus() {
		if(this.isFocused) {
			this.state -= STATE.FOCUS;
		}
	}

	/**
	 * @override
	*/
	draw() {
		const swap = () => {
			[this.constructor.background, this.constructor.foreground] =
				[this.constructor.foreground, this.constructor.background];
		};
		if(this.isActive) swap();
		switch(this.currentState) {
			case STATE.HOVER:
				this.drawHover();
				break;
			case STATE.ACTIVE:
				this.drawActive();
				break;
			case STATE.FOCUS:
				this.drawFocus();
				break;
			case STATE.DISABLED:
				this.drawDisabled();
			default:
				this.drawDefault();
				break;
		}
		if(this.isActive) swap();
	}

	/**
	 * Draws this element when it is hovered by the mouse
	 */
	drawHover() {
		this.drawFocus();
	}

	/**
	 * Draws this element when is is clicked on
	*/
	drawActive() {
		this.drawFocus();
	}

	/**
	 * Draws this element when it is selected
	 */
	drawFocus() {
		this.ctx.strokeStyle = "red";
		this.ctx.lineWidth = 1;
		this.ctx.strokeRect(this.absoluteX, this.absoluteY, this.width, this.height);
		this.drawDefault();
	}

	/**
	 * Draws this element when it is disabled
	*/
	drawDisabled() {
		this.drawDefault();
		this.ctx.fillStyle = "rgba(127, 127, 127, .5)";
		this.ctx.fillRect(this.absoluteX, this.absoluteY, this.width, this.height);
	}

	/** Draws this element when it is not hovered, clicked or selected
	*/
	drawDefault() {
		throw Error("The default draw method should be implemented");
	}

	/**
	 * @brief Removes the current tooltip and creats a new one
	 *
	 * @param {number} x
	 * @param {number} y
	 *
	 * @return Tooltip
	*/
	spawnTooltip(x, y) {
		if(this.tooltip !== null) {
			this.tooltip.remove();
		}
		this.tooltip = new Tooltip(this.gui, x, y, this.description);
		return this.tooltip;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	collision(point) {
		if(!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, but got ${point ? point.constructor.name : point}`);
		}
		if(point.x >= this.absoluteX && point.y >= this.absoluteY &&
			point.x <= this.absoluteX + this.width &&
			point.y <= this.absoluteY + this.height) {
			return this;
		}
		return null;
	}
}

/**
 * @enum {int}
*/
const STATE = {
	DEFAULT: 0,
	HOVER: 1,
	FOCUS: 2,
	ACTIVE: 4,
	DISABLED: 8,
};
export {STATE};
