import ComponentInterface from "./componentInterface";

/**
 * @brief A message displayed to the user on the screen
 * @version 2.0
 * @date 2022-01-30
 * @author Catgolin
 */
export default class Message extends ComponentInterface {

	/**
	 * @brief Equivalent to css padding
	 * @type {number}
	 *
	 * @description Space left between the borders (top, left, bottom, right) of the element and it's content
	 */
	static get padding() {
		return 10;
	}

	/**
	 * @brief Text displayed on the screen
	 * @type {string}
	 */
	get title() {
		return this.gui.translator.translate(`${this.descriptionId}.message`);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get description() {
		return this.gui.translator.translate(`${this.descriptionId}.message`);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get width() {
		if (!this.gui.canvas) {
			return 0;
		}
		const measure = this.gui.canvas.getContext("2d").measureText(this.title);
		return measure.width + (this.constructor.padding * 2);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get height() {
		if (!this.gui.canvas) {
			return 0;
		}
		const measure = this.gui.canvas.getContext("2d").measureText(this.title);
		return measure.actualBoundingBoxAscent + measure.actualBoundingBoxDescent + (this.constructor.padding * 2);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	drawDefault() {
		const ctx = this.gui.canvas.getContext("2d");
		if (!(this.width > 0 && this.height > 0)) {
			return;
		}
		ctx.fillStyle = this.constructor.foreground;
		ctx.textBaseline = "middle";
		const size = ctx.measureText(this.title);
		const x = this.absoluteX + this.constructor.padding;
		const y = this.absoluteY + ctx.measureText(this.title).actualBoundingBoxAscent + this.constructor.padding;
		ctx.fillText(this.title, x, y);
	}
}
