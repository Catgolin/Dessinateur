import ContainerInterface from './containerInterface.js';
import {
	ALIGNMENT
} from './../elementInterface.js';

/**
 * @class BarInterface
 * @abstract
 */
export default class BarInterface extends ContainerInterface {
	/**
	 * @type {DIRECTION}
	 */
	get direction() {
		switch (this.alignment) {
		case ALIGNMENT.TOP:
		case ALIGNMENT.BOTTOM:
			return DIRECTION.HORIZONTAL;
		case ALIGNMENT.LEFT:
		case ALIGNMENT.RIGHT:
			return DIRECTION.VERTICAL;
		case ALIGNMENT.CENTER:
			return this.parent.direction;
		default:
			throw new Error(`Invalid alignment ${this.alignment}`);
		}
	}

	/**
	 * @param {ContainerInterface} parent
	 * @param {ALIGNMENT} alignment
	 */
	constructor(parent, alignment) {
		super(parent);
		if (this.constructor === BarInterface) {
			throw Error("BarInterface is abstract and should not be implemented");
		}
		if (isNaN(alignment)) {
			throw Error(`Invalid alignment ${alignment}`);
		}
		this.parent = parent;
		this.alignment = alignment;
	}
}

/**
 * @enum {string}
 */
const DIRECTION = {
	HORIZONTAL: "hFlow",
	VERTICAL: "vFlow",
};
export {
	DIRECTION
};
