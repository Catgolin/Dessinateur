import ElementFactory from '../elementFactory';
import ScreenPoint from '../../screenPoint';
import ElementInterface from './../elementInterface';

/**
 * @breif Elements containing other elements of the interface
 *
 * @version 2.0
 * @date 2022-01-28
 * @author Catgolin
 */
export default class ContainerInterface extends ElementInterface {
	/**
	 * @brief (readonly) Elements contained by this container
	 * @type {Array<ElementInterface>}
	 */
	#components;
	get components() {
		return this.#components;
	}

	/**
	 * @brief The number of pixels between elements
	 * @type {number}
	 */
	#padding = 0;
	get padding() {
		return this.#padding;
	}
	set padding(value) {
		if (isNaN(value)) {
			throw new Error(`Expected the padding to be a number, got ${value ? value.constructor.name : value}`);
		}
		this.#padding = value;
	}
	/**
	 * @brief The object responsible to add new elements to this one
	 * @type {ElementFactory}
	 */
	get add() {
		return new ElementFactory(this);
	}

	/**
	 * @constructor
	 * @abstract
	 * @inheritdoc
	 */
	constructor(parent) {
		super(parent);
		this.#components = [];
		if (this.constructor === ContainerInterface) {
			throw Error("ContainerInterface is abstract and should not be implemented");
		}
	}

	/**
	 * @override
	 * @inheritdoc
	 **/
	draw() {
		this.components.forEach(component => component.draw());
	}

	/**
	 * @brief Adds an element to this container
	 * @abstract
	 * @param {ElementInterface} element
	 */
	append(element) {
		this.components.push(element);
		element.parent = this;
	}

	/**
	 * @override
	 * @inheritdoc
	 **/
	collision(point) {
		if (!(point instanceof ScreenPoint)) {
			throw new Error(`Expected point to be a ScreenPoint, but got ${point ? point.constructor.name : point}`);
		}
		const child = this.components.find((element) => element.collision(point));
		return child ? child.collision(point) : child;
	}

	/**
	 * @brief Returns the x position of the element inside this container
	 * @param {ElementInterface} element
	 * @return {number}
	 */
	getX(element) {
		throw new Error(`${this.constructor.name} should implement the getX() method`);
	}

	/**
	 * @brief Returns the y position of the element inside this container
	 * @param {ElementInterface} element
	 * @return {number}
	 */
	getY(element) {
		throw new Error(`${this.constructor.name} should implement the getY() method`);
	}

	/**
	 * @brief Removes every elements inside this bar
	 */
	empty() {
		this.#components = [];
	}
}
