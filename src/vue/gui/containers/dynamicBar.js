import {
	ALIGNMENT
} from "../elementInterface";
import BarInterface, {
	DIRECTION
} from "./barInterface";

/**
 * @brief A bar whose lenght depends of it's content
 *
 * @version 2.0
 * @date 2022-01-28
 * @author Catgolin
 */
export default class DynamicBar extends BarInterface {
	/**
	 * @override
	 * @inheritdoc
	 */
	get width() {
		if (this.components.length < 1) {
			return 0;
		}
		switch (this.direction) {
		case DIRECTION.HORIZONTAL:
			const block = this.components.reduce((width, component) => width + component.width, 0);
			const padding = this.padding * (this.components.length - 1);
			return block + padding;
		case DIRECTION.VERTICAL:
			return Math.max(...this.components.map((component) => component.width));
		default:
			throw new Error(`Invalid direction ${this.direction}`);
		}
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get height() {
		if (this.components.length < 1) {
			return 0;
		}
		switch (this.direction) {
		case DIRECTION.HORIZONTAL:
			return Math.max(...this.components.map((component) => component.height));
		case DIRECTION.VERTICAL:
			const block = this.components.reduce((height, component) => height + component.height, 0);
			const padding = this.padding * (this.components.length - 1);
			return block + padding;
		default:
			throw new Error(`Invalid direction ${this.direction}`);
		}
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	get direction() {
		return this.parent.direction;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	constructor(parent, alignment) {
		switch (alignment) {
		case ALIGNMENT.TOP:
		case ALIGNMENT.BOTTOM:
			if (parent.direction === DIRECTION.HORIZONTAL)
				throw Error("Invalid alignment");
			break;
		case ALIGNMENT.LEFT:
		case ALIGNMENT.RIGHT:
			if (parent.direction === DIRECTION.VERTICAL)
				throw Error("Invalid alignment");
			break;
		}
		super(parent, alignment);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	getX(element) {
		switch (this.direction) {
		case DIRECTION.HORIZONTAL:
			const i = this.components.indexOf(element);
			return i > 0 ? this.components[i - 1].x + this.components[i - 1].width : 0;
		case DIRECTION.VERTICAL:
			const width = Math.max(this.width, element.width);
			return width > 0 ? (width - element.width) / 2 : 0;
		default:
			throw new Error(`Invalid direction ${this.direction}`);
		}
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	getY(element) {
		switch (this.direction) {
		case DIRECTION.HORIZONTAL:
			const height = Math.max(this.height, element.height);
			return height > 0 ? (height - element.height) / 2 : 0;
		case DIRECTION.VERTICAL:
			const i = this.components.indexOf(element);
			return i > 0 ? this.components[i - 1].y + this.components[i - 1].height : 0;
		default:
			throw new Error(`Invalid direction ${this.direction}`);
		}
	}

}
