import Translator from '../../translator.js';
import {
	ALIGNMENT
} from '../elementInterface.js';
import ContainerInterface from './containerInterface.js';
import SpannedBar from './spannedBar.js';
export default class GUI extends ContainerInterface {
	/**
	 * @override
	 * @inheritdoc
	 */
	get x() {
		return 0;
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get y() {
		return 0;
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get gui() {
		return this;
	}
	/**
	 * @override
	 * @type {HTMLCanvasElement}
	 */
	get parent() {
		return this.canvas;
	}
	/**
	 * @override
	 * @type {HTMLCanvasElement}
	 */
	set parent(canvas) {
		this.canvas = canvas;
	}
	/**
	 * @param {Translator} translator
	 */
	get translator() {
		return this._translator;
	}
	/**
	 * @param {Translator} translator
	 */
	set translator(translator) {
		if (!translator instanceof Translator)
			throw Error("Expected Translator, got " + translator.constructor.name);
		this._translator = translator;
	}
	/**
	 * @inheritdoc
	 * @note The gui element doesn't have parents
	 */
	get absoluteX() {
		return this.x;
	}
	/**
	 * @inheritdoc
	 * @note The gui element doesn't have parents
	 */
	get absoluteY() {
		return this.y;
	}
	/**
	 * @inheritdoc
	 * @note The size of the gui is always the size of the canvas
	 */
	get width() {
		return this.parent.width;
	}
	/**
	 * @inheritdoc
	 * @note The size of the gui is always the size of the canvas
	 */
	get height() {
		return this.parent.height;
	}
	/**
	 * @inheritdoc
	 */
	get components() {
		return [
			this.top,
			this.left,
			this.bottom,
			this.right,
			...this.floatingElements
		];
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get gui() {
		return this;
	}
	/**
	 * @override
	 * @type {HTMLCanvasElement}
	 */
	get parent() {
		return this.canvas;
	}
	/**
	 * @override
	 * @type {HTMLCanvasElement}
	 */
	set parent(canvas) {
		this.canvas = canvas;
	}
	/**
	 * @param {Translator} translator
	 */
	get translator() {
		return this._translator;
	}
	/**
	 * @param {Translator} translator
	 */
	set translator(translator) {
		if (!translator instanceof Translator)
			throw Error("Expected Translator, got " + translator.constructor.name);
		this._translator = translator;
	}
	/**
	 * @inheritdoc
	 * @note The gui element doesn't have parents
	 */
	get absoluteX() {
		return this.x;
	}
	/**
	 * @inheritdoc
	 * @note The gui element doesn't have parents
	 */
	get absoluteY() {
		return this.y;
	}
	/**
	 * @inheritdoc
	 * @note The size of the gui is always the size of the canvas
	 */
	get width() {
		return this.parent.width;
	}
	/**
	 * @inheritdoc
	 * @note The size of the gui is always the size of the canvas
	 */
	get height() {
		return this.parent.height;
	}
	/**
	 * @inheritdoc
	 */
	get components() {
		return [
			this.top,
			this.left,
			this.bottom,
			this.right,
			...this.floatingElements
		];
	}

	/**
	 * @constructor
	 * @param {HTMLCanvasElement} canvas
	 * @param {Translator} translator
	 */
	constructor(canvas, translator) {
		super(canvas);
		this.translator = translator;
		this.floatingElements = [];
		this.left = new SpannedBar(this, ALIGNMENT.LEFT);
		this.top = new SpannedBar(this, ALIGNMENT.TOP);
		this.right = new SpannedBar(this, ALIGNMENT.RIGHT);
		this.bottom = new SpannedBar(this, ALIGNMENT.BOTTOM);
	}
	/**
	 * @inheritdoc
	 */
	append(element) {
		this.floatingElements.push(element);
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	empty() {
		this.components.forEach((component) => component instanceof ContainerInterface ? component.empty() : "");
	}
}
