import ElementInterface, {
	ALIGNMENT
} from "../elementInterface";
import BarInterface, {
	DIRECTION
} from "./barInterface.js";
import DynamicBar from "./dynamicBar";

/**
 * @brief A bar whose lenght depends of it's parent
 *
 * @description
 * The bar is divised into three dynamicBars:
 * - a left, center and right bar if the orientation is horizontal
 * - a top, center and bottom bar if the orientation is vertical
 *
 * @version 2.0
 * @date 2022-01-28
 * @author Catgolin
 */
export default class SpannedBar extends BarInterface {
	/**
	 * @brief The left side of this bar
	 * @type {DynamicBar}
	 * @throws {Error} if the orientation of this bar is not horizontal
	 */
	get left() {
		if (this.direction !== DIRECTION.HORIZONTAL) {
			throw new Error("This element is not aligned horizontaly");
		}
		return this.components.find((element) => element.alignment === ALIGNMENT.LEFT);
	}
	/**
	 * @brief The top part of this bar
	 * @type {DynamicBar}
	 * @throws {Error} if the orientation of this bar is not vertical
	 */
	get top() {
		if (this.direction !== DIRECTION.VERTICAL) {
			throw new Error("This element is not aligned vertically");
		}
		return this.components.find((element) => element.alignment === ALIGNMENT.TOP);
	}
	/**
	 * @brief The right side of this bar
	 * @type {DynamicBar}
	 * @throws {Error} if the orientation of this bar is not horizontal
	 */
	get right() {
		if (this.direction !== DIRECTION.HORIZONTAL) {
			throw new Error("This element is not aligned horizontaly");
		}
		return this.components.find((element) => element.alignment === ALIGNMENT.RIGHT);
	}
	/**
	 * @brief The bottom side of this bar
	 * @type {DynamicBar}
	 * @throws {Error} if the orientation of this bar is not vertical
	 */
	get bottom() {
		if (this.direction !== DIRECTION.VERTICAL) {
			throw new Error("This element is not aligned verticaly");
		}
		return this.components.find((element) => element.alignment === ALIGNMENT.BOTTOM);
	}
	/**
	 * @brief The middle part of this bar
	 * @type {DynamicBar}
	 */
	get center() {
		return this.components.find((element) => element.alignment === ALIGNMENT.CENTER);
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get x() {
		switch (this.alignment) {
		case ALIGNMENT.TOP:
		case ALIGNMENT.BOTTOM:
		case ALIGNMENT.LEFT:
			return 0;
		case ALIGNMENT.RIGHT:
			return this.parent.width - this.width;
		default:
			throw new Error(`Invalid aligment: ${this.aligment}`);
		}
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get y() {
		switch (this.alignment) {
		case ALIGNMENT.TOP:
		case ALIGNMENT.LEFT:
		case ALIGNMENT.RIGHT:
			return 0;
		case ALIGNMENT.BOTTOM:
			return this.parent.height - this.height;
		default:
			throw new Error(`Invalid aligment: ${this.aligment}`);
		}
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get width() {
		switch (this.direction) {
		case DIRECTION.VERTICAL:
			if (this.components.length < 1) {
				return 0;
			}
			return Math.max(...this.components.map((element) => element.width));
		case DIRECTION.HORIZONTAL:
			return this.parent.width;
		default:
			throw new Error(`Invalid direction ${this.direction}`);
		}
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	get height() {
		switch (this.direction) {
		case DIRECTION.VERTICAL:
			return this.parent.height;
		case DIRECTION.HORIZONTAL:
			if (this.components.length < 1) {
				return 0;
			}
			return Math.max(...this.components.map((element) => element.height));
		default:
			throw new Error(`Invalid direction ${this.direction}`);
		}
	}
	/**
	 * @override
	 */
	constructor(parent, alignment) {
		if (alignment === ALIGNMENT.CENTER) {
			throw Error("Invalid alignment");
		}
		super(parent, alignment);
		switch (this.direction) {
		case DIRECTION.HORIZONTAL:
			super.append(new DynamicBar(this, ALIGNMENT.LEFT));
			super.append(new DynamicBar(this, ALIGNMENT.CENTER));
			super.append(new DynamicBar(this, ALIGNMENT.RIGHT));
			break;
		case DIRECTION.VERTICAL:
			super.append(new DynamicBar(this, ALIGNMENT.TOP));
			super.append(new DynamicBar(this, ALIGNMENT.CENTER));
			super.append(new DynamicBar(this, ALIGNMENT.BOTTOM));
		}
	}

	/**
	 * @override
	 * @inheritdoc
	 * @note Elements are not appened to the spannedBar but to it's children
	 */
	append(element) {
		return this.first.append(element);
	}

	/**
* @override
* @inheritdoc
*/
	getX(element) {
		if(this.direction === DIRECTION.HORIZONTAL) {
			switch(element) {
			case this.left:
				return 0;
			case this.center:
				const space = this.width - this.right.width - this.left.width;
				return this.left.width + (space / 2) - (element.width / 2);
			case this.right:
				return this.width - element.width;
			default:
				throw new Error(`The element is not contained in this bar`);
			}
		} else {
			return (this.width - element.width) / 2;
		}
	}

	/**
* @override
* @inheritdoc
*/
	getY(element) {
		if(this.direction === DIRECTION.HORIZONTAL) {
			return (this.height - element.height) / 2;
		} else {
			switch(element) {
			case this.top:
				return 0;
			case this.center:
				const space = this.height - this.bottom.height - this.top.height;
				return this.top.height + (space / 2) - (element.height / 2);
			case this.bottom:
				return this.height - element.height;
			default:
				throw new Error(`The element is not in this bar`);
			}
		}
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	empty() {
		this.components.forEach((component) => component instanceof ContainerInterface ? component.empty() : "");
	}
}
