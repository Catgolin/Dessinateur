import Button from "./components/button";
import Message from "./components/message";

export default class ElementFactory {
	/**
	 * @param {ContainerInterface} container
	 */
	constructor(container) {
		this.container = container;
	}
	/**
	 * @type {ContainerInterface}
	 */
	get container() {
		return this._container;
	}
	/**
	 * @param {ContainerInterface} container
	 */
	set container(container) {
		let prototype = container;
		while (prototype != null) {
			if (prototype.constructor.name === "ContainerInterface")
				return this._container = container;
			prototype = prototype.__proto__;
		}
		throw Error(
			"Expected " + container.constructor.name + " to extend ContainerInterface"
		);
	}
	/**
	 * Add a new button to the container
	 * @param {string} descriptionId
	 * @param {function} action
	 * @param {number} preferedWidth
	 * @param {number] preferedHeight
	 */
	button(descriptionId, action, preferedWidth, preferedHeight) {
		const button = new Button(this.container,
			action,
			descriptionId
		);
		if (preferedWidth > 0)
			button.width = preferedWidth;
		if (preferedHeight > 0)
			button.height = preferedHeight;
		this.container.append(button);
	}

	/**
	 * @brief Adds a new message to the container
	 * @param {string} name
	 */
	message(name) {
		const message = new Message(this.container, name);
		this.container.append(message);
	}
}
