/**
 * @abstract
 * @brief Describes any element of the menu
 *
 * @description
 * Defines abstrac methods
 * draw()
 * collision()
 */
export default class ElementInterface {
	/**
	 * @brief The container of this element
	 * @public
	 * @type {ContainerInterface|HTMLCanvasElement}
	 *
	 * @note
	 * The type of the parent is not tested here to avoid circular dependancy
	 * Only the GUI element should have an HTMLCanvasElement as parent.
	 */
	#parent;
	get parent() {
		return this.#parent;
	}
	set parent(element) {
		this.#parent = element;
	}

	/**
	 * @brief Number of pixels between the left and right sides of this element
	 * @type {number}
	 */
	#width;
	get width() {
		return this.#width;
	}
	set width(value) {
		if (isNaN(value)) {
			throw new Error(`Expected width to be a number, got ${value? value.constructor.name : value}`);
		}
		if (value < 0) {
			throw new Error(`The width of an element can't be negative, got ${value}`);
		}
		this.#width = value;
	}
	/**
	 * @brief Number of pixels between the top and bottom sides of this element
	 * @type {number}
	 */
	#height;
	get height() {
		return this.#height;
	}
	set height(value) {
		if (isNaN(value)) {
			throw new Error(`Expected height to be a number, got ${value? value.constructor.name : value}`);
		}
		if (value < 0) {
			throw new Error(`The height of an element can't be negative, got ${value}`);
		}
		this.#height = value;
	}

	/**
	 * @brief Number of pixels from the left side of the parent to the left side of this element
	 * @public
	 * @type {number}
	 */
	#x;
	get x() {
		if (this.parent) {
			return this.parent.getX(this);
		}
		return this.#x;
	}
	set x(value) {
		if (isNaN(value)) {
			throw new Error(`Expected x to be a number, got ${value ? value.constructor.name : value}`);
		}
		this.#x = value;
	}

	/**
	 * @brief Number of pixels from the top side of the parent to the top side of this element
	 * @public
	 * @type {number}
	 */
	#y;
	get y() {
		if (this.parent) {
			return this.parent.getY(this);
		}
		return this.#y;
	}
	set y(value) {
		if (isNaN(value)) {
			throw new Error(`Expected y to be a number, got ${value ? value.constructor.name : value}`);
		}
		this.#y = value;
	}

	/**
	 * @brief (readonly) Number of pixels between the left side of this element and the left side of the canvas
	 * @type {number}
	 *
	 * @description
	 * The position of the element is calculated relative to it's arent.
	 * This allows to build more complex interfaces where elements don't overlap.
	 *
	 * @see x
	 */
	get absoluteX() {
		return this.parent.absoluteX + this.x;
	}
	/**
	 * @brief (readonly) Number of pixels between the top side of this element and the top side of the canvas
	 * @type {number}
	 *
	 * @description
	 * The position of the element is calculated relative to it's arent.
	 * This allows to build more complex interfaces where elements don't overlap.
	 *
	 * @see y
	 */
	get absoluteY() {
		return this.parent.absoluteY + this.y;
	}

	/**
	 * @brief (readonly) The main menu container
	 * @type {GUI}
	 */
	get gui() {
		return this.parent.gui;
	}

	/**
	 * @brief (readonly) The current drawing context
	 * @type {CanvasRenderingContext2D}
	 * @note The context is inherited from the gui's canvas
	 */
	get ctx() {
		return this.gui.canvas.getContext("2d");
	}

	/**
	 * @constructor
	 * @abstract
	 * @param {ContainerInteface} parent
	 * The element of the interface containing this one
	 */
	constructor(parent) {
		if (this.constructor === ElementInterface) {
			throw Error("ElementInterface is abstract and should not be implemented");
		}
		this.parent = parent;
	}


	/**
	 * @brief Draws the element on the canvas
	 * @abstract
	 */
	draw(canvas) {
		throw Error(`${this.constructor.name} should implement the draw method`);
	}

	/**
	 * @abstract
	 * Checks if this element contains the given point
	 * @param {ScreenPoint} point: The point we want to check
	 * @return {ComponentInterface}: the component containing the coordinates
	 * or null if none matches
	 */
	collision(point) {
		throw Error("Collision should be implmented in " + this.constructor.name);
	}
}
/**
 * Alignment types are signed integers of two digits in base 2
 * - Odd numbers represent vertical alignment (top/bottom)
 * - Zero represents centered alignment (vertical or horizontal)
 * - Event not-null numbers represent horizontal alignment (left/right)
 * @note Opposite numbers (x/-x) represent opposite aligments
 * @enum {number}
 */
const ALIGNMENT = {
	TOP: 1,
	BOTTOM: -1,
	CENTER: 0,
	LEFT: 2,
	RIGHT: -2,
};
export {
	ALIGNMENT
};
