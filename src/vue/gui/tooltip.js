import ElementInterface from './elementInterface.js';

export default class Tooltip extends ElementInterface {
	/** @type {string} */
	static background = "yellow";
	/** @type {string} */
	static color = "black";
	/** @type {number} */
	static margin = 5;

	/**
	 * @override
	 * @inheritdoc
	 */
	#x;
	get x() {
		return this.#x;
	}
	set x(value) {
		if (isNaN(value)) {
			throw new Error(`x should be a number, got ${value ? value.controller.name : value}`);
		}
		this.#x = value;
	}
	/**
	 * @override
	 * @inheritdoc
	 */
	#y;
	get y() {
		return this.#y;
	}
	set y(value) {
		if (isNaN(value)) {
			throw new Error(`y should be a number, got ${value ? value.controller.name : value}`);
		}
		this.#y = value;
	}
	/**
	 * @constructor
	 * @param {GUI} gui
	 * @param {number} x
	 * @param {number} y
	 * @param {string} content
	 * @param {color} background: background color
	 * @param {color} color: text color
	 * @param {number} preferedWidth
	 */
	constructor(
		gui,
		x,
		y,
		content,
		background = undefined,
		color = undefined,
		preferedWidth = 200
	) {
		super(gui);
		this.x = x;
		this.y = y;
		this.content = content;
		this.preferedWidth = preferedWidth;
		this.background =
			background === undefined ?
			this.constructor.background :
			background;
		this.color =
			color === undefined ?
			this.constructor.color :
			color;
		this.splitContent();
		this.gui.floatingElements.push(this);
	}

	remove() {
		this.ctx.clearRect(this.x, this.y, this.width, this.height);
		let i = this.gui.floatingElements.indexOf(this);
		if (i > -1) {
			this.gui.floatingElements.splice(i, 1);
		}
	}

	/**
	 * @override
	 * @note Text is always aligned left
	 **/
	draw() {
		const x = this.#leftOrRight(this.ctx.canvas);
		const y = this.#aboveOrBelow(this.ctx.canvas);
		this.ctx.fillStyle = this.background;
		this.ctx.fillRect(x, y, this.width, this.height);
		this.ctx.fillStyle = this.color;
		this.ctx.textAlign = "left";
		this.drawLine(
			x + this.constructor.margin,
			y + this.constructor.margin,
			0
		);
	}

	/**
	 * @param {HTMLCanvasElement} canvas
	 * @return {number} x
	 */
	#leftOrRight(canvas) {
		const rightSpace = canvas.width - this.x;
		if (rightSpace < this.width && rightSpace < this.x) {
			// Print the tooltip on the left side of the mouse
			return this.x - this.width;
		} else {
			return this.x;
		}
	}

	/**
	 * @param {HTMLCanvasElement} canvas
	 * @return {number} y
	 */
	#aboveOrBelow(canvas) {
		const bellowSpace = canvas.height - this.y;
		if (bellowSpace < this.height && bellowSpace < this.y) {
			// Print the tooltip above the mouse
			return this.y - this.height;
		} else {
			return this.y;
		}
	}


	/**
	 * Recursively draw each line untill the end
	 * @private
	 * @param {number} x
	 * @param {number} y
	 * @param {number} n
	 **/
	drawLine(x, y, n) {
		if (n >= this.lines.length) {
			return;
		}
		let measure = this.ctx.measureText(this.lines[n]);
		this.ctx.fillText(this.lines[n], x, y + measure.actualBoundingBoxAscent);
		this.drawLine(x, y + measure.actualBoundingBoxDescent, n + 1);
	}

	/**
	 * @override
	 **/
	get width() {
		const ctx = this.ctx;
		return 2 * this.constructor.margin +
			this.lines.reduce(
				(length, line) => Math.max(length, ctx.measureText(line).width),
				0
			);
	}
	/**
	 * @override
	 **/
	get height() {
		const ctx = this.ctx;
		return 2 * this.constructor.margin +
			this.lines.reduce(
				(height, line) =>
				height +
				ctx.measureText(line).actualBoundingBoxAscent +
				ctx.measureText(line).actualBoundingBoxDescent,
				0
			);
	}

	/**
	 * Splits the content in lines to match the available size
	 * @private
	 **/
	splitContent() {
		let words = this.content.split(" ");
		this.lines = [];
		// Get the smallest between prefered width and available width
		let width = Math.min(
			this.preferedWidth,
			Math.max(
				this.gui.canvas.width - this.x,
				this.x
			)
		);
		let n = 0;
		this.lines[0] = "";
		const ctx = this.ctx;
		for (let word of words) {
			if (ctx.measureText(this.lines[n] + " " + word) > width) {
				n++;
				this.lines[n] = "";
			}
			this.lines[n] += " " + word;
		}
		return this.lines;
	}

	/**
	 * @override
	 */
	collision(x, y) {
		return null;
	}
}
