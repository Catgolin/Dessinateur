import Printer from "./printer";
import Line from "../../geometry/curves/line";
import Segment from "../../geometry/curves/segment";
import ScreenPoint from "../screenPoint";
import LineCollision from "../../physics/collisions/lineCollision";

export default class LinePrinter extends Printer {
	static color = "yellow";

	/**
	 * @override
	 * @inheritdoc
	 */
	static get expectedType() {
		return Line;
	}

	/**
	 * @override
	 * @inheritdoc
	 */
	print(object, camera, ctx) {
		super.print(object, camera, ctx);
		const start = this.startPoint(object, camera, ctx.canvas);
		const end = this.endPoint(object, camera, ctx.canvas);
		if (!start) {
			// The line is not contained in the canvas
			return;
		}
		ctx.beginPath();
		ctx.moveTo(start.x, start.y);
		ctx.lineTo(end.x, end.y);
		ctx.stroke();
	}

	/**
	 * @brief Returns the ScreenPoint at which the first part of the line should be printed
	 * @param {Line} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {CanvasRendreingContext2D} canvas
	 * @return {?ScreenPoint} null if the point is not on the canvas
	 */
	startPoint(object, camera, canvas) {
		const top = this.topCollision(object, camera, canvas);
		if (top) {
			return top;
		}
		const left = this.leftCollision(object, camera, canvas);
		if (left) {
			return left;
		}
		const bottom = this.bottomCollision(object, camera, canvas);
		if (bottom) {
			return bottom;
		}
		return null;
	}

	/**
	 * @brief Returns the ScreenPoint at which the end part of the line should be printed
	 * @param {Line} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {HTMLCanvasElement} canvas
	 * @return {?ScreenPoint} null if the line is not on the canvas
	 */
	endPoint(object, camera, canvas) {
		const right = this.rightCollision(object, camera, canvas);
		if (right) {
			return right;
		}
		const bottom = this.bottomCollision(object, camera, canvas);
		if (bottom) {
			return bottom;
		}
		const left = this.leftCollision(object, camera, canvas);
		if (left) {
			return left;
		}
		return null;
	}

	/**
	 * @brief Returns the point of collision between the object and the top border of the canvas
	 * @param {Line} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {HTMLCanvasElement} canvas
	 * @return {?ScreenPoint} null if the line does not intersect the top border
	 */
	topCollision(object, camera, canvas) {
		const left = camera.canvasToScene(new ScreenPoint(0, 0), canvas);
		const right = camera.canvasToScene(new ScreenPoint(canvas.width, 0), canvas);
		const border = new Segment(left, right);
		const collision = new LineCollision(border.line, object).collisions;
		if (collision.length > 0 && border.distance.contains(collision[0])) {
			return camera.sceneToCanvas(collision[0], canvas);
		}
		return null;
	}

	/**
	 * @brief Returns the point of collision between the object and the left border of the canvas
	 * @param {Line} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {CanvasRenderingContext2D} canvas
	 * @return {?ScreenPoint} null if the line does not intersect the left border
	 */
	leftCollision(object, camera, canvas) {
		const top = camera.canvasToScene(new ScreenPoint(0, 0), canvas);
		const bottom = camera.canvasToScene(new ScreenPoint(0, canvas.height), canvas);
		const border = new Segment(top, bottom);
		const collision = new LineCollision(border.line, object).collisions;
		if (collision.length > 0 && border.distance.contains(collision[0])) {
			return camera.sceneToCanvas(collision[0], canvas);
		}
		return null;
	}

	/**
	 * @brief Returns the point of collision between the object and the bottom border of the canvas
	 * @param {Line} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {CanvasRenderingContext2D} canvas
	 * @return {?ScreenPoint} null if the line does not intersect the bottom border
	 */
	bottomCollision(object, camera, canvas) {
		const left = camera.canvasToScene(new ScreenPoint(0, canvas.height), canvas);
		const right = camera.canvasToScene(new ScreenPoint(canvas.width, canvas.height), canvas);
		const border = new Segment(left, right);
		const collision = new LineCollision(border.line, object).collisions;
		if (collision.length > 0 && border.distance.contains(collision[0])) {
			return camera.sceneToCanvas(collision[0], canvas);
		}
		return null;
	}

	/**
	 * @brief Returns the point of collision between the object and the right border of the canvas
	 * @param {Line} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {CanvasRenderingContext2D} canvas
	 * @return {?ScreenPoint} null if the line does not intersect the right border
	 */
	rightCollision(object, camera, canvas) {
		const top = camera.canvasToScene(new ScreenPoint(canvas.width, 0), canvas);
		const bottom = camera.canvasToScene(new ScreenPoint(canvas.width, canvas.height), canvas);
		const border = new Segment(top, bottom);
		const collision = new LineCollision(border.line, object).collisions;
		if (collision.length > 0 && border.distance.contains(collision[0])) {
			return camera.sceneToCanvas(collision[0], canvas);
		}
		return null;
	}
}
