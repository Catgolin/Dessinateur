import Camera from "../camera";

export default class Printer {
	/**
	 * @brief The default color used to draw the elements
	 * @type {string}
	 *
	 * @description This valud should be override by the child classes
	 */
	static color = "white";

	/**
	 * @brief The color used to draw the elements
	 * @type {string}
	 *
	 * @description Defaults to the static color
	 */
	#color;
	get color() {
		if (this.#color) {
			return this.#color;
		}
		return this.constructor.color;
	}

	/**
	 * @param object
	 * @return {bool}
	 */
	manages(object) {
		return object instanceof this.constructor.expectedType;
	}

	static get expectedType() {
		return null;
	}

	/**
	 * @param object
	 * @param {Camera} camera
	 * @param {CanvasRenderingContext2D} ctx
	 */
	print(object, camera, ctx) {
		if (!this.manages(object)) {
			throw new Error(`Expected first parameter to be a ${this.constructor.expectedType.name}, got ${object ? object.constructor.name : object}`);
		}
		if (!(camera instanceof Camera)) {
			throw new Error(`Expected second parameter to be a Camera, got ${camera ? camera.constructor.name : camera}`);
		}
		if (!(ctx instanceof CanvasRenderingContext2D)) {
			throw new Error(`Expected third parameter to be a CanvasRenderingContext2D, got ${ctx ? ctx.constructor.name : ctx}`);
		}
		ctx.fillStyle = this.color;
		ctx.strokeStyle = this.color;
	}
}
