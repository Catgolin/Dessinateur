import Printer from "./printer";
import Point from "../../geometry/points/point";

export default class RoundPointPrinter extends Printer {
	static color = "white";

	static get expectedType() {
		return Point;
	}

	print(object, camera, ctx) {
		super.print(object, camera, ctx);
		ctx.beginPath();
		const point = camera.sceneToCanvas(object, ctx.canvas);
		ctx.arc(point.x, point.y, 3, 0, 2 * Math.PI);
		ctx.fill();
	}
}
