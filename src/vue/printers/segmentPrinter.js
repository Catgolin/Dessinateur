import Printer from "./printer";
import Segment from "../../geometry/curves/segment";

export default class SegmentPrinter extends Printer {
	static color = "red";

	static get expectedType() {
		return Segment;
	}

	print(object, camera, ctx) {
		super.print(object, camera, ctx);
		const start = this.startPoint(object, camera, ctx.canvas);
		if(!start) {
			return;
		}
		const end = this.endPoint(object, camera, ctx.canvas);
		ctx.beginPath();
		ctx.moveTo(start.x, start.y);
		ctx.lineTo(end.x, end.y);
		ctx.stroke();
	}

	/**
	 * @brief Returns the ScreenPoint at which the first extremity of the line will be drawn
	 * @param {Segment} object The object to print
	 * @param {Camera} camera The camera used to convert coordinates
	 * @param {?ScreenPoint} null if the segment does not cross the canvas
	 */
	startPoint(object, camera, canvas) {
		const vertice = camera.sceneToCanvas(object.vertices[0], canvas);
		if(vertice.x >= 0 && vertice.y >= 0 && vertice.x <= canvas.width && vertice.y <= canvas.height) {
			return vertice;
		}
		const start = camera.getPrinterFor(object.line).startPoint(object.line, camera, canvas);
		if(!start) {
			return null;
		}
		const end = camera.getPrinterFor(object.line).endPoint(object.line, camera, canvas);
		const startPoint = camera.canvasToScene(start, canvas);
		const endPoint = camera.canvasToScene(end, canvas);
		if(new Segment(startPoint, object.vertices[0]).distance.contains(endPoint)) {
			return end;
		}
		return start;
	}

	/**
	 * @brief Returns the ScreenPoint at which the second extremity of the line will be drawn
	 * @param {Segment} object The object to print
	 * @param {Camera} camera The camera used to convert the coordinates
	 * @param {?ScreenPoint} null if the segment does not cross the canvas
	 */
	endPoint(object, camera, canvas) {
		const vertice = camera.sceneToCanvas(object.vertices[1], canvas);
		if(vertice.x >= 0 && vertice.y >= 0 && vertice.x <= canvas.width && vertice.y <= canvas.height) {
			return vertice;
		}
		const start = camera.getPrinterFor(object.line).startPoint(object.line, camera, canvas);
		if(!start) {
			return null;
		}
		const end = camera.getPrinterFor(object.line).endPoint(object.line, camera, canvas);
		const startPoint = camera.canvasToScene(start, canvas);
		const endPoint = camera.canvasToScene(end, canvas);
		if(new Segment(startPoint, object.vertices[0]).distance.contains(endPoint)) {
			return start;
		}
		return end;
	}
}
