/**
 * @brief A position within the screen coordinate system
 *
 * @version 2.0
 * @date 2022-01-26
 * @author Catgolin
 */
export default class ScreenPoint {
	/**
	 * @brief Algebric distance from left to right to the left corner of the canvas
	 * @var {number}
	 */
	#x;
	get x() {
		return this.#x;
	}
	set x(val) {
		if (typeof val !== typeof 1 || isNaN(val)) {
			throw new Error(`Expected x to be a number, got ${val}`);
		}
		this.#x = val;
	}

	#y;
	get y() {
		return this.#y;
	}
	set y(val) {
		if (typeof val !== typeof 1 || isNaN(val)) {
			throw new Error(`Expected y to be a number, got ${val}`);
		}
		this.#y = val;
	}

	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
}
