import translations from "../../translations.json";

/** @class Translator
 * Translates texts in different languages
 */
export default class Translator {
	/**
	 * @param {object} translations,
	 * @param {string} locale
	 */
	constructor(locale = "fr_FR") {
		this.locale = locale;
		this.missingTranslations = new Object();
		this.missingTranslations[this.locale] = [];
	}

	/**
	 * @param {string} text
	 * @return {string}
	 */
	translate(text) {
		const keys = text.split(".");
		let dictionnary = translations;
		for(const key of keys) {
			dictionnary = dictionnary[key];
			if(dictionnary === undefined)
				return this.addMissingTranslation(text);
		}
		const translation = dictionnary[this.locale];
		if(translation === undefined)
			return this.addMissingTranslation(text);
		return translation;
	}

	/**
	 * @param {string} text: the key of the text to translate
	 */
	addMissingTranslation(text) {
		this.missingTranslations[this.locale].push(text);
		console.group(text+" has no translation in "+this.locale);
		console.log(this.missingTranslations);
		console.groupEnd();
		return text;
	}
}
