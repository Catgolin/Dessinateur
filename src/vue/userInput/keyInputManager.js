import UserActionController from "../../controller/userActionController";

export default class KeyInputManager {
	/**
	 * @param {HTMLCanvasElement} canvas
	 * @param {UserActionController} controller
	 */
	constructor(controller, canvas) {
		this.controller = controller;
		canvas.addEventListener("keydown", this.keyDown.bind(this));
		this.isFocused = true;
	}
	get isFocused() {
		return this._isFocused;
	}
	set isFocused(focus) {
		if(typeof focus !== "boolean")
			throw Error("Illegal value: type "+typeof(focus)+" is not boolean");
		this._isFocused = focus;
	}
	/**
	 * @param {KeyboardEvent} event
	 */
	keyDown(event) {
		if(!this.isFocused)
			return;
	}
	/**
	 * @param {KeyboardEvent} event
	 */
	keyUp(event) {
	}
}
