import UserInputController from "../../controller/userInputController";
import ScreenPoint from "../screenPoint";

export default class MouseInputManager {

	/**
	 * @brief (readonly) Position at which a dragging started
	 * @type {ScreenPoint}
	 */
	#dragStartPosition;
	get dragStartPostion() {
		return this.#dragStartPosition;
	}

	/**
	 * @brief (readonly) Is the mouse currently dragging
	 * @type {bool}
	 */
	get dragging() {
		return this.dragStartPosition instanceof ScreenPoint &&
			this.dragStartPosition.x >= 0 &&
			this.dragStartPosition.y >= 0;
	}
	set dragging(value) {
		if(value !== false) {
			throw new Error(`dragging can only be set manually to false, got ${value}`);
		}
		this.#dragStartPosition = undefined;
	}

	/**
	 * @brief The event controller to which delegate actions
	 * @type {UserInputController} controller
	 */
	#controller;
	get controller() {
		return this.#controller;
	}
	set controller(object) {
		if (!(object instanceof UserInputController)) {
			throw new Error(`Expected the controller to be an instance of UserInputController, got ${object ? object.controller.name : object}`);
		}
		this.#controller = object;
	}

	/**
	 * @brief Is the left button of the mouse currently pressed
	 * @type {bool}
	 */
	#isMouseDown;
	get isMouseDown() {
		return this.#isMouseDown;
	}
	set isMouseDown(value) {
		if (typeof value !== typeof true) {
			throw new Error(`Expected isMouseDown to be a boolean, got ${typeof value}`);
		}
		this.#isMouseDown = value;
	}

	/**
	 * @param {UserInputController} controller
	 * @param {HTMLCanvasElement} canvas
	 */
	constructor(controller, canvas) {
		this.controller = controller;
		canvas.addEventListener("mousedown", this.mouseDown.bind(this));
		canvas.addEventListener("mouseup", this.mouseUp.bind(this));
		canvas.addEventListener("mousemove", this.mouseMove.bind(this));
		canvas.addEventListener("mouseout", this.mouseOut.bind(this));
		canvas.addEventListener("mouseover", this.mouseOver.bind(this));
		this.leftMouseDown = false;
		this.dragging = false;
	}

	/**
	 * @param {MouseEvent} event
	 */
	mouseDown(event) {
		if (event.button !== 0) {
			return;
		}
		if (event.offsetX < 0 || event.offsetY < 0) {
			return;
		}
		const point = new ScreenPoint(event.offsetX, event.offsetY);
		this.isMouseDown = true;
		this.controller.prepareClick(point);
	}

	/**
	 * @param {MouseEvent} event
	 */
	mouseUp(event) {
		if (this.isMouseDown !== true || event.button !== 0) {
			return;
		}
		this.isMouseDown = false;
		if (event.offsetX < 0 || event.offsetY < 0) {
			return;
		}
		const point = new ScreenPoint(event.offsetX, event.offsetY);
		if (this.dragging) {
			this.dragging = false;
			this.controller.dragEnd(point);
		} else {
			this.controller.leftClick(point);
		}
	}

	/**
	 * @param {MouseEvent} event
	 */
	mouseOver(event) {}

	/**
	 * @param {MouseEvent} event
	 */
	mouseOut(event) {
		const point = new ScreenPoint(event.offsetX, event.offsetY);
		if (this.dragging) {
			this.controller.dragCancel(point);
		}
		this.#isMouseDown = false;
		this.dragging = false;
	}

	/**
	 * @param {MouseEvent} event
	 */
	mouseMove(event) {
		const point = new ScreenPoint(event.offsetX, event.offsetY);
		if (this.dragging) {
			this.controller.dragMove(this.dragStartPosition, point);
		} else if (this.isMouseDown && this.controller.dragStart(point)) {
			this.dragStartPosition = point;
		} else {
			this.controller.hover(point);
		}
	}
}
