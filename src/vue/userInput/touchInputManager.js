export default class TouchInputManager {
	/**
	 * @param {HTMLCanvasElement} canvas
	 */
	constructor(canvas) {
		canvas.addEventListener("touchstart", this.touchStart);
		canvas.addEventListener("touchmove", this.touchMove);
		canvas.addEventListener("touchend", this.touchEnd);
		canvas.addEventListener("touchcancel", this.touchCancel);
	}
	/**
	 * @param {HTMLCanvasElement} canvas
	 * @param {TouchEvent} event
	 */
	touchStart(canvas, event) {
	}
	/**
	 * @param {HTMLCanvasElement} canvas
	 * @param {TouchEvent} event
	 */
	touchMove(canvas, event) {
	}
	/**
	 * @param {HTMLCanvasElement} canvas
	 * @param {TouchEvent} event
	 */
	touchEnd(canvas, event) {
	}
	/**
	 * @param {HTMLCanvasElement} canvas
	 * @param {TouchEvent} event
	 */
	touchCancel(canvas, event) {
	}
}
