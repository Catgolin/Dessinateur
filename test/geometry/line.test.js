import Point from "../../src/geometry/points/point";
import {
	expect
} from "chai";
import Line from "../../src/geometry/curves/line";
import Segment from "../../src/geometry/curves/segment";
import LineCollision from "../../src/physics/collisions/lineCollision";
import SegmentCollision from "../../src/physics/collisions/segmentCollision";
import Collection from "../../src/collections/collection";

describe("Lines", function () {
	describe("Line creation", function () {
		it("should create a line from cartesian coefficients",
			function () {
				const [a, b, c] = [Math.random(), Math.random(), Math.random()];
				const line = new Line(a, b, c);
				expect(line.cartesianCoefficients).to.be.an("object");
				expect(line.cartesianCoefficients.a).to.equal(a);
				expect(line.cartesianCoefficients.b).to.equal(b);
				expect(line.cartesianCoefficients.c).to.equal(c);
			}
		);

		it("should refuse to create a line if a = b = 0",
			function () {
				expect(() => new Line(0, 0, Math.random())).to.throw("a and b cannot both equal 0");
			}
		);

		it("should create a line if a xor b is null",
			function () {
				const a = new Line(0, Math.random(), Math.random());
				expect(a.cartesianCoefficients).to.be.an("object");
				expect(a.cartesianCoefficients.a).to.equal(0);
				const b = new Line(Math.random(), 0, Math.random());
				expect(b.cartesianCoefficients.b).to.equal(0);
			}
		);

		it("should throw an error if some coefficients are not numbers",
			function () {
				expect(() => new Line(null, undefined, NaN)).to.throw("Expected three numbers as cartesian coefficients but got object, undefined, number (NaN)");
			}
		);

		it("should create a vertical line",
			function () {
				const A = new Point(0, 0);
				const B = new Point(0, 1);
				const AB = Line.fromPoints(A, B);
				expect(Math.abs(AB.cartesianCoefficients.a)).to.equal(1);
				expect(AB.cartesianCoefficients.b).to.equal(0);
				expect(AB.cartesianCoefficients.c).to.equal(0);
			}
		);

		it("should create a line from two points",
			function () {
				const A = new Point(0, 1);
				const B = new Point(1, 2);
				const AB = Line.fromPoints(A, B);
				expect(Math.abs(AB.cartesianCoefficients.a)).to.equal(1);
				expect(Math.abs(AB.cartesianCoefficients.b)).to.equal(1);
				expect(Math.abs(AB.cartesianCoefficients.c)).to.equal(1);
			}
		);
		it("should throw an error if the points are not valid",
			function () {
				const [x, y] = [Math.random(), Math.random()];
				expect(
					() => Line.fromPoints(null, 10)
				).to.throw("Expected two Points, got null and Number");
				expect(
					() => Line.fromPoints(new Point(x, y), new Point(x, y))
				).to.throw(`Can't create a line with two points located at the same position at (${x}, ${y})`);
			}
		);
	});

	describe("Lines distances", function () {
		it("should return an error when we are measuring the distance to something else than a point",
			function () {
				const d = new Line(Math.random(), Math.random(), Math.random());
				const e = new Line(Math.random(), Math.random(), Math.random());
				expect(
					() => d.distance.to(undefined)
				).to.throw('Expected destination to be a Point, got undefined');
				expect(
					() => d.distance.to(e)
				).to.throw('Expected destination to be a Point, got Line');
			}
		);
		it("should get distance from point to line",
			function () {
				const A = new Point(2, 2);
				const d = Line.fromPoints(new Point(0, 0), new Point(1, 0));
				expect(d.distance.to(A)).to.equal(2);
				expect(d.distance.squareTo(A)).to.equal(4);
			}
		);

		it("should get that a point is on the line",
			function () {
				const A = new Point(10, 10);
				const d = new Line(1, -1, 0);
				expect(d.distance.contains(A), "A\in (d)").to.be.true;
				expect(d.distance.containsStrict(A), "A\in(d) strict").to.be.true;
			}
		);
		it("should get that a point is not on the line",
			function () {
				const A = new Point(10, 20);
				const d = new Line(1, 1, 0);
				expect(d.distance.contains(A), "A\notin(d)").to.be.false;
				expect(d.distance.containsStrict(A), "A\notin(d) strict").to.be.false;
			}
		);
		it("should get that a point is on the line even with computing error",
			function () {
				const A = new Point(10, Math.sqrt(10) ** 2);
				const d = new Line(1, -1, 0);
				expect(d.distance.contains(A), "A\in(d)").to.be.true;
				expect(d.distance.containsStrict(A), "A\in(d) strict").to.be.true;
			}
		);
	});

	describe("Intersection of lines", function () {
		it("should get one parallels class",
			function () {
				const a = new Line(1, 0, 0);
				const b = new Line(Math.random(), 0, Math.random());
				const collider = new LineCollision(a, b);
				expect(collider.equivalence).to.have.lengthOf(1);
				expect(collider.equivalence[0]).to.be.an.instanceof(Collection);
				expect(collider.equivalence[0].contains(a), "a should be in the class").to.be.true;
				expect(collider.equivalence[0].contains(b), "b should be in the class").to.be.true;
			}
		);
		it("should get two parallels class",
			function () {
				const a = new Line(Math.random(), 0, Math.random());
				const b = new Line(Math.random(), 0, Math.random());
				const c = new Line(Math.random(), Math.random(), 0);
				const collider = new LineCollision(a, b, c);
				expect(collider.equivalence, "Two vertical lines and one not-vertical should give 2 parallel classes").to.have.lengthOf(2);
				const parallels = collider.equivalence.find(equivalence => equivalence.elements.length === 2);
				expect(parallels, "There should be one class with the 2 vertical elements").not.to.be.undefined;
				expect(parallels.contains(a), "a should be in the first class").to.be.true;
				expect(parallels.contains(b), "b should be in the first class").to.be.true;
				expect(parallels.contains(c), "c should not be in the first class").to.be.false;
				const other = collider.equivalence.find(
					equivalence => equivalence.elements.length === 1
				);
				expect(other).not.to.be.undefined;
				expect(other.contains(a), "a should not be in the second class").to.be.false;
				expect(other.contains(b), "b should not be in the second class").to.be.false;
				expect(other.contains(c), "c should be in the second class").to.be.true;
			}
		);
		it("should get three parallels classes",
			function () {
				const A = new Point(Math.random(), Math.random());
				const B = new Point(A.x, Math.random());
				const C = new Point(Math.random(), Math.random());
				const D = new Point(C.x, Math.random());
				const AB = Line.fromPoints(A, B);
				const CD = Line.fromPoints(C, D);
				const BC = Line.fromPoints(B, C);
				const AC = Line.fromPoints(A, C);
				const collider = new LineCollision(AB, CD, BC, AC);
				const textCoordinates = [A, B, C, D].reduce(
					(string, point) => string + point.name + "=(" + point.x + ", " + point.y + ")\n",
					"\n"
				);
				const message = `Expected ${AB.name} and ${CD.name} to be parallel with {textCoordinates}, so we should get 3 equivalence classes`;
				expect(collider.equivalence, message).to.have.lengthOf(3);
			}
		);

		it("should get no collision between parallel lines",
			function () {
				const a = new Line(Math.random(), 0, Math.random());
				const b = new Line(Math.random(), 0, Math.random());
				const c = new Line(Math.random(), 0, Math.random());
				const collider = new LineCollision(a, b, c);
				expect(collider.collisions, `Vertical lines should not have any collision point but we got ${collider.collisions.length}`).to.have.lengthOf(0);
				expect(collider.intersection.elements, `We should not have any intersection, but we got some: ${collider.intersection.elements}`).to.have.lengthOf(0);
			}
		);
		it("should get one collision",
			function () {
				const vertical = new Line(1, 0, -1);
				const horizontal = new Line(0, 1, -1);
				const linear = new Line(1, -1, 0);
				const collider = new LineCollision(vertical, horizontal);
				// All three lines collide at the same point
				// so there should be 1 collision point of coordinates (1, 1)
				expect(collider.collisions, "Two non-parallel lines should have exactly one collision point").to.have.lengthOf(1);
				expect(collider.add(linear).collisions, "Three lines intersecting at the same point should have only one collision point").to.have.lengthOf(1);
				expect(collider.collisions[0].x).to.equal(1, `Expected x=1, got x=${collider.collisions[0].x}`);
				expect(collider.collisions[0].y).to.equal(1, `Expected y=1, got y=${collider.collisions[0].y}`);
				expect(collider.intersection.elements, "The intersection between multiple lines meeting at the same point should be reduced to one point").to.have.lengthOf(1);
			}
		);

		it("should get intersection of two identical lines",
			function () {
				const a = new Line(1, 0, 1);
				const b = new Line(3, 0, Math.sqrt(3) ** 2);
				const collider = new LineCollision(a, b);
				expect(collider.collisions, "We shouldn't have any collision points").to.have.lengthOf(0);
				expect(collider.intersection.elements, "Intersection of identical lines should be the line itself").to.have.lengthOf(1);
				const intersection = collider.intersection.elements[0];
				expect(collider.add(intersection).intersection.elements).to.have.lengthOf(1);
				expect(collider.intersection.elements[0]).to.equal(intersection);
			}
		);
		it("should get no intersection between lines colliding at different points",
			function () {
				const a = new Line(Math.random(), Math.random(), Math.random());
				const b = new Line(Math.random(), Math.random(), Math.random());
				const c = new Line(Math.random(), Math.random(), Math.random());
				const collider = new LineCollision(a, b, c);
				expect(collider.collisions, "We should have three points of intersection between those three lines").to.have.lengthOf(3);
				expect(collider.intersection.elements, `The intersection between those three line should be empty but we got ${collider.intersection.elements}`).to.have.lengthOf(0);
			}
		);

		it("should give no collision point between (almost) parallel lines",
			function () {
				const a = new Line(1, 2, 3);
				const b = new Line(1, 1 / (1 / 2), 1 / (1 / 3));
				const collider = new LineCollision(a, b);
				expect(collider.equivalence).to.have.lengthOf(1);
				expect(collider.collisions).to.have.lengthOf(0);
			}
		);
	});
});

describe("Segments", function () {
	describe("Segment creation", function () {
		it("should create a segment",
			function () {
				const A = new Point(0, 0);
				const B = new Point(0, 1);
				const AB = new Segment(A, B);
				expect(Math.abs(AB.cartesianCoefficients.a)).to.equal(1);
				expect(AB.cartesianCoefficients.b).to.equal(0);
				expect(AB.cartesianCoefficients.c).to.equal(0);
				expect(AB.vertices).to.have.lengthOf(2);
				expect(AB.vertices).to.contain(A);
				expect(AB.vertices).to.contain(B);
			}
		);
	});

	describe("Segment distances", function () {
		it("should give distance between a point and the segment",
			function () {
				const A = new Point(0, 0);
				const B = new Point(1, 1);
				const C = new Point(-2, 0);
				const AB = new Segment(A, B);
				expect(AB.distance.to(C)).to.equal(2);
				expect(AB.distance.squareTo(C)).to.equal(4);
			}
		);
		it("should get that a point is on the segment",
			function () {
				const A = new Point(0, 0);
				const B = new Point(2, 2);
				const C = new Point(1, 1);
				const AB = new Segment(A, B);
				expect(AB.distance.contains(C), "C\in[AB]").to.be.true;
				expect(AB.distance.containsStrict(C), "C\in]AB[").to.be.true;
			}
		);
		it("should get that a point is not on the segment",
			function () {
				const A = new Point(0, 0);
				const B = new Point(1, 1);
				const C = new Point(2, 2);
				const AB = new Segment(A, B);
				expect(AB.distance.contains(C), "C\notin[AB]").to.be.false;
				expect(AB.distance.containsStrict(C), "C\notin]AB[").to.be.false;
			}
		);
		it("should contain not strictly a point at the extremity of the line",
			function () {
				const A = new Point(0, 0);
				const B = new Point(1, 1);
				const C = new Point(1, 1);
				const AB = new Segment(A, B);
				expect(AB.distance.contains(A), "A\in[AB]").to.be.true;
				expect(AB.distance.contains(B), "B\in[AB]").to.be.true;
				expect(AB.distance.contains(C), "C\in[AB]").to.be.true;
				expect(AB.distance.containsStrict(A), "A\notin ]AB[").to.be.false;
				expect(AB.distance.containsStrict(B), "B\notin ]AB[").to.be.false;
				expect(AB.distance.containsStrict(C), "C\notin ]AB[").to.be.false;
			}
		);
		it("should get that a point is on the segment with computing error",
			function () {
				const A = new Point(0, 0);
				const B = new Point(2, 2);
				const C = new Point(Math.sqrt(2) ** 2, Math.sqrt(2) ** 2);
				const AB = new Segment(A, B);
				expect(AB.distance.contains(C), "C\in[AB]").to.be.true;
				expect(AB.distance.containsStrict(C), "C\notin ]AB[").to.be.false;
			}
		);
	});

	describe("Collision of segments", function () {
		it("should return a collision",
			function () {
				const [A, B, C, D] = [
					new Point(-1, 0), new Point(1, 0),
					new Point(0, -1), new Point(0, 1)
				];
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				// Tests
				expect(collider.collisions).to.have.lengthOf(1);
				// StrictCollision identical
				expect(collider.strictCollisions).to.have.lengthOf(1);
				expect(collider.strictCollisions[0].x).to.equal(0);
				expect(collider.strictCollisions[0].y).to.equal(0);
			}
		);
		it("should not detect collision",
			function () {
				const [A, B, C, D] = [
					new Point(-1, 0), new Point(1, 0),
					new Point(0, 1), new Point(0, 2)
				];
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				expect(collider.collisions).to.have.lengthOf(0);
				expect(collider.strictCollisions).to.have.lengthOf(0);
			}
		);
		it("should detect non strict collision",
		   function() {
			   const [A, B, C, D] = [
				   new Point(-1, 0), new Point(1, 0),
				   new Point(0, 0), new Point(0, -1)
			   ];
			   const AB = new Segment(A, B);
			   const CD = new Segment(C, D);
			   const collider = new SegmentCollision(AB, CD);
			   expect(collider.collisions).to.have.lengthOf(1);
			   expect(collider.strictCollisions).to.have.lengthOf(0);
		   }
		  );
	});

	describe("Intersection of segments", function () {
		it("should get parallel classes of segments as segments",
			function () {
				const A = new Point(0, 0);
				const B = new Point(A.x, 1);
				const C = new Point(1, 0);
				const D = new Point(C.x, 1);
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				const names = [AB, CD].reduce(
					(string, segment) => `${string}${segment.name} = (${segment.vertices[0].x}, ${segment.vertices[0].y}) -> (${segment.vertices[1].x}, ${segment.vertices[1].y})`,
					"\n"
				);
				expect(collider.equivalence.length, `Expected ${AB.name} and ${CD.name} to be parallel, with ${names}, so there should be exactly 1 equivalence class`).to.equal(1);
				expect(collider.equivalence[0].elements).to.contain(AB);
				expect(collider.equivalence[0].elements).to.contain(CD);
			}
		);
		it("should return one segment included in the other",
			function () {
				const [A, B, C, D, E, F] = [
					new Point(0, 0), new Point(3, 0),
					new Point(1, 0), new Point(Math.sqrt(3) ** 2, 0),
					new Point(1, 0), new Point(2, 0),
				];
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const EF = new Segment(E, F);
				// Intersection (with rounding problems)
				const collider = new SegmentCollision(AB, CD);
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.have.lengthOf(1);
				const intersectionC = collider.intersection.elements[0].vertices.find((p) => p.distance.contains(C));
				const intersectionB = collider.intersection.elements[0].vertices.find((p) => p.distance.contains(B));
				expect(intersectionC, "There should be an intersection at the same position as C").not.to.be.undefined;
				expect(intersectionB, "There should be an intersection at the same position as B").not.to.be.undefined;
				// Refining intersection (without rounding problems)
				expect(collider.add(EF).intersection.elements).to.have.lengthOf(1);
				const intersectionE = collider.intersection.elements[0].vertices.find((p) => p.distance.contains(E));
				const intersectionF = collider.intersection.elements[0].vertices.find((p) => p.distance.contains(F));
				expect(intersectionE, "There should be an intersection at the same position as E").not.to.be.undefined;
				expect(intersectionF, "There should be an intersection at the same position as F").not.to.be.undefined;
			}
		);
		it("should return intersection between two overlapping segments",
			function () {
				 const A = new Point(0, 0);
				 const B = new Point(2, 2);
				 const C = new Point(1, 1);
				 const D = new Point(3, 3);
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.have.lengthOf(1);
				const intersection = collider.intersection.elements[0];
				expect(
					intersection.vertices.find(
						p => p.x === 1 && p.y === 1
					)).not.to.be.undefined;
				expect(
					intersection.vertices.find(
						p => p.x === 2 && p.y === 2
					)).not.to.be.undefined;
			}
		);
		it("should return no intersection",
			function () {
				const [A, B, C, D] = [
					new Point(0, 0), new Point(1, 1),
					new Point(2, 2), new Point(3, 3),
				];
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				expect(collider.intersection.elements).to.have.lengthOf(0);
				expect(
					collider.strictIntersection.elements).to.have.lengthOf(0);
			}
		);
	});

	describe("Strict collisions and intersection", function () {
		it("should have a not-strict collisions on polygonal line (1)",
			function () {
				const [A, B, C] = [
					new Point(Math.random(), Math.random()),
					new Point(1, 1),
					new Point(Math.random(), Math.random()),
				];
				const AB = new Segment(A, B);
				const BC = new Segment(B, C);
				const collider = new SegmentCollision(AB, BC);
				expect(collider.collisions).to.have.lengthOf(1);
				expect(collider.strictCollisions).to.be.empty;
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.be.empty;
			}
		);
		it("should have not-strict collision on polygonal line (2)",
			function () {
				const [A, B, C, D] = [
					new Point(Math.random(), Math.random()),
					new Point(2, 2),
					new Point(Math.sqrt(2) ** 2, Math.sqrt(2) ** 2),
					new Point(Math.random(), Math.random()),
				];
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				expect(collider.collisions).to.have.lengthOf(1);
				expect(collider.strictCollisions).to.be.empty;
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.be.empty;
			}
		);

		it("should have not-strict collision on line",
			function () {
				const [A, B, C, D] = [
					new Point(0, 0), new Point(2, 2),
					new Point(1, 1), new Point(1, 0),
				];
				const AB = new Segment(A, B);
				const CD = new Segment(C, D);
				const collider = new SegmentCollision(AB, CD);
				expect(collider.collisions).to.have.lengthOf(1);
				expect(collider.strictCollisions).to.be.empty;
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.be.empty;
			}
		);
	});
});
