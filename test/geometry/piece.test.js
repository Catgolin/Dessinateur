import {expect} from "chai";
import Segment from "../../src/geometry/curves/segment";
import Piece from "../../src/geometry/pieces/piece";
import Point from "../../src/geometry/points/point";
import PointCollision from "../../src/physics/collisions/pointCollision";

describe("Pieces", function () {
	/** @type {Point} */
	let A, B, C, D, E, F, G, H;
	/** @type {Segment} */
	let AB, BC, CD, DA, AE, EF, FG, GA, BE, EF2, FC, CB, GE;
	beforeEach(
		function () {
			// Generate points
			const [x1, y1, x2, y2, xb, yd, w1, w2, h1, h2] = [
				Math.random(), Math.random(), // A
				Math.random() + 2, Math.random() + 2, // x2, y2
				Math.random() + 1, Math.random() + 1, // xb, yd
				Math.random() + 1, Math.random() + 1, // width
				Math.random() + 1, Math.random() + 1, // height
			];
			A = new Point(x1, y1);
			// B \in [AE]
			B = new Point(xb, ((y2 - y1) * (xb + w1) + w1 * y2) / w1);
			// C \in AEFG
			C = new Point(Math.random() + 1, Math.random() + 1);
			// D \in [AG]
			D = new Point((yd + x2 * y1 - x1 * (y1 + h1)) / h1, yd);
			E = new Point(x1 + w1, y2);
			F = new Point(x2 + w2, y2 + h2);
			G = new Point(x2, y1 + h1);
			// H \in [EF]
			H = new Point((C.y * (F.x - E.x) - F.x * E.y + E.x * F.y) / (F.y - E.y), C.y);
			// Generate segments
			AB = new Segment(A, B);
			BC = new Segment(B, C);
			CD = new Segment(C, D);
			DA = new Segment(D, A);
			AE = new Segment(A, E);
			EF = new Segment(E, F);
			FG = new Segment(F, G);
			GA = new Segment(G, A);
			BE = new Segment(B, E);
			EF2 = new Segment(E, F);
			FC = new Segment(F, C);
			CB = new Segment(C, B);
			GE = new Segment(G, E);
		}
	);

	describe("Piece creation", function () {
		it("should create a piece",
			function () {
				const piece = new Piece(AB, BE, EF, FC, CD, DA);
				expect(piece.edges).to.have.lengthOf(6);
			}
		);
		it("should work even if the lines have different references",
			function () {
				A.x += Math.random();
				A.y += Math.random();
				const Abis = new Point(Math.sqrt(A.x) ** 2, Math.sqrt(A.y) ** 2);
				const Cbis = new Point(C.x, C.y);
				const ACbis = new Segment(A, Cbis);
				const CAbis = new Segment(C, Abis);
				const ABC = new Piece(AB, ACbis, CAbis);
				expect(ABC.vertices).to.have.lengthOf(3);
			}
		);
		it("should return an error if path is not closed",
			function () {
				expect(() => new Piece(AB, BC, DA)).to.throw("The piece was not closed");
			}
		);
	});

	describe.skip("Piece distances", function () {
		it("should get the distance of a point to a piece",
			function () {
				// Maybe we can keep skipping this test until we need it
				// In the meantime, return 0 will be fine
				const ABCD = new Piece(AB, BC, CD, DA);
				expect(ABCD.distance.to(F)).to.equal(Math.sqrt(2));
				expect(ABCD.distance.squareTo(F)).to.equal(2);
			}
		);
		it("should check if a point is inside a piece",
			function () {
				const AEFG = new Piece(AE, EF, FG, GA);
				expect(AEFG.distance.to(C)).to.equal(0);
				expect(AEFG.distance.squareTo(C)).to.equal(0);
				expect(AEFG.distance.contains(C), "C\\in AEFG").to.be.true;
				expect(AEFG.distance.containsStrict(C), "C\\in AEFG strict").to.be.true;
			}
		);
		it("should check if a point is not inside a piece",
			function () {
				const ABCD = new Piece(AB, BC, CD, DA);
				expect(ABCD.distance.contains(F), "F\\notin AEFG").to.be.false;
				expect(ABCD.distance.containsStrict(C), "F\\notin AEFG strict").to.be.false;
			}
		);
		it("should check if a point is on the edge of a piece",
			function () {
				const AEFG = new Piece(AE, EF, FG, GA);
				expect(AEFG.distance.to(B)).to.equal(0);
				expect(AEFG.distance.squareTo(B)).to.equal(0);
				expect(AEFG.distance.contains(B), "B\in AEFG").to.be.true;
				expect(AEFG.distance.containsStrict(B), "B\notin AEFG strict").to.be.false;
			}
		);
	});

	describe.skip("Intersections of pieces", function () {
		it("should get one point of non-strict intersection",
			function () {
				const ABCD = new Piece(AB, BC, CD, DA);
				const EFG = new Piece(EF, FG, GE);
				const collider = new PieceCollision(ABCD, EFG);
				expect(collider.collisions).to.have.lengthOf(1);
				expect(collider.strictCollisions).to.be.empty;
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.be.empty;
				expect(collider.collision[0].x).to.equal(1);
				expect(collider.collision[0].y).to.equal(1);
				const intersection = collider.intersection.elements[0];
				expect(intersection).to.be.an.instanceof(Point);
			}
		);
		it("should get one line of non-strict intersection",
			function () {
				const ABCD = new Pice(AB, BC, CD, DA);
				const BEFC = new Piece(BE, EF, FC, BC);
				const collider = new PieceCollision(ABCD, BEFC);
				// Expect one non-strict Segment element in the intersection
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.be.empty;
				const intersection = collider.intersection.elements[0];
				expect(intersection).to.be.an.instanceof(Segment);
				// Expect the element to be at the same position as BC
				let pCollider = new PointCollision(B, C);
				expect(
					pCollider.add(intersection.vertices).collisions,
					"The extremities of the intersection should be at the same position as B and C"
				).to.have.lengthOf(2);
				// What do we expect of collisions ?
				/*
				expect(collider.collisions).to.have.lengthOf(2);
				expect(collider.strictCollisions).to.be.empty;
				pCollider = new PointCollision(B, C);
				expect(
				    pCollider.add(collider.collisions).collisions,
				    "The collision points between the two pieces should be at the same position as B and C"
				) .to.have.lengthOf(2);
				*/
			}
		);
		it("should get intersection if one polygon is included in the other",
			function () {
				const AEFG = new Piece(AE, EF, FG, GA);
				const BEFC = new Piece(BE, EF2, FC, CB);
				const collider = new PieceCollision(AEFG, BEFC);
				// Expect exactly one element of intersection
				const intersection = collider.intersection.elements;
				const strictIntersection = collider.strictCollisions.elements;
				expect(intersection).to.have.lengthOf(1);
				expect(strictIntersection).to.have.lengthOf(1);
				// Check if the intersection is BEFC
				const pCollider = new PointCollision(B, E, F, C);
				expect(pCollider.add(...intersection[0].vertices).collisions)
					.to.have.lengthOf(4);
				// What do we expect of collisions?
				expect(collider.collisions).to.have.lengthOf(3);
				expect(collider.strictCollisions).to.be.empty;
			}
		);
		it("should get one triangle of intersection",
			function () {
				const CG = new Segment(C, G);
				const GB = new Segment(G, B);
				const AECD = new Piece(AE, EC, CD, DA);
				const GBC = new Piece(GB, BC, CG);
				const collider = new PieceCollider(AECD, GBC);
				// Expect 1 triangle
				const intersection = collider.intersection.elements;
				const strictIntersection = collider.strictIntersection.elements;
				expect(intersection).to.have.lengthOf(1);
				expect(strictIntersection).to.have.lengthOf(1);
				expect(intersection[0].vertices).to.have.lengthOf(3);
				expect(strictIntersection[0].vertices).to.have.lengthOf(3);
				// Expect a triangle with BC and not G nor A
				expect(intersection[0].distance.containsStrict(C)).to.be.false;
				expect(intersection[0].distance.containsStrict(B)).to.be.false;
				expect(intersection[0].distance.contains(C)).to.be.true;
				expect(intersection[0].distance.contains(B)).to.be.true;
				expect(intersection[0].distance.contains(G)).to.be.false;
				expect(intersection[0].distance.contains(A)).to.be.false;
				// 3 collision points: C, B, and a point between BG and CD
				expect(collider.collisions).to.have.lengthOf(3);
				// Do we count the point between BG and CD as a strict point?
				expect(collider.strictCollisions).to.have.lengthOf(0);
				// B and C are in collider.collision
				let pCollider = new PointCollision(...collider.collision);
				expect(pCollider.add(B, C).collisions).to.have.lengthOf(2);
				// B and C are not in collider.strictCollisions
				pCollider = new PointCollision(...collider.strictCollisions);
				expect(pCollider.add(B, C).collisions).to.have.lengthOf(0);
			}
		);
		it("should have an intersection not connected",
			function () {
				const GH = new Segment(G, H);
				const HA = new Segment(H, A);
				const AEFGH = new Piece(AE, EF, FG, GH, HA);
				const BEFC = new Piece(BE, EF, FC);
				const collider = new PieceCollider(AEFGH, BEFC);
				expect(collider.intersection.elements).to.have.lengthOf(2);
				expect(collider.intersection.elements).to.have.lengthOf(2);
			}
		);
		it("should have an intersection not strictly connected",
			function () {
				const GD = new Segment(G, D);
				const DB = new Segment(D, B);
				const CE = new Segment(C, E);
				const EH = new Segment(E, H);
				const HD = new Segment(H, D);
				const GDBCEF = new Piece(GD, DB, BC, CE, EF, FG);
				const AEHD = new Piece(AE, EH, HD, DA);
				const collider = new PieceCollision(GDBCEF, AEHD);
				expect(collider.intersection.elements).to.have.lengthOf(1);
				expect(collider.strictIntersection.elements).to.have.lengthOf(2);
			}
		);
	});
});
