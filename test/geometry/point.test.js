import Point from "../../src/geometry/points/point";
import {expect} from "chai";
import PointCollision from "../../src/physics/collisions/pointCollision";
import Collection from "../../src/collections/collection";
import Segment from "../../src/geometry/curves/segment";

describe("Points", function () {
	describe("Create a point", function () {
		it("should create a point",
			function () {
				const x = Math.random() * 100;
				const y = Math.random() * 100;
				const point = new Point(x, y);
				expect(point.x, "The x coordinate of the point should have been set").to.equal(x);
				expect(point.y, "The y coordinate of the point should have been set").to.equal(y);
			}
		);

		it("should not create a point with invalid coordinates",
			function () {
				expect(() => new Point(undefined, Math.random())).to.throw("Expected x to be a number, undefined given");
				expect(() => new Point(null, Math.random())).to.throw("Expected x to be a number, object given");
				expect(() => new Point(NaN, Math.random())).to.throw("Expected x to be a number, NaN given");
				expect(() => new Point(Math.random(), undefined)).to.throw("Expected y to be a number, undefined given");
				expect(() => new Point(Math.random(), null)).to.throw("Expected y to be a number, object given");
				expect(() => new Point(1, NaN)).to.throw("Expected y to be a number, NaN given");
			}
		);
	});

	describe("Distance between points", function () {
		it("should never strictly contain a point",
			function () {
				const A = new Point(Math.random(), Math.random());
				const B = new Point(A.x, A.y);
				expect(A.distance.containsStrict(B), "A should not contain B").to.be.false;
				expect(A.distance.containsStrict(A), "A shouldn't contain itself").to.be.false;
			}
		);

		it("should contain an identical point",
			function () {
				const A = new Point(Math.random(), Math.random());
				const B = new Point(A.x, A.y);
				expect(A.distance.contains(B), "A should contain B").to.be.true;
				expect(B.distance.contains(A), "A contains B but B doesn't contain A").to.be.true;
			}
		);

		it("should contain almost identical point",
			function () {
				const A = new Point(Math.random(), Math.random());
				const B = new Point(Math.sqrt(A.x) ** 2, Math.sqrt(A.y) ** 2);
				expect(A.distance.contains(B), "A should contain B").to.be.true;
				expect(B.distance.contains(A), "A contains B but B doesn't contain A").to.be.true;
			}
		);

		it("should not contain different point",
			function () {
				const A = new Point(Math.random(), Math.random());
				const B = new Point(Math.random(), Math.random() + 1);
				expect(A.distance.contains(B), "A shoud not contain B").to.be.false;
				expect(B.distance.contains(A), "A doesn't contain B but B contains A").to.be.false;
			}
		);
	});

	describe("Detect collision between points",
		function () {
			/** @type {Point} */
			let A, B, C;
			/** @type {Point} */
			let Abis, Bclose;
			beforeEach(function () {
				A = new Point(Math.random(), Math.random() + 1);
				B = new Point(Math.random(), Math.random() + 2);
				C = new Point(Math.random() - 1, Math.random());
				Abis = new Point(A.x, A.y);
				Bclose = new Point(Math.sqrt(B.x) ** 2, Math.sqrt(B.y) ** 2);
			});

			it("should return no collision",
				function () {
					const collider = new PointCollision(A, B);
					expect(collider.collisions.length, "The list of collisions between A and B should be an empty array").to.equal(0);
					expect(collider.intersections).to.be.an.instanceof(Collection);
				}
			);

			it("should return a collision for identical points",
				function () {
					const collider = new PointCollision(A, Abis);
					expect(collider.collisions.length, "There should be exactly one collision between A and Abis").to.equal(1);
					expect(collider.collisions[0].x, "The x coordinate of the collision point should correspond to A.x").to.equal(A.x);
					expect(collider.collisions[0].y, "The y coordinate of the collision point should correspond to A.y").to.equal(A.y);
					expect(collider.add(C, B).collisions.length, "Adding new unrelated points to the list shoud not have increased the number of collisions").to.equal(1);
				}
			);

			it("should have identical collisions and intersections lists",
				function () {
					const collider = new PointCollision(A, B, C, Abis);
					collider.add(Bclose, A);
					const intersections = collider.intersections;
					const collisions = collider.collisions;
					expect(intersections).to.be.an.instanceof(Collection);
					expect(collisions).to.be.an("array");
					expect(intersections.elements).to.have.lengthOf(collisions.length);
					collisions.forEach(
						(point) => {
							const intersection = intersections.elements.find(p => p.x === point.x && p.y === point.y);
							expect(intersection, "There should be one point of intersection for each collision point").not.to.be.undefined;
						}
					);
				}
			);

			it("should return a collision for close identical points",
				function () {
					const collider = new PointCollision(B, Bclose);
					expect(collider.collisions.length).to.equal(1);
				}
			);

			it("should identify three equivalence classes",
				function () {
					const collider = new PointCollision(A, B, C, Abis);
					expect(collider.equivalence).to.be.an("array");
					expect(collider.equivalence).have.lengthOf(3);
				}
			);

			it("should throw an error if adding a bad type",
				function () {
					const collider = new PointCollision();
					expect(() => collider.add(new Segment(A, B))).to.throw();
				}
			);
			it("should throw an error if creating collision with bad type",
				function () {
					expect(() => new PointCollision(new Segment(A, B))).to.throw();
				}
			);
		});
});
