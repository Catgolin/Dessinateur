// loading every test file for webpack to compiile them before testing
const context = require.context(".", true, /.+\.test\.js?$/);
context.keys().forEach(context);
module.exports = context;
