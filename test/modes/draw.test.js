import {
	expect
} from "chai";

import MCDG from "../../src/index";
import Button from "../../src/vue/gui/components/button";
import DrawMode from "../../src/controller/modes/drawMode";
import Point from "../../src/geometry/points/point";
import Line from "../../src/geometry/curves/line";
import Segment from "../../src/geometry/curves/segment";
import OperationInterface from "../../src/controller/operations/operationInterface";
import CreatePointOperation from "../../src/controller/operations/createPointOperation";
import CreatePointAction from "../../src/controller/actions/createPointAction";
import CreateLineOperation from "../../src/controller/operations/createLineOperation";
import CreateLineAction from "../../src/controller/actions/createLineAction";
import CreateSegmentAction from "../../src/controller/actions/createSegmentAction";
import CreateSegmentOperation from "../../src/controller/operations/createSegmentOperation";

describe("Draw mode", function () {
	let app;
	let selectModeButton;

	beforeEach(function () {
		app = new MCDG();
		selectModeButton = app.menu.right.top.components.find((component) => component instanceof Button && component.description === "Passer en mode dessin");
	});

	describe("Select the draw mode", function () {
		it("should have a button on the top right corner",
			function () {
				expect(selectModeButton, "Expected to have a button for the draw mode on the top right corner").not.to.be.undefined;
				selectModeButton.click();
				expect(app.mode).to.be.an.instanceof(DrawMode, "Expect the app to be in draw mode when the button is clicked");
			}
		);
	});

	describe("Create a point", function () {
		describe("The operation should be available", function () {
			it("should have the operation inside the mode",
				function () {
					selectModeButton.click();
					expect(app.mode.operations.point, "There should be a CreatePointOperation").to.be.an.instanceof(CreatePointOperation);
					assertOperationInterface(app, "Placer un point", "Cliquez pour placer un point");
				}
			);
			it("should be the default click operation",
				function () {
					selectModeButton.click();
					expect(app.mode.default.click).to.be.an.instanceof(CreatePointOperation);
				}
			);
		});
		describe("The operation should work on click", function () {
			it("should be able to prepare the operation",
				function () {
					const point = new Point(Math.random(), Math.random());
					const operation = app.mode.operations.point;
					expect(operation.actions).to.have.lengthOf(0, "The operation should not be initialized");
					expect(operation.prepare(point).actions).to.have.lengthOf(1, "The operation should prepare one action");
					expect(operation.actions[0]).to.be.an.instanceof(CreatePointAction);
				}
			);
			it("should create a point on the default layer",
				function () {
					selectModeButton.click();
					const point = new Point(Math.random(), Math.random());
					app.mode.click(point);
					expect(app.scene.layers[0].elements).to.have.lengthOf(1, "There should be one element in the layer");
					expect(app.scene.layers[0].elements[0].x).to.equal(point.x, "Point.x should be set");
					expect(app.scene.layers[0].elements[0].y).to.equal(point.y, "Point.y should be set");
				}
			);
		});
	});
	describe("Create a line", function () {
		describe("The operation should be available", function () {
			it("should have the operation inside the mode",
				function () {
					selectModeButton.click();
					expect(app.mode.operations.line, "There should be a CreateLineOperation").to.be.an.instanceof(CreateLineOperation);
					assertOperationInterface(app, "Tracer une droite", "Cliquez sur un point de la droite");
				}
			);
			it("should be the default drag operation",
				function () {
					selectModeButton.click();
					expect(app.mode.default.drag).to.be.an.instanceof(CreateLineOperation);
				}
			);
			it("should print help message when dragging started",
				function () {
					const start = new Point(Math.random(), Math.random());
					selectModeButton.click();
					app.mode.dragStart(start);
					expect(app.mode.operation).to.be.an.instanceof(CreateLineOperation);
					expect(app.menu.top.center.components[0].title).to.equal("Déplacez le curseur pour indiquer la direction de la droite puis relâchez");
					app.menu.bottom.center.components[0].click();
					expect(app.mode.operation).to.be.null;
				}
			);
			it("should print help message after a click when operation was selected",
				function () {
					const start = new Point(Math.random(), Math.random());
					selectModeButton.click();
					app.mode.selectOperation("line");
					app.mode.click(start);
					expect(app.menu.top.center.components[0].title).to.equal("Cliquez sur un deuxième point pour indiquer la direction de la droite");
				}
			);
		});
		describe.skip("The operation should be previewed", function () {
			it("should preview the operation when dragging",
				function () {
					const start = new Point(Math.random(), Math.random());
					const end = new Point(Math.random(), Math.random());
					selectModeButton.click();
					app.mode.dragStart(start);
					app.mode.move(end);
					expect(app.scene.preview.elements).to.have.lengthOf(1, "There should be one element to preview");
					const preview = app.scene.preview.elements[0];
					expect(preview).to.be.an.instanceOf(Line);
					expect(preview.distance.contains(start), "The preview line should contain the starting point").to.be.true;
					expect(preview.distance.contains(end), "The preview line should contain the end point").to.be.true;
					app.menu.top.center.components[0].click();
					expect(app.scene.preview.elements).to.have.lengthOf(0, "The preview should be removed on cancel");
				}
			);
			it("should preview the operation when clicking",
				function () {
					const start = new Point(Math.random(), Math.random());
					const end = new Point(Math.random(), Math.random());
					selectModeButton.click();
					app.mode.selectOperation("line");
					app.mode.click(start);
					app.mode.move(-end);
					expect(app.scene.preview.elements).to.have.lenghtOf(1);
					const preview = app.scene.preview.elements[0];
					expect(preview).to.be.an.instanceOf(Line);
					expect(preview.distance.contains(start), "The preview line should contain the starting point").to.be.true;
					expect(preview.distance.contains(end), "The preview line should contain the end point").to.be.true;
				}
			);
		});
		describe("The operation should create a line", function () {
			it("should prepare the operation",
				function () {
					selectModeButton.click();
					const start = new Point(Math.random(), Math.random());
					const end = new Point(Math.random(), Math.random());
					const operation = app.mode.operations.line;
					expect(operation.actions).to.have.lengthOf(0, "the operation should be empty");
					operation.prepare(start);
					expect(operation.actions).to.have.lengthOf(0, "There should not be any action with only one point");
					operation.prepare(end);
					expect(operation.actions).to.have.lengthOf(1, "There should be one action when two pointsare added");
					expect(operation.actions[0]).to.be.an.instanceof(CreateLineAction);
				}
			);
			it("should create a line",
				function () {
					selectModeButton.click();
					const start = new Point(Math.random(), Math.random());
					const end = new Point(Math.random(), Math.random());
					app.mode.dragStart(start);
					app.mode.dragEnd(end);
					expect(app.scene.preview.elements).to.have.lengthOf(0, "The preview layer should be emptied");
					expect(app.scene.layers[0].elements).to.have.lengthOf(1, "There should be one element in the default layer");
					const line = app.scene.layers[0].elements[0];
					expect(line).to.be.an.instanceof(Line);
					expect(line.distance.contains(start)).to.be.true;
					expect(line.distance.contains(end)).to.be.true;
				}
			);
		});
	});
	describe("Create a segment", function () {
		describe("The operation should be available", function () {
			it("should have the operation inside the mode",
				function () {
					selectModeButton.click();
					expect(app.mode.operations.segment, "There should be a CreateSegmentOperation").to.be.an.instanceof(CreateSegmentOperation);
					assertOperationInterface(app, "Tracer un segment", "Cliquez sur le point de départ du segment");
				}
			);
		});
		describe.skip("The operation should be previewed", function () {
			it("should preview the operation on drag and drop",
				function () {}
			);
			it("should preview the operation on clicks",
				function () {}
			);
		});
		describe("The operation should create a segment", function () {
			it("should prepare the operation",
				function () {
					const start = new Point(Math.random(), Math.random());
					const end = new Point(Math.random(), Math.random());
					selectModeButton.click();
					const operation = app.mode.operations.segment;
					expect(operation.actions).to.have.lengthOf(0, "The operation should not contain any action when starting");
					operation.prepare(start);
					expect(operation.actions).to.have.lengthOf(1, "The operation should prepare the creation of the first point");
					operation.prepare(end);
					expect(operation.actions).to.have.lengthOf(3, "The operation should be ready");
					expect(operation.ready, "The operation should be ready").to.be.true;
					expect(operation.actions[0]).to.be.an.instanceof(CreatePointAction);
					expect(operation.actions[1]).to.be.an.instanceof(CreatePointAction);
					expect(operation.actions[2]).to.be.an.instanceof(CreateSegmentAction);
				}
			);
			it("should create a line segment when dragging",
				function () {
					const start = new Point(Math.random(), Math.random());
					const end = new Point(Math.random(), Math.random());
					selectModeButton.click();
					app.mode.selectOperation("segment");
					app.mode.dragStart(start);
					app.mode.dragEnd(end);
					expect(app.scene.preview.elements).to.have.lengthOf(0, "There should not be any preview when the operation is finished");
					expect(app.scene.layers[0].elements).to.have.lengthOf(3, "There should be trhee elements on the scene");
					assertSegment(app, start, end);
				}
			);
		});
	});
});

const assertOperationInterface = function (app, description, help) {
	const button = app.menu.left.top.components.find((element) => element instanceof Button && element.description === description);
	expect(button, `There should be a button with the description '${description}'`).not.to.be.undefined;
	button.click();
	expect(app.mode.operation).to.be.an.instanceof(OperationInterface, "The operation should have been selected");
	expect(app.menu.top.center.components).to.have.lengthOf(1, "There should be a help message on top center menu");
	expect(app.menu.top.center.components[0].title).to.equal(help, "Help message not valid");
	expect(app.menu.bottom.center.components).to.have.lengthOf(1, "There should be a button on the bottom center of the menu");
	expect(app.menu.bottom.center.components[0]).to.be.an.instanceof(Button);
	expect(app.menu.bottom.center.components[0].description).to.equal("Annuler");
	app.menu.bottom.center.components[0].click();
	expect(app.mode.operation).to.be.null;
};

const assertSegment = function (app, start, end) {
	const elements = app.scene.layers[0].elements;
	const first = elements.find((element) => element instanceof Point && element.distance.contains(start));
	const second = elements.find((element) => element instanceof Point && element.distance.contains(end));
	const line = elements.find((element) => element instanceof Segment);
	expect(first, "The start point should be in the scene").not.to.be.undefined;
	expect(second, "The end point should be in the scene").not.to.be.undefined;
	expect(line.distance.contains(first), "The starting point should be in the segment").to.be.true;
	expect(line.distance.contains(second), "the end point should be in the segment").to.be.true;
	expect(line.distance.containsStrict(first), "The first point should be an extremity").to.be.false;
	expect(line.distance.containsStrict(second), "the second point should be on the extremity of the segment").to.be.false;
};
