import {
	expect
} from "chai";

import Camera from "../../src/vue/camera";
import Point from "../../src/geometry/points/point";
import Segment from "../../src/geometry/curves/segment";
import Line from "../../src/geometry/curves/line";
import ScreenPoint from "../../src/vue/screenPoint";
import RoundPointPrinter from "../../src/vue/printers/roundPointPrinter";
import SegmentPrinter from "../../src/vue/printers/segmentPrinter";
import LinePrinter from "../../src/vue/printers/linePrinter";
// import DashedSegmentPrinter from "../../src/vue/printers/dashedSegmentPrinter";


describe("Printing objects on the canvas", function () {
	/** @var {{width: {number}, height: {number}, getContext: {function}} */
	let canvas;
	/** @var {Point} */
	let center;
	/** @var {Point} */
	let point;
	/** @var {Camera} */
	let camera;

	beforeEach(function () {
		canvas = {
			width: 800,
			height: 500,
			getContext: () => {
				return {};
			}
		};
		center = new Point(0, 0);
		point = new Point(10, 30);
		camera = new Camera();
	});

	describe("Camera movements", function () {
		describe("Get coordinates of a point on the camera", function () {
			xit("should have canvasToScene and sceneToCanvas reciprocal bijections",
				function () {
					// Declarations
					/** @var {Point} */
					const scenePoint = new Point(Math.random(), Math.random());
					/** @var {ScreenPoint} */
					const screenPoint = new ScreenPoint(Math.random(), Math.random());
					/** @var {function}: {ScreenPoint} -> {ScreenPoint} */
					const canvasToCanvas = camera.canvasToScene(camera.sceneToCanvas(scenePoint, canvas), canvas);
					/** @var {function}: {Point} -> {Point} */
					const sceneToScene = camera.sceneToCanvas(camera.canvasToScene(screenPoint, canvas), canvas);
					// Tests
					expect(canvasToCanvas.distance.contains(screenPoint), `Distance to screenPoint ${canvasToCanvas.distance.to(screenPoint)}`).to.be.true;
					expect(sceneToScene.distance.contains(scenePoint), `Distance fo scenePoint ${sceneToScene.distance.to(scenePoint)}`).to.be.true;
				}
			);

			describe("From the plane to the canvas", function () {
				it("should not accept a screen point as entry",
					function () {
						expect(() => camera.sceneToCanvas(new ScreenPoint(Math.random(), Math.random()))).to.throw("Expected point to be a Point, got ScreenPoint");
						expect(() => camera.sceneToCanvas(null)).to.throw("Expected point to be a Point, got null");
						expect(() => camera.sceneToCanvas(10)).to.throw("Expected point to be a Point, got Number");
						expect(() => camera.sceneToCanvas(new Point(Math.random(), Math.random()))).to.throw("Expected canvas to be a HTMLCanvasElement, got undefined");
					}
				);

				it("should put the center of the plane at the center of the canvas",
					function () {
						expect(camera.sceneToCanvas(center, canvas)).to.be.an.instanceof(ScreenPoint);
						expect(camera.sceneToCanvas(center, canvas).x).to.equal(400);
						expect(camera.sceneToCanvas(center, canvas).y).to.equal(250);
					}
				);

				it("should position points that are not in the center",
					function () {
						// The point (10, 30) translates to coordinates (400 + 10, 250 - 30)
						expect(camera.sceneToCanvas(point, canvas).x).to.equal(410);
						expect(camera.sceneToCanvas(point, canvas).y).to.equal(220);
					}
				);

				it("should work when we resize the canvas",
					function () {
						canvas.width = 1000;
						canvas.height = 2000;
						expect(camera.sceneToCanvas(center, canvas).x).to.equal(500);
						expect(camera.sceneToCanvas(center, canvas).y).to.equal(1000);
						// The point (10, 30) translates to coordinates (500 + 10, 1000 - 30)
						expect(camera.sceneToCanvas(point, canvas).x).to.equal(510);
						expect(camera.sceneToCanvas(point, canvas).y).to.equal(970);
					}
				);
			});

			describe("From the canvas to the plane", function () {
				it("should only accept ScreenPoint as argument",
					function () {
						expect(() => camera.canvasToScene(center, canvas)).to.throw("Expected point to be a ScreenPoint, got Point");
						expect(() => camera.canvasToScene()).to.throw("Expected point to be a ScreenPoint, got undefined");
						expect(() => camera.canvasToScene(Math.random(), canvas)).to.throw("Expected point to be a ScreenPoint, got Number");
						expect(() => camera.canvasToScene(new ScreenPoint(Math.random(), Math.random()))).to.throw("Expected canvas to be a HTMLCanvasElement, got undefined");
					}
				);
				it("should get the center of the canvas as the center of the plane",
					function () {
						expect(camera.canvasToScene(new ScreenPoint(400, 250), canvas)).to.be.an.instanceof(Point);
						expect(camera.canvasToScene(new ScreenPoint(400, 250), canvas).x).to.equal(0);
						expect(camera.canvasToScene(new ScreenPoint(400, 250), canvas).y).to.equal(0);
					}
				);

				it("should get the coordinates of the top left corner of the canvas",
					function () {
						expect(camera.canvasToScene(new ScreenPoint(0, 0), canvas)).to.be.an.instanceof(Point);
						// (0,0) is on the left corner of the 800 units wide screen: the center of the screen is 400 units right
						expect(camera.canvasToScene(new ScreenPoint(0, 0), canvas).x).to.equal(-400, "Failed to get the x coordinate with canvasToScene");
						// (0,0) is on the top of the 500 units tall screen: the center is 250 units bellow and the y axis is inverted
						expect(camera.canvasToScene(new ScreenPoint(0, 0), canvas).y).to.equal(250, "Failed to get the y coordinate with canvasToScene");
					}
				);

				it("should work when we resize the canvas",
					function () {
						canvas.width = 1000;
						canvas.height = 2000;
						// (500, 1000) is at the center of the screen, which is set to be equal to point (0,0) of the canvas.
						expect(camera.canvasToScene(new ScreenPoint(500, 1000), canvas).x).to.equal(0);
						expect(camera.canvasToScene(new ScreenPoint(500, 1000), canvas).y).to.equal(0);
						// (0,0) is 500 units left to the center
						expect(camera.canvasToScene(new ScreenPoint(0, 0), canvas).x).to.equal(-500);
						// (0,0) is 1000 units above the center, and the y axis is inverted
						expect(camera.canvasToScene(new ScreenPoint(0, 0), canvas).y).to.equal(1000);
					}
				);
			});
		});

		describe.skip("Translation the camera", function () {
			it("should expect numbers",
				function () {
					expect(camera.move(NaN, Math.random())).to.throw("Expected x to be a number, got NaN");
					expect(camera.move("test", Math.random())).to.throw("Expected x to be a number, got string");
					expect(camera.move(Math.random(), NaN)).to.throw("Expected y to be a number, got NaN");
					expect(camera.move(Math.random(), "test")).to.throw("Expected y to be a number, got string");
				}
			);
			it("should move the center of the camera",
				function () {
					camera.move(10, 20);
					// (0,0) is 10 units left and 20 units bellow of the new center of the screen,
					// and the center of the screen has coordinates (400, 250) in the canvas' frame
					// so it should give us position (400-10, 250+20)
					expect(camera.sceneToCanvas(new Point(0, 0)).x).to.equal(390);
					expect(camera.sceneToCanvas(new Point(0, 0)).y).to.equal(270);
					camera.move(-5, -10);
					// New center is (10, 20) + (-5, -10) = (5, 10)
					expect(camera.sceneToCanvas(new Point(0, 0)).x).to.equal(395);
					expect(camera.sceneToCanvas(new Point(0, 0)).y).to.equal(280);
				}
			);
		});

		describe.skip("Rotation the camera", function () {
			it("should keep the center",
				function () {
					camera.move(10, 20);
					camera.rotate(Math.random() * Math.PI * 2);
					expect(camera.sceneToCanvas(new Point(10, 20)).x).to.equal(400);
					expect(camera.sceneToCanvas(new Point(10, 20)).y).to.equal(250);
				}
			);

			it("should make a quarter of a turn",
				function () {
					camera.rotate(Math.PI / 2);
					// Rotation: (1, 0) -> (0, 1)
					// Change of coordinates: (0,1) -> (400, 249)
					expect(camera.sceneToCanvas(new Point(1, 0)).x).to.equal(400);
					expect(camera.sceneToCanvas(new Point(1, 0)).y).to.equal(249);
					// Rotation: (0, 1) -> (-1, 0)
					// Change of coordinates: (-1, 0) -> (399, 250)
					expect(camera.sceneToCanvas(new Point(0, 1)).x).to.equal(399);
					expect(camera.sceneToCanvas(new Point(0, 1)).y).to.equal(250);
					// Rotation: (-1, 0) -> (0, -1)
					// Change of coordinates: (0, -1) -> (400, 251)
					expect(camera.sceneToCanvas(new Point(-1, 0)).x).to.equal(400);
					expect(camera.sceneToCanvas(new Point(-1, 0)).y).to.equal(251);
					// Rotation: (0, -1) -> (1, 0)
					// Change of coordinates: (1, 0) -> (401, 250)
					expect(camera.sceneToCanvas(new Point(0, -1)).x).to.equal(401);
					expect(camera.sceneToCanvas(new Point(0, -1)).y).to.equal(250);
				}
			);
		});

		describe.skip("Scaling of the camera", function () {
			it("should preserve the center",
				function () {
					camera.move(10, 20);
					camera.scale(Math.random());
					expect(camera.sceneToCanvas(new Point(10, 20)).x).to.equal(400);
					expect(camera.sceneToCanvas(new Point(10, 20)).y).to.equal(250);
				}
			);
			it("should scale up",
				function () {
					camera.move(10, 20);
					// 1 unit of the scene is two units on the canvas
					camera.scale(2);
					// Translate to move (10, 20) to the center: (2, 4) -> (2-10, 4-20)
					// Scale up: (-8, -16) -> (-16, -32)
					// Change of reference: (-16, -32) -> (400-16, 250+32)
					expect(camera.sceneToCanvas(new Point(2, 4)).x).to.equal(384);
					expect(camera.sceneToCanvas(new Point(2, 4)).y).to.equal(282);
					// 1 unit of the scene is now four units on the canvas
					camera.move(-5, -10);
					// New center is (10, 20) + (5, -10) = (5, 10)
					camera.scale(2);
					// Translate (5, 10) to the center: (2, 4) -> (2-5, 4-10)
					// Scale up: (-3, -8) -> (-12, -24)
					// Change of reference: (-12, -24) -> (400-12, 250+24)
					expect(camera.sceneToCanvas(new Point(2, 4)).x).to.equal(388);
					expect(camera.sceneToCanvas(new Point(2, 4)).y).to.equal(274);
				}
			);

			it("should scale down",
				function () {
					camera.move(10, 20);
					camera.scale(1 / 2);
					// Translation: (2, 4) -> (2-10, 4-20)
					// Scale down: (-8, -16) -> (-4, -8)
					// Change of reference: (-4, -8) -> (400-4, 250+8)
					expect(camera.sceneToScreen(new Point(2, 4)).x).to.equal(396);
					expect(camera.sceneToScreen(new Point(2, 4)).y).to.equal(258);
				}
			);
		});
	});

	describe("Get appropriate printers for each object", function () {
		it("should get the RoundPointPrinter",
			function () {
				expect(camera.getPrinterFor(point)).to.be.an.instanceof(RoundPointPrinter);
				expect(camera.getPrinterFor(point).print).to.be.a("function");
				expect(() => camera.getPrinterFor(point).print()).to.throw("Expected first parameter to be a Point, got undefined");
				expect(() => camera.getPrinterFor(point).print(point)).to.throw("Expected second parameter to be a Camera, got undefined");
				expect(() => camera.getPrinterFor(point).print(point, camera)).not.to.throw;
				expect(() => camera.printObject(point)).not.to.throw;
			}
		);

		it("should get the SegmentPrinter",
			function () {
				const segment = new Segment(center, point);
				expect(camera.getPrinterFor(segment)).to.be.an.instanceof(SegmentPrinter);
				expect(() => camera.getPrinterFor(segment).print(point)).to.throw("Expected first parameter to be a Segment");
				expect(() => camera.getPrinterFor(segment).print(segment)).to.throw("Expected second parameter to be a Camera, got undefined");
				expect(() => camera.getPrinterFor(segment).print(segment, camera)).not.to.throw;
				expect(() => camera.printObject(segment)).not.to.throw;
			}
		);

		it("should get the LinePrinter",
			function () {
				const line = new Line(Math.random(), Math.random(), Math.random());

				//Aucune idée de pourquoi la ligne qui suit ne fonctionne pas, peut-être à cause de conflits avec Babel ?
				expect(camera.getPrinterFor(line)).to.be.an.instanceof(LinePrinter);
				// expect(camera.getPrinterFor(line).constructor.name).to.equal('LinePrinter');

				expect(() => camera.getPrinterFor(line).print()).to.throw("Expected first parameter to be a Line, got undefined");
				expect(() => camera.getPrinterFor(line).print(line)).to.throw("Expected second parameter to be a Camera, got undefined");
				expect(() => camera.getPrinterFor(line).print(line, camera)).not.to.throw;
				expect(() => camera.printObject(line)).not.to.throw;
			}
		);

		xit("should be able to set a different printer for one specific object",
			function () {
				const segment = new Segment(center, point);
				const other = new Segment(center, point);
				const printer = new DashedSegmentPrinter(segment, 10, 10);
				camera.addPrinter(printer);
				expect(camera.getPrinterFor(segment)).to.equal(printer);
				expect(camera.getPrinterFor(other)).to.be.an.instanceof(SegmentPrinter);
				expect(camera.getPrinterFor(segment).print()).to.throw("Expected object to be a Segment, got undefined");
			}
		);

		xit("should be able to set printer for a type of objects",
			function () {
				const segment = new Segment(center, point);
				camera.setPrinter(DashedSegmentPrinter);
				expect(camera.getPrinterFor(center)).to.be.an.instanceof(RoundPointPrinter);
				expect(camera.getPrinterFor(segment)).to.be.an.instanceof(DashedSegmentPrinter);
			}
		);
	});
});
