import {
	expect
} from "chai";
import Button from "../../src/vue/gui/components/button";
import {
	DIRECTION
} from "../../src/vue/gui/containers/barInterface";
import GUI from "../../src/vue/gui/containers/gui";
import ElementInterface, {
	ALIGNMENT
} from "../../src/vue/gui/elementInterface";
import Translator from "../../src/vue/translator";
import ScreenPoint from "../../src/vue/screenPoint";

describe("Placement of menu elements", function () {
	let canvas;
	let translator;
	let gui;
	beforeEach(
		function () {
			canvas = {
				width: 800,
				height: 500,
			};
			translator = new Translator("fr_FR");
			gui = new GUI(canvas, translator);
		}
	);

	describe("General GUI object", function () {
		it("should have the gui at the top left corner",
			function () {
				assertPosition(gui, "gui", 0, 0, 0, 0);
				assertSize(gui, "gui", canvas.width, canvas.height);
			}
		);
	});

	describe("Main menu bars", function () {
		it("should have the top bar",
			function () {
				assertAlignment(gui.top, "top",
					ALIGNMENT.TOP, DIRECTION.HORIZONTAL);
				assertPosition(gui.top, "top",
					0, 0, 0, 0);
				assertSize(gui.top, "top",
					canvas.width, 0);

				// Top left
				assertAlignment(gui.top.left, "top>left",
					ALIGNMENT.LEFT, DIRECTION.HORIZONTAL);
				assertPosition(gui.top.left, "top>left",
					0, 0, 0, 0);
				assertSize(gui.top.left, "top>left",
					0, 0);

				// Top center
				assertAlignment(gui.top.center, "top>center",
					ALIGNMENT.CENTER, DIRECTION.HORIZONTAL);
				const center = canvas.width / 2;
				assertPosition(gui.top.center, "top>center",
					center, 0, center, 0);
				assertSize(gui.top.center, "top>center",
					0, 0);

				// Top right
				const right = canvas.width;
				assertAlignment(gui.top.right, "top>right",
					ALIGNMENT.RIGHT, DIRECTION.HORIZONTAL);
				assertPosition(gui.top.right, "top>right",
					right, 0, right, 0);
				assertSize(gui.top.right, "top>right",
					0, 0);

				// Top top
				expect(() => gui.top.top).to.throw();
			}
		);

		it("should have the left bar",
			function () {
				assertAlignment(gui.left, "left",
					ALIGNMENT.LEFT, DIRECTION.VERTICAL);
				assertPosition(gui.left, "left",
					0, 0, 0, 0);
				assertSize(gui.left, "left",
					0, canvas.height);

				// Left top
				assertAlignment(gui.left.top, "left>top",
					ALIGNMENT.TOP, DIRECTION.VERTICAL);
				assertPosition(gui.left.top, "left>top",
					0, 0, 0, 0);
				assertSize(gui.left.top, "left>top",
					0, 0);

				// Left bottom
				assertAlignment(gui.left.bottom, "left>bottom",
					ALIGNMENT.BOTTOM, DIRECTION.VERTICAL);
				const bottom = canvas.height;
				assertPosition(gui.left.bottom, "left>bottom",
					0, bottom, 0, bottom);
				assertSize(gui.left.bottom, "left>bottom",
					0, 0);

				// Left center
				assertAlignment(gui.left.center, "left>center",
					ALIGNMENT.CENTER, DIRECTION.VERTICAL);
				const center = canvas.height / 2;
				assertPosition(gui.left.center, "left>center",
					0, center, 0, center);
				assertSize(gui.left.center, "left>center",
					0, 0);

				// Let left
				expect(() => gui.left.left).to.throw();
			}
		);

		it("sould have the bottom bar",
			function () {
				const height = canvas.height;
				assertAlignment(gui.bottom, "bottom",
					ALIGNMENT.BOTTOM, DIRECTION.HORIZONTAL);
				assertPosition(gui.bottom, "bottom",
					0, height, 0, height);
				assertSize(gui.bottom, "bottom",
					canvas.width, 0);

				// Bottom left
				assertAlignment(gui.bottom.left, "bottom>left",
					ALIGNMENT.LEFT, DIRECTION.HORIZONTAL);
				assertPosition(gui.bottom.left, "bottom>left",
					0, 0, 0, height);
				assertSize(gui.bottom.left, "bottom>left",
					0, 0);

				// Bottom right
				const right = canvas.width;
				assertAlignment(gui.bottom.right, "bottom>right",
					ALIGNMENT.RIGHT, DIRECTION.HORIZONTAL);
				assertPosition(gui.bottom.right, "bottom>right",
					right, 0, right, height);
				assertSize(gui.bottom.right, "bottom>right",
					0, 0);

				// Bottom center
				const center = canvas.width / 2;
				assertAlignment(gui.bottom.center, "bottom>center",
					ALIGNMENT.CENTER, DIRECTION.HORIZONTAL);
				assertPosition(gui.bottom.center, "bottom>center",
					center, 0, center, height);
				assertSize(gui.bottom.center, "bottom>center",
					0, 0);

				// Bottom bottom
				expect(() => gui.bottom.bottom).to.throw();
			}
		);

		it("should have the right bar",
			function () {
				const right = canvas.width;
				assertAlignment(gui.right, "right",
					ALIGNMENT.RIGHT, DIRECTION.VERTICAL);
				assertPosition(gui.right, "right",
					right, 0, right, 0);
				assertSize(gui.right, "right",
					0, canvas.height);

				// Right top
				assertAlignment(gui.right.top, "right>top",
					ALIGNMENT.TOP, DIRECTION.VERTICAL);
				assertPosition(gui.right.top, "right>top",
					0, 0, right, 0);
				assertSize(gui.right.top, "right>top",
					0, 0);

				// Right center
				const center = canvas.height / 2;
				assertAlignment(gui.right.center, "right>center",
					ALIGNMENT.CENTER, DIRECTION.VERTICAL);
				assertPosition(gui.right.center, "right>center",
					0, center, right, center);
				assertSize(gui.right.center, "right>center",
					0, 0);

				// Right bottom
				const bottom = canvas.height;
				assertAlignment(gui.right.bottom, "right>bottom",
					ALIGNMENT.BOTTOM, DIRECTION.VERTICAL);
				assertPosition(gui.right.bottom, "right>bottom",
					0, bottom, right, bottom);
				assertSize(gui.right.bottom, "right>bottom",
					0, 0);

				// Right right
				expect(() => gui.right.right).to.throw();
			}
		);
	});

	describe("Buttons", function () {
		it("should create a button with text",
			function () {
				const throwingFunction = () => {
					throw Error("Super test");
				};
				gui.left.center.add.button("test", throwingFunction, 10, 10);
				expect(gui.left.center.components, "Expected the left center bar to have 1 element").to.have.lengthOf(1);
				expect(gui.left.center.components[0], "Expected the element of the bar to be a button").to.be.an.instanceof(Button);
				const height = canvas.height / 2;
				expect(gui.left.center.components[0].parent, "Expected the parent of the button to be the bar").to.equal(gui.left.center);
				expect(gui.left.center.components[0].x).to.equal(0, "Expected x=0");
				expect(gui.left.center.components[0].y).to.equal(0, "Expected y=0");
				// [group] Tests to diagnose why absoluteX and absoluteY ant numbers if the next one fails
				expect(gui.left.center.x, "gui.left.center.x should be a number").not.to.be.NaN;
				expect(gui.left.center.y, "gui.left.center.y should be a number").not.to.be.NaN;
				expect(gui.left.x, "gui.left.x should be a number").not.to.be.NaN;
				expect(gui.left.y, "gui.left.y should be a number").not.to.be.NaN;
				expect(gui.left.absoluteX, "gui.left.absoluteX should be 0").to.equal(0);
				expect(gui.left.absoluteY, "gui.left.absoluteX should be 0").to.equal(0);
				expect(gui.left.center.absoluteX, "gui.left.center.absoluteX").not.to.be.NaN;
				expect(gui.left.center.absoluteY, "gui.left.center.absoluteY").not.to.be.NaN;
				// [/group]
				expect(gui.left.center.components[0].absoluteX).to.equal(0, "Expected absoluteX = 0");
				expect(gui.left.center.components[0].absoluteY).to.equal(height - 5, 'Expected absolueY=height-5');
				expect(gui.collision(new ScreenPoint(5, height))).to.be.an.instanceof(Button, "Expected the collision to find the button");
				const button = gui.collision(new ScreenPoint(5, height));
				expect(button.click.bind(button)).to.throw("Super test");
			}
		);
		it("should create a button on the top center",
			function () {
				const throwingFunction = () => {
					throw Error("Super test");
				};
				gui.top.center.add.button("test", throwingFunction, 10, 10);
				expect(gui.top.center.height).to.equal(10, "Expected the dynamic bar's height to be 10");
				expect(gui.top.height).to.equal(10, "Expected the spanned bar's height to be 10");
				expect(gui.top.center.width).to.equal(10, "Expected the dynamic bar's width to be 10");
				expect(gui.top.absoluteX).to.equal(0, "Expected the spanned bar's absoluteX to be 0");
				expect(gui.top.absoluteY).to.equal(0, "Expected the spanned bar's absoluteY to be 0");
				expect(gui.top.center.absoluteX).to.equal(canvas.width / 2 - 5, "Expected the dynamic bar's absoluteX to be at the center minus the margin");
				expect(gui.top.center.absoluteY).to.equal(0, "Expected the dynamic bar's absoluteY to be 0");
				const button = gui.collision(new ScreenPoint(canvas.width / 2, 5));
				expect(button.click.bind(button)).to.throw("Super test");
				expect(button.x).to.equal(0, "button should have x=0");
				expect(button.y).to.equal(0, "button should have y=0");
				expect(button.absoluteX).to.equal(canvas.width / 2 - 5, "Button's aboluteX should be centered");
				expect(button.absoluteY).to.equal(0, "Button should have absoluteY=0");
			}
		);
		it("should manage translation",
			function () {
				gui.top.left.add.button("test", () => {}, 10, 10);
				const button = gui.collision(new ScreenPoint(5, 5));
				expect(button).to.be.an.instanceof(Button, "Expected the collision to detect a button");
				expect(button.description).to.equal("Ceci est un bouton de test");
				expect(button.title).to.equal("Bouton test");
				gui.translator.locale = "en_US";
				expect(button.description).to.equal("This is a test button");
				expect(button.title).to.equal("Test Button");
			}
		);
	});

	describe("Positionning", function () {
		it("should position two buttons in the top center",
			function () {
				gui.top.center.add.button("test", () => {}, 20, 10);
				gui.top.center.add.button("test", () => {}, 30, 15);
				expect(gui.top.center.components).to.have.lengthOf(2, "top.center should have two elements");
				const first = gui.top.center.components[0];
				const second = gui.top.center.components[1];
				expect(first.x).to.equal(0, "The first element should be on the left of the center");
				expect(second.x).to.equal(20, "The second element should be right of the first");
				expect(first.y).to.equal(2.5, "The first element should be centered vertically");
				expect(second.y).to.equal(0, "The second element should be on the top because it is the tallest one");
				expect(first.absoluteX).to.equal(375, "first.absoluteX should be the center minus the size of the center dynamic bar");
				expect(second.absoluteX).to.equal(395, "second.absoluteX should be first.absoluteX + first.width");
				expect(first.absoluteY).to.equal(first.y, "first.absoluteY sould equal first.y");
				expect(second.absoluteY).to.equal(second.y, "second.absoluteY should equal second.y");
			}
		);
		it("should position two buttons in the left top",
			function () {
				gui.left.top.add.button("test", () => {}, 20, 10);
				gui.left.top.add.button("test", () => {}, 30, 15);
				expect(gui.left.top.components).to.have.lengthOf(2, "left.topshould have two elements");
				const first = gui.left.top.components[0];
				const second = gui.left.top.components[1];
				expect(first.x).to.equal(5, "The first element should be centered horizontally");
				expect(second.x).to.equal(0, "The second element's x should be left because itis the largest one");
				expect(first.y).to.equal(0, "the first element should be on the top");
				expect(second.y).to.equal(10, "The second element should be bellow the first one");
				expect(first.absoluteX).to.equal(first.x, "first.absoluteX");
				expect(second.absoluteX).to.equal(second.x, "second.absoluteX");
				expect(first.absoluteY).to.equal(first.y, "first.absoluteY sould equal first.y");
				expect(second.absoluteY).to.equal(second.y, "second.absoluteY should equal second.y");
			}
		);
	});
});

/**
 * @param {ElementInterface} object,
 * @param {string} name
 * @param {number} x
 * @param {number} y
 * @param {number} absoluteX
 * @param {number] absoluteY
 */
const assertPosition = function (object, name, x, y, absoluteX, absoluteY) {
	if (!object instanceof ElementInterface)
		throw Error("Should assert on ElementInterface");
	if (typeof x !== "number")
		throw Error("Expected x to be a number, " + typeof x + " given");
	if (typeof y !== "number")
		throw Error("Expected y to be a number, " + typeof y + " given");
	if (typeof absoluteX !== "number")
		throw Error("Expected absoluteX to be a number, " + typeof absoluteX + " given");
	if (typeof absoluteY !== "number")
		throw Error("Expected absoluteY to be a number, " + typeof absoluteY + " given");
	expect(object.x).to.equal(x,
		"Expected " + name + ".x to be " + x + ", got " + object.x);
	expect(object.y).to.equal(y,
		"Expected " + name + ".y to be " + y + ", got " + object.y);
	expect(object.absoluteX).to.equal(absoluteX,
		"Expected " + name + ".absoluteX to be " + absoluteX + ", got " + object.absoluteX);
	expect(object.absoluteY).to.equal(absoluteY,
		"Expected " + name + ".absoluteY to be " + absoluteY + ", got " + object.absoluteY);
}
/**
 * @param {ElementInterface} object
 * @param {string} name
 * @param {number} width
 * @param {number} height
 */
const assertSize = function (object, name, width, height) {
	expect(object.width).to.equal(width,
		"Expected " + name + ".width to be " + width + ", got " + object.width);
	expect(object.height).to.equal(height,
		"Expected " + name + ".height to be " + height + ", got " + object.height);
}
/**
 * @param {ElementInterface} object
 * @param {string} name
 * @param {ALIGNMENT} alignment
 * @param {DIRECTION} direction
 */
const assertAlignment = function (object, name, alignment, direction) {
	expect(object, `${name} should not be undefined!`).not.to.be.undefined;
	expect(object.alignment).to.equal(alignment, `Expected ${name} to be aligned ${textAlignment(alignment)}, git ${textAlignment(object.alignment)}`);
	expect(object.direction).to.equal(direction, `Expectef ${name} to be oriented with ${direction}, got ${object.direction}`);
};

/**
 * @param {ALIGNMENT} alignment
 * @return {string}
 */
const textAlignment = function (alignment) {
	switch (alignment) {
	case ALIGNMENT.TOP:
		return "top";
	case ALIGNMENT.BOTTOM:
		return "bottom";
	case ALIGNMENT.LEFT:
		return "left";
	case ALIGNMENT.RIGHT:
		return "right";
	case ALIGNMENT.CENTER:
		return "center";
	default:
		return "invalid";
	}
};
