import {
	expect
} from "chai";
import Camera from "../../src/vue/camera";
import Point from "../../src/geometry/points/point";
import Segment from "../../src/geometry/curves/segment";
import Line from "../../src/geometry/curves/line";
import ScreenPoint from "../../src/vue/screenPoint";
import SegmentPrinter from "../../src/vue/printers/segmentPrinter";
import LinePrinter from "../../src/vue/printers/linePrinter";

describe("Line and segment printers", function () {
	let canvas;
	let camera;
	let leftCanvas;
	let rightCanvas;
	let topCanvas;
	let bottomCanvas;
	let leftScene;
	let rightScene;
	let topScene;
	let bottomScene;
	beforeEach(function () {
		canvas = {
			width: 1000,
			height: 500
		};
		canvas.getContext = () => {
			return {
				canvas: canvas
			};
		};
		camera = new Camera();
	});

	describe("Line printers", function () {
		beforeEach(function () {
			leftCanvas = new ScreenPoint(
				0,
				Math.round(Math.random() * canvas.height)
			);
			rightCanvas = new ScreenPoint(
				canvas.width,
				Math.round(Math.random() * canvas.height)
			);
			topCanvas = new ScreenPoint(
				Math.round(canvas.width * Math.random()),
				0
			);
			bottomCanvas = new ScreenPoint(
				Math.round(canvas.width * Math.random()),
				canvas.height
			);
			leftScene = camera.canvasToScene(leftCanvas, canvas);
			rightScene = camera.canvasToScene(rightCanvas, canvas);
			topScene = camera.canvasToScene(topCanvas, canvas);
			bottomScene = camera.canvasToScene(bottomCanvas, canvas);
		});
		it("should detect the pertinent points to draw a line from left to right",
			function () {
				const line = Line.fromPoints(leftScene, rightScene);
				assertStartAndEnd(line, leftCanvas, rightCanvas);
			}
		);
		it("should detect the points to draw a line from left to top",
			function () {
				const line = Line.fromPoints(leftScene, topScene);
				assertStartAndEnd(line, topCanvas, leftCanvas);
			}
		);
		it("should detect the points to draw a line from left to bottom",
			function () {
				const line = Line.fromPoints(leftScene, bottomScene);
				assertStartAndEnd(line, leftCanvas, bottomCanvas);
			}
		);
		it("should detect the points to draw a line from top to right",
			function () {
				const line = Line.fromPoints(topScene, rightScene);
				assertStartAndEnd(line, topCanvas, rightCanvas);
			}
		);
		it("should detect the points to draw a line from top to bottom",
			function () {
				const line = Line.fromPoints(topScene, bottomScene);
				assertStartAndEnd(line, topCanvas, bottomCanvas);
			}
		);
		it("should detect the points to draw a line from right to bottom",
			function () {
				const line = Line.fromPoints(rightScene, bottomScene);
				assertStartAndEnd(line, bottomCanvas, rightCanvas);
			}
		);
		it("should not print a line outside the camera",
			function () {
				const start = new ScreenPoint(-10, canvas.height + 15);
				const end = new ScreenPoint(canvas.width + 100, canvas.height + 50);
				const line = Line.fromPoints(camera.canvasToScene(start, canvas), camera.canvasToScene(end, canvas));
				const printer = camera.getPrinterFor(line);
				expect(printer.startPoint(line, camera, canvas), "The start point should be null").to.be.null;
				expect(printer.endPoint(line, camera, canvas), "The end point should be null").to.be.null;
			}
		);
	});
	describe("Segment printers", function () {
		it("should detect the pertinent points to draw a segment from left to right",
			function () {
				canvas.height = 1020;
				// y = x + 15
				const start = camera.canvasToScene(new ScreenPoint(-2, 13), canvas);
				const end = camera.canvasToScene(new ScreenPoint(1002, 1017), canvas);
				assertStartAndEnd(new Segment(start, end), new ScreenPoint(0, 15), new ScreenPoint(1000, 1015));
			}
		);
		it("should detect the pertinent points to draw a segment from left to top",
			function () {
				// y = 15 - x
				const start = camera.canvasToScene(new ScreenPoint(-2, 17), canvas);
				const end = camera.canvasToScene(new ScreenPoint(17, -2), canvas);
				assertStartAndEnd(new Segment(start, end), new ScreenPoint(0, 15), new ScreenPoint(15, 0));
			}
		);
		it("should detect the pertinent points to draw a segment from bottom to left",
			function () {
				// y = x + 400
				const start = camera.canvasToScene(new ScreenPoint(200, 600), canvas);
				const end = camera.canvasToScene(new ScreenPoint(-15, 385), canvas);
				assertStartAndEnd(new Segment(start, end), new ScreenPoint(100, 500), new ScreenPoint(0, 400));
			}
		);
		it("should detect the pertinent points to draw a segment from top to right",
			function () {
				// y = x - 950
				const start = camera.canvasToScene(new ScreenPoint(-20, -970), canvas);
				const end = camera.canvasToScene(new ScreenPoint(1050, 100), canvas);
				assertStartAndEnd(new Segment(start, end), new ScreenPoint(950, 0), new ScreenPoint(1000, 50));
			}
		);
		it("should detect the pertinent points to draw a segment from top to bottom",
			function () {
				const start = camera.canvasToScene(new ScreenPoint(10, -10), canvas);
				const end = camera.canvasToScene(new ScreenPoint(10, 540), canvas);
				assertStartAndEnd(new Segment(start, end), new ScreenPoint(10, 0), new ScreenPoint(10, 500));
			}
		);
		it("should detect the pertinent points to draw a line from right to bottom",
			function () {
				// y = 1010 - x
				const start = camera.canvasToScene(new ScreenPoint(10, 1000), canvas);
				const end = camera.canvasToScene(new ScreenPoint(1040, -30), canvas);
				assertStartAndEnd(new Segment(start, end), new ScreenPoint(510, 500), new ScreenPoint(1000, 10));
			}
		);
		it("should detect the pertinent points if the start of the segment is outside the canvas and the end in the canvas",
			function () {
				const start = new ScreenPoint(canvas.width + 400, 8);
				const end = new ScreenPoint(10, 8);
				const line = new Segment(camera.canvasToScene(start, canvas), camera.canvasToScene(end, canvas));
				assertStartAndEnd(line, new ScreenPoint(canvas.width, 8), end);
			}
		);
		it("should detect the pertinent points if the start of the segment is inside the canvas and the end out",
			function () {
				const start = new ScreenPoint(10, 40);
				const end = new ScreenPoint(10, canvas.height + 100);
				const line = new Segment(camera.canvasToScene(start, canvas), camera.canvasToScene(end, canvas));
				assertStartAndEnd(line, start, new ScreenPoint(10, canvas.height));
			}
		);
		it("should detect the correct points if both the start and the end point of the segment are inside the canvas",
			function () {
				const start = new ScreenPoint(Math.round(Math.random() * canvas.width), Math.round(Math.random() * canvas.height));
				const end = new ScreenPoint(Math.round(Math.random() * canvas.width), Math.round(Math.random() * canvas.height));
				const line = new Segment(camera.canvasToScene(start, canvas), camera.canvasToScene(end, canvas));
				assertStartAndEnd(line, start, end);
			}
		);
		it("should not detect any pertinent points if the segment does not cross the canvas",
			function () {
				const start = new ScreenPoint(-10, canvas.height + 15);
				const end = new ScreenPoint(canvas.width + 100, canvas.height + 50);
				const line = new Segment(camera.canvasToScene(start, canvas), camera.canvasToScene(end, canvas));
				const printer = camera.getPrinterFor(line);
				expect(printer.startPoint(line, camera, canvas), "The start point should be null").to.be.null;
				expect(printer.endPoint(line, camera, canvas), "The end point should be null").to.be.null;
		   }
		);
	});
	const assertStartAndEnd = function (object, correctStart, correctEnd) {
		const printer = camera.getPrinterFor(object);
		const start = printer.startPoint(object, camera, canvas);
		const end = printer.endPoint(object, camera, canvas);
		expect(start.x).to.equal(correctStart.x, "The starting point doesn't have the right abscissa");
		expect(start.y).to.equal(correctStart.y, "The strating point doesn't have the right ordinate");
		expect(end.x).to.equal(correctEnd.x, "The end point doesn't have the right abscissa");
		expect(end.y).to.equal(correctEnd.y, "The end point doesn't have the right ordinate");
	};
});
