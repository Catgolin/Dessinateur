const path = require("path");

let config = {
	entry: "./src/index.js",
	output: {
		path: path.resolve(__dirname, "./build/"),
		filename: "./app.js",
		libraryTarget: "var",
		library: "MCDG",
	},
	mode: "development"
};
module.exports = config;
