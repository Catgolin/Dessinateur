const path = require("path");
const CircularDependencyPlugin = require('circular-dependency-plugin');

let config = {
	entry: "./test/index.js",
	output: {
		path: path.resolve(__dirname, "./build/"),
		filename: "test.js",
		libraryTarget: "var",
		library: "MCDG",
	},
	mode: "development",
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "@jsdevtools/coverage-istanbul-loader"
			}
		]
	},
	plugins: [
		new CircularDependencyPlugin({
			exclude: /a\.js|node_modules/,
			include: /src/,
			failOnError: false,
			allowAsyncCycles: false,
			cxd: process.cwd(),
		}),
	],
};
module.exports = config;
